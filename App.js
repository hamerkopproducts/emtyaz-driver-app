/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import React, { useEffect } from "react";
import {
  SafeAreaView,
  StatusBar,
  View,
  AppState
} from 'react-native';
import globals from "./app/lib/globals";
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import {HomeContainer } from './app/lib/navigationRoutes';
import SplashScreen from 'react-native-splash-screen';
import * as loginActions from "./app/actions/loginActions";
import Toast from 'react-native-toast-message';
const App: () => React$Node = (props) => {

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    AppState.addEventListener('change',handleAppStateChange);
    setTimeout(() => {
      SplashScreen.hide();
    }, 2000); 
  };
  //unmount
  useEffect(() => {
    return () => {
      handleComponentUnmount();
    }
  }, []);

  const handleComponentUnmount = () => {
    AppState.removeEventListener('change', handleAppStateChange);
  };

  const handleAppStateChange = (nextAppState) => {
    if (nextAppState === 'inactive') {
      console.log('the app is closed');
      if (props.currentScreen !== 'Home') {
        props.setCurrentScreen('Home')
      }
    }
  }
  //updated
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {
   
  };

  return (
      <>
      {props.isLogged ? <SafeAreaView style={{ flex: 0, backgroundColor: globals.COLOR.headerColor}} />:null}
      <SafeAreaView style={{ flex: 1, backgroundColor: globals.COLOR.screenBackground}}>
        <StatusBar
          barStyle="dark-content"
          backgroundColor={globals.COLOR.screenBackground}
        />
        <View style={{ width: '100%', height: '100%'}}>
          <HomeContainer />
        </View>
      </SafeAreaView>
      <Toast ref={(ref) => Toast.setRef(ref)} />
    </>
  );
};

const mapStateToProps = (state, props) => {
  return {
    currentScreen: state.loginReducer.currentScreen,
    isLogged: state.loginReducer.isLogged
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    setCurrentScreen: loginActions.setCurrentScreen
  }, dispatch)
};

const appWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(App);

appWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default appWithRedux;
