import { I18nManager } from 'react-native';
import { displayName as appName } from '../../app.json';

module.exports = {
	SCREEN_TITLE: {
		loginScreenTitle: 'Login'
	},
	STRING: {
		appName: appName,
		// Home list item card
		readyForDelivery: I18nManager.isRTL ? 'جاهز للتوصيل' : 'Ready For Delivery',
		readyForCollection: I18nManager.isRTL ? 'Ready For Collection' : 'Ready For Collection',
		readyForReturn: I18nManager.isRTL ? 'Ready For Return' : 'Ready For Return',
		toCollect: I18nManager.isRTL ? 'تجميع' : 'To Collect',
		toReturn: I18nManager.isRTL ? 'إرجاع' : 'To Return',
		deliverySchedule: I18nManager.isRTL ? 'جدول التوصيل' : 'Delivery Schedule',
		deliveryLocation: I18nManager.isRTL ? 'موقع التوصيل' : 'Delivery Location',
		markAsDelivered: I18nManager.isRTL ? 'تم التوصيل' : 'Mark As Delivered',
		//Details Screen
		deliveryAddress: I18nManager.isRTL ? 'عنوان التوصيل' : 'Delivery Address',
		billingAddress: I18nManager.isRTL ? 'عنوان الفوترة' : 'Billing Address',
		orderDetails: I18nManager.isRTL ? 'تفاصيل الطلب' : 'Order Details',
		items: I18nManager.isRTL ? 'سلع' : 'Items',
		orderId: I18nManager.isRTL ? 'رقم الطلب' : 'Order ID',
		customerName: I18nManager.isRTL ? 'اسم العميل' : 'Customer Name',
		orderSummary: I18nManager.isRTL ? 'ملخص الطلب' : 'Order Summary',
		subtotal: I18nManager.isRTL ? 'الإجمالي قبل الضريبة' : 'Total without VِAT',
		discount: I18nManager.isRTL ? 'الخصم' : 'Discount',
		deliveryCharge: I18nManager.isRTL ? 'رسوم التوصيل' : 'Delivery Charge',
		vatPercentage: I18nManager.isRTL ? 'ضَريبة القيمة المُضافة(%)' : 'VAT(%)',
		totalWithTax: I18nManager.isRTL ? 'الإجمالي بعد الضريبة' : 'Total with VAT',
		paymentMethod: I18nManager.isRTL ? 'طريقة الدفع' : 'Payment Method',
		note: I18nManager.isRTL ? 'ملاحظة الطلب' : 'Order Note',
		selectAll: I18nManager.isRTL ? 'حدد الكل' : 'Select All',
		markSelectedAsCollected: I18nManager.isRTL ? 'تم التجميع من الاختيار' : 'Mark All as collected',
		markSelectedAsReturned: I18nManager.isRTL ? 'تم الاسترجاع من الاختيار' : 'Mark Selected As Returned',
		markAsCollected: I18nManager.isRTL ? 'تم التجميع' : 'Mark As Collected',
		markAsReturned: I18nManager.isRTL ? 'تم الاسترجاع' : 'Return',
		markAsReturnedstatus: I18nManager.isRTL ? 'Mark As Returned' : 'Mark As Returned',
		returnReasonText: I18nManager.isRTL ? 'غير قادر على التواصل مع العميل أو العميل طلب العودة' : 'Unable to communicate with customer or customer ask for return',
		currency: I18nManager.isRTL ? 'ريال':'SAR',
		myOrders: I18nManager.isRTL ? 'طلباتي':'My Orders',
		//Order Screen
		delivered: I18nManager.isRTL ? 'تم التوصيل' : 'Delivered',
		returned: I18nManager.isRTL ? 'تم الاسترجاع' : 'Returned',
		nodata: I18nManager.isRTL ? 'No Orders found!' : 'No Orders found!',
	},
	SELECT_LANGUAGE:{
		Chooseyourlanguage: I18nManager.isRTL ? 'Choose your language' : 'Choose your language',
		En: I18nManager.isRTL ? 'English' : 'English',
		AR: I18nManager.isRTL ? 'عربى' : 'عربى',
	},
	CHAT:{
		Type: I18nManager.isRTL ? "الرد على المسؤول" : "Reply to admin",
		Send: I18nManager.isRTL ? "سال" : "SEND",
		placeholder: I18nManager.isRTL ? "اكتب شيئاً" : "Type something",
	},
	NOTIFICATION: {
		Message: I18nManager.isRTL ? "إعدادات الإشعارات" : "Notification Settings",
		Notication: I18nManager.isRTL ? "الإشعارات" : "Notification",
		Descripton: I18nManager.isRTL ? "تم قبول طلب قبول طلب قبول طلبك" : "Your order has been accepted",
		Type: I18nManager.isRTL ? " اك هن ا2 " : "2 hrs ago",
		Body: I18nManager.isRTL ? "إع قبول طلب قبول طلب قبول طلبدادات الإشعارات" : "Driver info: Ahmed moosa Delivery schedule 12th july 2020, 2pm- 5pm sasass",
	},
	WALKTHROUGH:{
		Login: I18nManager.isRTL ? 'تسجيل الدخول إلى حسابك باستخدام رقم الهاتف' : 'Login to your account using phone number',
		Mobile: I18nManager.isRTL ? 'رقم الهاتف' : 'Phone Number',
		VALIDMESSAGE:I18nManager.isRTL ? 'أدخل رقم هاتف صالح' : 'Enter a valid phone number',
		
	},
	HELPPOPUP:{
		Heading: I18nManager.isRTL ? 'نحن هنا للمساعدة' : 'We are here to help',
		Subject: I18nManager.isRTL ? 'الموضوع' : 'Subject',
		Message: I18nManager.isRTL ? 'رسالة' : 'Message',
		Send: I18nManager.isRTL ? 'ارسال' : 'Send',
		Subjecterror: I18nManager.isRTL ? 'الموضوع مطلوب' : 'Subject is required',
		Messageerror: I18nManager.isRTL ? 'الرسالة مطلوبة' : 'Messsage is required',
	},
	OTP:{
		Heading: I18nManager.isRTL ? 'تحقق من رقمك' : 'Verify your number',
		Enter: I18nManager.isRTL ? 'تم إرسال رمز تحقق إلى' : 'A Verification code has been sent to',
		Edit: I18nManager.isRTL ? 'الدخول' : 'Edit',
		Resend: I18nManager.isRTL ? ' ألم تحصل على الرمز؟' : "Did not get the code ?",
		Resends: I18nManager.isRTL ? 'ارسال' : "Resend",
		Sentto: I18nManager.isRTL ? 'الدخول' : 'Sent to',
		Submit: I18nManager.isRTL ? 'التحقق' : 'Verify',
		Verificationcode: I18nManager.isRTL ? 'اكتب رمز التحقق للمتابعة' : 'Type your verification code to continue',
		VALIDMESSAGE:I18nManager.isRTL ? 'رمز التحقق الذي تم إدخاله غير صحيح' : 'Verification code entered is wrong',
	},
	EDITPROFILE:{
		Heading: I18nManager.isRTL ? 'تعديل' : 'Edit',
		Name: I18nManager.isRTL ? 'اسم' : 'Name',
		Email: I18nManager.isRTL ? 'عنوان البريد الإلكتروني*' : 'Email Address *',
		Phone: I18nManager.isRTL ? 'رقم الهاتف*' : 'Phone Number *',
		Update: I18nManager.isRTL ? 'تحديث' : 'Update',
		Nameerror: I18nManager.isRTL ? 'الاسم مطلوب' : 'Name is required',
		Emailerror: I18nManager.isRTL ? 'مطلوب عنوان البريد الإلكتروني' : 'Email address is required',
		Emailvaliderror: I18nManager.isRTL ? 'أدخل عنوان بريد إلكتروني صالح' : 'Enter a valid email address',
		Phoneerror: I18nManager.isRTL ? 'رقم الهاتف مطلوب' : 'Phone number is required',
		phonevaliderror: I18nManager.isRTL ? 'أدخل رقم هاتف صالح' : 'Enter a valid phone number',
		profilemsg: I18nManager.isRTL ? 'تم تحديث ملف التعريف بنجاح' : 'Profile updated successfully',
	},
	PROFILELISTING:{
		Faq: I18nManager.isRTL ? 'الأسئلة الأكثرشيوعاً' : 'FAQs',
		Privacy: I18nManager.isRTL ? 'سياسة الخصوصية' : 'Privacy Policy',
		Terms: I18nManager.isRTL ? 'الشروط والأحكام' : 'Terms and Conditions',
		Support: I18nManager.isRTL ? 'مركز الخدمة' : 'Support',
		Logout: I18nManager.isRTL ? 'الخروج' : 'Logout',
		Edit:I18nManager.isRTL ? 'تعديل' : 'Edit',
	},
	APP_SLIDER:{
Content: I18nManager.isRTL ? 'تسجيل الدخول ا داعي لإهدار الوقت في أخذ المواعيد وجدولتها، عليك الموافقة على الحجوزات في ثواني، وانتظار عميلتك' : 'Am I missing something also? Because Im trying to clear cache in react-native, to be able to repeat a bug that only occurs on first usage. But clearing thos'
	},
	PRIVACY_CONTENT:{
		Title: I18nManager.isRTL ? ' وانتظاروانتظار  عميلتك' : 'Ut porta diam et ornare tempus.?',
		Content: I18nManager.isRTL ? 'تسجيل الدخول ا داعي لإهدار الوقت في أخذ المواعيد وجدولتها، عليك الموافقة على الحجوزات في ثواني، وانتظار عميلتك' : 'Am I missing something also? Because Im trying to clear cache in react-native, to be able to repeat a bug that only occurs on first usage. But clearing those caches above did not help. This is'
			},
	FAQ_CONTENT:{
		Title: I18nManager.isRTL ? ' وانتظار عميلتك' : 'Ut porta diam et ornare tempus.?',
		Content: I18nManager.isRTL ? ' وانتظاعليك الموافقة على الحجوزات في ثوار عميلتك' : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur sunt itaque adipisci quisquam pariatur qui, reiciendis architecto quod sint incidunt labore nisi totam illum numquam non magnam praesentium, maxime quaerat!',
			},
COMMON_POPUP:{
	Heading: I18nManager.isRTL ? 'تسجيل الدخول' : 'Are You want to accept the order',
	Accept: I18nManager.isRTL ?  'نعم' : 'Yes',
	Reject: I18nManager.isRTL ?'لا' : 'No',
	No: I18nManager.isRTL ? 'لا' : 'No',
	Yes: I18nManager.isRTL ? 'نعم' : 'Yes',
	cancel: I18nManager.isRTL ? 'cancel' : 'Cancel',
	done: I18nManager.isRTL ? 'done' : 'Done',
	Continue:I18nManager.isRTL ? 'استمر' : 'Continue',
	AmounttoCollect: I18nManager.isRTL ? 'المبلغ المطلوب تحصيله:' : 'Amount to Collect:',
	modalText1: I18nManager.isRTL ? 'هل قمت بتسليم الطلب إلى العميل؟' : 'Did you deliver the order to the customer?',
	modalText2: I18nManager.isRTL ? 'هل قمت بتسليم الطلب إلى العميل وجمعت المال؟' : 'Did you deliver the order to the customer and collected the money?',
	modalText3: I18nManager.isRTL ? 'تهانينا! تم إكمال الطلب': 'Congratulations! Order Completed',
	modal3SubText: I18nManager.isRTL ? 'لقد أكملت':'You Have Completed',
	modalText4: I18nManager.isRTL ? 'هل تريد بالتأكيد قبول جميع الطلبات؟' : 'Are you sure you want to accept the selected orders?',
	modalText7: I18nManager.isRTL ? 'هل تريد بالتأكيد قبول جميع الطلبات؟' : 'Are you sure you want to accept the selected orders?',
	modalText5: I18nManager.isRTL ? 'هل تريد استلام الطلب؟' : 'Do you want to collect the order?',
	modalText6: I18nManager.isRTL ? 'هل تريد إرجاع الطلب؟' : 'Do you want to return the order?',
	thankuModal: I18nManager.isRTL ? 'شكرًا لك!' : 'Thank you!',
	thankucontentModal: I18nManager.isRTL ? 'شكراً لاتصالك بنا! فريقنا سوف يعود إليك قريباً' : 'Thank you for contacting us! Our team	will get back to you soon.',
	logoutModal: I18nManager.isRTL ? 'هل تريد بالتأكيد تسجيل الخروج؟' : 'Are you sure want to logout?'
},
	PRIVACY:{
		Heading: I18nManager.isRTL ? 'تسجيل الدخول' : 'Privacy Policy',
		
		
	},
	REJECT:{
		Heading: I18nManager.isRTL ? 'تسجيل الدخول' : 'Are You want to Reject the order?',
		Headreason: I18nManager.isRTL ? 'تسجيل الدخول' : 'If you are cancelling the item,return the item to ware house',
		
		
	},
	ALERT_MESSAGES: {
		specifyPhoneNumber: I18nManager.isRTL ? 'رقم الهاتف مطلوب' :'Phone number is required',
		enterValidPhoneNumber: I18nManager.isRTL ? 'أدخل رقم هاتف صالح' : 'Enter a valid phone number',
		verifyYourNumber: I18nManager.isRTL ? 'تحقق من رقمك':'Verify your number',
		specifyOtp: I18nManager.isRTL ? 'اكتب رمز التحقق للمتابعة': 'Type your verification code to continue',
		wrongOtp: I18nManager.isRTL ? 'رمز التحقق الذي تم إدخاله غير صحيح' : 'Verification code entered is wrong',
		otpSendSuccess: I18nManager.isRTL ? 'تم إرسال رمز تحقق إلى': 'A verification code has been sent to',
		orderReturnedSuccessfully: I18nManager.isRTL ? 'Order Returned Successfully' : 'Order Returned Successfully',
		orderCollectedSuccessfully: I18nManager.isRTL ? 'Order Collected Successfully' : 'Order Collected Successfully',
		noInternet: I18nManager.isRTL ? 'No Network Connection' :'No Network Connection',
		success: I18nManager.isRTL ? 'Success' : 'Success',
		error: I18nManager.isRTL ? 'Error' : 'Error',
		nameRequired: I18nManager.isRTL ? 'الاسم مطلوب' : 'Name is rquired',
		emailRequired: I18nManager.isRTL ? 'مطلوب عنوان البريد الإلكتروني' : 'Email address is required',
		validEmailRequired: I18nManager.isRTL ? 'أدخل عنوان بريد إلكتروني صالح' : 'Enter a valid email address',
		phoneRequired: I18nManager.isRTL ? 'رقم الهاتف مطلوب' : 'Phone number is required',
		profileUpdatedSuccess: I18nManager.isRTL ? 'تم تحديث ملف التعريف بنجاح' : 'Profile updated successfully',
		subjectRequired: I18nManager.isRTL ? 'الموضوع مطلوب' : 'Subject is rquired',
		messageRequired: I18nManager.isRTL ? 'الرسالة مطلوبة' : 'Message is rquired',
		supportsendSuccess:  I18nManager.isRTL ? 'شكراً لاتصالك بنا! فريقنا سوف يعود إليك قريباً' : 'Thank you for contacting us! Our team will get back to you soon.',
	}
};
