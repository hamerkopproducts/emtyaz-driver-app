import { Platform, Dimensions } from 'react-native';
let { height, width } = Dimensions.get('window');
import { getStatusBarHeight, getBottomSpace } from 'react-native-iphone-x-helper'
let headerHeight= 50;
let footerTabBarHeight = 80;
let screenMargin = 20;
let screenPaddingFromFooter = 5;

module.exports = {
  DEVICE_TYPE: Platform.OS === "ios" ? 1 : 2,
  DEVICE_TYPE_STRING: Platform.OS,
  SPLASH_DELAY: 3000,
  SCREEN_SIZE: {
    width: width,
    height: height
  },
  COLOR: {
    headerColor: '#ffffff',
    transparent: 'transparent',
    screenBackground: '#fcfcfc',
    tabUnderLineColor:'#ff8001',
    textColorGreen:'#259245',
    linkTextColor:'#0090E5',
    borderColor: '#cccccc',
    textColor: '#000000',
    lightTextColor: '#707070',
    white: '#ffffff',
    black: '#000000',
    redTextColor:'#DB3236'
  },
  INTEGER: {
    headerHeight: headerHeight,
    topTabHieght:44,
    footerTabBarHeight: footerTabBarHeight,
    screenPaddingFromFooter: screenPaddingFromFooter,
    screenWidthWithMargin: width - (2 * screenMargin),
    screenContentHeight: height - (footerTabBarHeight + headerHeight + getStatusBarHeight('safe') + getBottomSpace() + screenPaddingFromFooter),
    leftRightMargin: screenMargin,
    statusBarHeight: getStatusBarHeight('safe'),
    bottomSpace: getBottomSpace()
  },
  FONTS: {
    avenirBlack: 'Avenir-Black',
    avenirLight: 'Avenir-Light',
    avenirMedium: 'Avenir-Medium',
    avenirHeavy: 'Avenir-Heavy',
    avenirRoman: 'Avenir-Roman',
    notokufiArabic:'NotoKufiArabic',
    notokufiarabicBold:'NotoKufiArabic-Bold',
  },
  FONTSIZE: {
    fontSizeSeven: 7,
    fontSizeEight: 8,
    fontSizeNine: 9,
    fontSizeTen: 10,
    fontSizeEleven: 11,
    fontSizeTwelve: 12,
    fontSizeThirteen: 13,
    fontSizeFourteen: 14,
    fontSizeFifteen: 15,
    fontSizeSixteen: 16,
    fontSizeSeventeen: 17,
    fontSizeEighteen: 18,
    fontSizeTwenty: 20,
    fontSizeTwentyTwo: 22,
    fontSizeTwentyFour: 24
  },
  MARGIN: {
    marginOne: 1,
    marginTwo: 2,
    marginThree: 3,
    marginFour: 4,
    marginFive: 5,
    marginSix: 6,
    marginSeven: 7,
    marginEight: 8,
    marginNine: 9,
    marginTen: 10,
    marginTwelve: 12,
    marginThirteen: 13,
    marginFifteen: 15,
    marginEighteen: 18,
    marginTwenty: 20
  }
};
