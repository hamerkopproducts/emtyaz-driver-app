import globals from "./globals";

let productionURL = "https://emtyaz.ourdemo.online/api/";
let developmentURL = "https://emdev.ourdemo.online/api/";

let baseURL = productionURL;
let getOtpAPI = "driver/login";
let loginAPI = "driver/verify";
let resendOtpAPI = "driver/resend";
let getReadyForDeliveryAPI = "order/redy-for-delivery";

let updateOrderStatus = "order/status-update";
let getOrdersToCollect = "order/collect-list";
let getDeliveredOrdersAPI = "order/delivered";

let getReturnedOrdersAPI = "order/returned";
let getOrderDetails = "order-details";
let updateProfileAPI = "profile/update";
let ChatAPI = "support";

const apiHelper = {
  getBaseUrl: () => {
    return baseURL;
  },
  getAPIHeaderForDelete: (params, token) => {
    const AuthStr = "Bearer ".concat(token);
    let headerConfig = {
      headers: {
        "Content-Type": "application/json",
        Authorization: AuthStr,
        Accept: "application/json",
      },
      data: params,
    };
    return headerConfig;
  },
  getAPIHeader: (token) => {
    const AuthStr = "Bearer ".concat(token);
    let headerConfig = {
      headers: {
        "Content-Type": "application/json",
        Authorization: AuthStr,
        Accept: "application/json",
      },
    };
    return headerConfig;
  },
  getOtpAPI: () => {
    return baseURL + getOtpAPI;
  },
  resendOtpAPI: () => {
    return baseURL + resendOtpAPI;
  },
  loginAPI: () => {
    return baseURL + loginAPI;
  },
  getReadyForDeliveryAPI: (page) => {
    let queryString = page !== 0 ? "?page=" + page : "";
    return baseURL + getReadyForDeliveryAPI + queryString;
  },
  getOrdersToCollectAPI: (page) => {
    let queryString = page !== 0 ? "?page=" + page : "";
    return baseURL + getOrdersToCollect + queryString;
  },
  getDeliveredOrdersAPI: (page, selectedDate) => {
    let queryString = "";
    if (page !== 0 && selectedDate) {
      queryString = "?page=" + page + "&date=" + selectedDate;
    } else {
      if (page !== 0) {
        queryString = "?page=" + page;
      } else {
        if(selectedDate) {
          queryString = "?date=" + selectedDate;
        }
      }
    }
    return baseURL + getDeliveredOrdersAPI + queryString;
  },
  getReturnedOrdersAPI: (page, selectedDate) => {
    let queryString = "";
    if (page !== 0 && selectedDate) {
      queryString = "?page=" + page + "&date=" + selectedDate;
    } else {
      if (page !== 0) {
        queryString = "?page=" + page;
      } else {
        queryString = "?date=" + selectedDate;
      }
    }
    return baseURL + getReturnedOrdersAPI + queryString;
  },
  updateOrderStatusAPI: () => {
    return baseURL + updateOrderStatus;
  },
  getOrderDetailsAPI: (orderId) => {
    return baseURL + getOrderDetails + "/" + orderId;
  },
  updateProfileAPI: () => {
    return baseURL + updateProfileAPI;
  },
  ChatAPI: () => {
    return baseURL + ChatAPI;
  },
};

export default apiHelper;
