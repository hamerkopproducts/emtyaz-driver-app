import React, { Component } from 'react';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import globals from "./globals"

//Appintrologin Screens
import AppIntrosliderloginScreen from '../containers/AppIntrosliderloginScreen'
import SplashScreen from '../containers/SplashScreen'
import ChooseLanguageScreen from '../containers/ChooseLanguageScreen'
import OtpScreen from '../containers/OtpScreen'
import HelpPopupScreen from '../containers/HelppopupScreen'
import HomeScreen from '../containers/HomeScreen'
import OrderDetailsScreen from '../containers/OrderDetailsScreen'
import PrivacyScreen from '../containers/PrivacypolicyScreen'
import FaqScreen from '../containers/FaqScreen'
import TermsScreen from '../containers/TermsandconditionsScreen'
//MyOrdersScreen Tab Screen
import MyOrdersScreen from '../containers/MyOrdersScreen'

// ScanScreen Tab Screen
import ScanScreen from '../containers/ScanScreen'

// Chat  Screen
import ChatScreen from '../containers/ChatScreen'

//Profile Tab Screen
import ProfileScreen from '../containers/ProfileScreen'

//Footer Tab Item Style
import FooterTabItem from '../components/FooterTabItem'

import NotificationScreen from '../containers/NotificationScreen'


// HomeTab stacks for screen navigation
const HomeStack = createStackNavigator();
const HomeTab = () => {
	return (
		<HomeStack.Navigator screenOptions={{ headerShown: false }}>
			<HomeStack.Screen name="HomeScreen" component={HomeScreen} />
		</HomeStack.Navigator>
	)
}
// MyOrdersTab stacks for screen navigation
const MyOrdersScreenStack = createStackNavigator();
const MyOrdersTab = () => {
	return (
		<MyOrdersScreenStack.Navigator screenOptions={{ headerShown: false }}>
			<MyOrdersScreenStack.Screen name="MyOrdersScreen" component={MyOrdersScreen} />
		</MyOrdersScreenStack.Navigator>
	)
}
// ScanTab stacks for screen navigation
const ScanScreenStack = createStackNavigator();
const ScanTab = () => {
	return (
		<ScanScreenStack.Navigator screenOptions={{ headerShown: false }}>
			<ScanScreenStack.Screen name="ScanScreen" component={ScanScreen} />
		</ScanScreenStack.Navigator>
	)
}
// ProfileTab stacks for screen navigation
const ProfileScreenStack = createStackNavigator();
const ProfileTab = () => {
	return (
		<ProfileScreenStack.Navigator screenOptions={{ headerShown: false }}>
			<ProfileScreenStack.Screen name="ProfileScreen" component={ProfileScreen} />
		</ProfileScreenStack.Navigator>
	)
}

//Initialize bottom tab navigator
const DashboardTabRoutes = createBottomTabNavigator();

const TabNavigator = () => {
	return (
		<DashboardTabRoutes.Navigator tabBarOptions={{
			showLabel: false,
			style: {
				height: globals.INTEGER.footerTabBarHeight,
				borderWidth: 0,
				borderTopLeftRadius: 20,
				borderTopRightRadius: 20,
				borderTopWidth: 0.5,
				borderRightWidth: 0.5,
				borderLeftWidth: 0.5,
				backgroundColor: globals.COLOR.white,
				borderColor: "#ccc",
				shadowColor: "#000",
				shadowOffset: { width: 0, height: 0 },
				shadowOpacity: 0.2,
				shadowRadius: 1,
				elevation: 1,
			}
		}}>
			<DashboardTabRoutes.Screen
				name="Home"
				component={HomeTab}
				options={{
					tabBarLabel: 'Home',
					tabBarIcon: ({ focused }) => (
						<FooterTabItem tabBarIndex={0} isFocused={focused} />
					),
				}}
			/>
			<DashboardTabRoutes.Screen
				name="MyOrders"
				component={MyOrdersTab}
				options={{
					tabBarLabel: 'My Orders',
					tabBarIcon: ({ focused }) => (
						<FooterTabItem tabBarIndex={1} isFocused={focused} />
					),
				}}
			/>
			<DashboardTabRoutes.Screen
				name="Scan"
				component={ScanTab}
				options={{
					tabBarLabel: 'Scan',
					tabBarIcon: ({ focused }) => (
						<FooterTabItem tabBarIndex={2} isFocused={focused} />
					),
				}}
			/>
			<DashboardTabRoutes.Screen
				name="MyProfile"
				component={ProfileTab}
				options={{
					tabBarLabel: 'My Profile',
					tabBarIcon: ({ focused }) => (
						<FooterTabItem tabBarIndex={3} isFocused={focused} />
					),
				}}
			/>
		</DashboardTabRoutes.Navigator>

	);
};


//Auth stacks for screen navigation
const AuthScreenStack = createStackNavigator();
const AuthStackNavigator = () => {
	return (
		<AuthScreenStack.Navigator
			screenOptions={{
				headerShown: false
			}}
		>
			<AuthScreenStack.Screen name="SplashScreen" component={SplashScreen} />
			<AuthScreenStack.Screen name="ChooseLanguageScreen" component={ChooseLanguageScreen} />
			<AuthScreenStack.Screen name="AppIntrosliderloginScreen" component={AppIntrosliderloginScreen} />
			<AuthScreenStack.Screen name="OtpScreen" component={OtpScreen} />
		</AuthScreenStack.Navigator>
	)
}

//Main stacks for screen navigation
const MainScreenStack = createStackNavigator();
const MainScreenStackNavigator = () => {
	return (
		<MainScreenStack.Navigator
			screenOptions={{
				headerShown: false
			}}
		>
		<MainScreenStack.Screen name="AuthStackNavigator" component={AuthStackNavigator} />
			<MainScreenStack.Screen name="TabNavigator" component={TabNavigator} />
			<MainScreenStack.Screen name="OrderDetailsScreen" component={OrderDetailsScreen} />
			<MainScreenStack.Screen name="PrivacyScreen" component={PrivacyScreen} />
			<MainScreenStack.Screen name="FaqScreen" component={FaqScreen} />
			<MainScreenStack.Screen name="TermsScreen" component={TermsScreen} />
			<MainScreenStack.Screen name="HelpPopupScreen" component={HelpPopupScreen} />
			<MainScreenStack.Screen name="ChatScreen" component={ChatScreen} />
			<MainScreenStack.Screen name="NotificationScreen" component={NotificationScreen} />
		</MainScreenStack.Navigator>
	)
}
const MyTheme = {
	...DefaultTheme,
	colors: {
		...DefaultTheme.colors,
		background: '#FFFFFF'
	},
};
export const HomeContainer = () => {
	return (
		<NavigationContainer theme={MyTheme}>
			<MainScreenStackNavigator />
		</NavigationContainer>
	)
}