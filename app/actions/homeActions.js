import * as ActionTypes from './types';
import apiHelper from "../lib/apiHelper"
import axios from 'react-native-axios';
import globals from "../lib/globals"

export const apiServiceActionLoading = () => ({
  type: ActionTypes.GET_ORDER_LOADING
});

export const apiServiceActionError = (error) => ({
  type: ActionTypes.GET_ORDER_ERROR,
  error: error
});

export function updateOderToDeliver(responseData) {
  return {
    type: ActionTypes.GET_ORDER_TO_DELIVER_SUCCESS,
    responseData: responseData
  };
}

export function updateOderToCollect(responseData) {
  return {
    type: ActionTypes.GET_ORDER_TO_COLLECT_SUCCESS,
    responseData: responseData
  };
}
export function updateOderToReturn(responseData) {
  return {
    type: ActionTypes.GET_ORDER_TO_RETURN_SUCCESS,
    responseData: responseData
  };
}

export function getOrdersToDeliver(page, token) {
  return async dispatch => {
    dispatch(apiServiceActionLoading());
    await axios.get(apiHelper.getReadyForDeliveryAPI(page), apiHelper.getAPIHeader(token))
      .then(response => {
       //console.log('response::'+JSON.stringify(response))
        dispatch(updateOderToDeliver(response.data.data.order));
      }).catch(error => {
        dispatch(apiServiceActionError(error.response.data.error));
      });
  }
}
export function getOrdersToCollect(page,token) {
  return async dispatch => {
    dispatch(apiServiceActionLoading());
    await axios.get(apiHelper.getOrdersToCollectAPI(page), apiHelper.getAPIHeader(token))
      .then(response => {
        //console.log('response::' + JSON.stringify(response))
        dispatch(updateOderToCollect(response.data.data.order));
      }).catch(error => {
        dispatch(apiServiceActionError(error.response.data.error));
      });
  }
}
export function getOrdersToReturn(page,token) {
  return async dispatch => {
    dispatch(apiServiceActionLoading());
    await axios.get(apiHelper.getReadyForDeliveryAPI(page), apiHelper.getAPIHeader(token))
      .then(response => {
        //console.log('response::' + JSON.stringify(response))
        dispatch(updateOderToReturn(response.data.data.order));
      }).catch(error => {
        dispatch(apiServiceActionError(error.response.data.error));
      });
  }
}

export function updateOderToDeliverStatus() {
  return {
    type: ActionTypes.RESET_ORDER_TO_DELIVER_STATUS
  };
}
export function updateOderToCollectStatus() {
  return {
    type: ActionTypes.RESET_ORDER_TO_COLLECT_STATUS
  };
}
export function updateOderToReturnStatus() {
  return {
    type: ActionTypes.RESET_ORDER_TO_RETURN_STATUS
  };
}
