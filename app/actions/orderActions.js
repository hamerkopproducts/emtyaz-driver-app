import * as ActionTypes from './types';
import apiHelper from "../lib/apiHelper"
import axios from 'react-native-axios';
import globals from "../lib/globals"

export const apiServiceActionLoading = () => ({
  type: ActionTypes.GET_COMPLETED_ORDER_LOADING
});

export const apiServiceActionError = (error) => ({
  type: ActionTypes.GET_COMPLETED_ORDER_ERROR,
  error: error
});

export function updateDeliveredOder(responseData) {
  return {
    type: ActionTypes.GET_DELIVERED_ORDER_SUCCESS,
    responseData: responseData
  };
}

export function updateReturnedOder(responseData) {
  return {
    type: ActionTypes.GET_RETURNED_ORDER_SUCCESS,
    responseData: responseData
  };
}

export function updateOrderStatusSuccess(responseData) {
  return {
    type: ActionTypes.UPDATE_ORDER_STATUS,
    responseData: responseData
  };
}

export function updateOrderDetailsSuccess(responseData) {
  return {
    type: ActionTypes.UPDATE_ORDER_DETAILS,
    responseData: responseData
  };
}
export function updateOrderStatusFromDetails(responseData) {
  return {
    type: ActionTypes.UPDATE_ORDER_DETAILS_STATUS,
    responseData: responseData
  };
}

export function getDeliveredOrders(page,selectedDate,token) {
  return async dispatch => {
    dispatch(apiServiceActionLoading());
    console.log('url::' + apiHelper.getDeliveredOrdersAPI(page,selectedDate))
    //console.log('token::' + token)
    await axios.get(apiHelper.getDeliveredOrdersAPI(page,selectedDate), apiHelper.getAPIHeader(token))
      .then(response => {
        //console.log('getDeliveredOrders response::' + JSON.stringify(response))
        dispatch(updateDeliveredOder(response.data.data.order));
        //dispatch(updateDeliveredOder(orderArray));
        
      }).catch(error => {
        dispatch(apiServiceActionError(error.response.data.error));
      });
  }
}

export function getReturnedOrders(page,selectedDate,token) {
  return async dispatch => {
    dispatch(apiServiceActionLoading());
    console.log('url::' + apiHelper.getReturnedOrdersAPI(page,selectedDate))
    //console.log('token::' + token)
    await axios.get(apiHelper.getReturnedOrdersAPI(page,selectedDate), apiHelper.getAPIHeader(token))
      .then(response => {
        //console.log('getReturnedOrders response::' + JSON.stringify(response))
        dispatch(updateReturnedOder(response.data.data.order));
      }).catch(error => {
        dispatch(apiServiceActionError(error.response.data.error));
      });
  }
}

export function updateStatus(params,token) {
  console.log('params::' + JSON.stringify(params))
  //console.log('token::' + token)
  return async dispatch => {
    dispatch(apiServiceActionLoading());
    await axios.post(apiHelper.updateOrderStatusAPI(), params, apiHelper.getAPIHeader(token))
      .then(response => {
       // console.log('response::' + JSON.stringify(response.data))
        dispatch(updateOrderStatusSuccess(response.data));
      }).catch(error => {
        dispatch(apiServiceActionError(error.response.data.error));
      });
  }
}

export function updateStatusFromDetails(params, token) {
  //console.log('params::' + JSON.stringify(params))
  //console.log('token::' + token)
  return async dispatch => {
    dispatch(apiServiceActionLoading());
    await axios.post(apiHelper.updateOrderStatusAPI(), params, apiHelper.getAPIHeader(token))
      .then(response => {
        console.debug(response.data)
        // console.log('response::' + JSON.stringify(response.data))
        dispatch(updateOrderStatusFromDetails(response.data));
      }).catch(error => {
        console.debug(error)
        dispatch(apiServiceActionError(error.response.data.error));
      });
  }
}

export function getOrderDetails(orderId, token) {
 
  return async dispatch => {
    dispatch(apiServiceActionLoading());
    await axios.get(apiHelper.getOrderDetailsAPI(orderId), apiHelper.getAPIHeader(token))
      .then(response => {
        console.log('order details response::' + JSON.stringify(response))
        dispatch(updateOrderDetailsSuccess(response.data.data.details));
      }).catch(error => {
        dispatch(apiServiceActionError(error.response.data.error));
      });
  }
}