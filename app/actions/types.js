export const LOGIN_SERVICE_LOADING = 'login_service_loading'
export const OTP_SERVICE_LOADING = 'otp_service_loading'
export const LOGIN_SERVICE_ERROR = 'login_service_error'
export const LOGIN_SUCCESS = 'login_success'
export const UPDATE_LOGOUT_STATUS = 'update_logout_status'
export const LOGOUT_SUCCESS = 'logout_success'
export const LANGUAGE_SELECTION_SUCCESS = 'language_selection_success'
export const OTP_SUCCESS = 'otp_success'
export const OTP_SERVICE_ERROR = 'otp_service_error'
export const PROFILE_UPDATE_SUCCESS = 'profile_update_success'
export const UPDATE_PROFILE_UPDATE_STATUS = 'update_profile_update_status'
export const RESET_ERROR = 'reset_error'


//home

export const GET_ORDER_LOADING = 'get_order_loading'
export const GET_ORDER_ERROR = 'get_order_error'
export const GET_ORDER_TO_DELIVER_SUCCESS = 'get_order_to_deliver_success'
export const GET_ORDER_TO_COLLECT_SUCCESS = 'get_order_to_collect_success'
export const GET_ORDER_TO_RETURN_SUCCESS = 'get_order_to_return_success'
export const RESET_ORDER_TO_DELIVER_STATUS = 'reset_order_to_deliver_status'
export const RESET_ORDER_TO_COLLECT_STATUS = 'reset_order_to_collect_status'
export const RESET_ORDER_TO_RETURN_STATUS = 'reset_order_to_return_status'


//order
export const GET_COMPLETED_ORDER_LOADING = 'get_completed_order_loading'
export const GET_COMPLETED_ORDER_ERROR = 'get_completed_order_error'
export const GET_DELIVERED_ORDER_SUCCESS = 'get_delivered_order_success'
export const GET_RETURNED_ORDER_SUCCESS = 'get_returned_order_success'

export const UPDATE_ORDER_STATUS = 'update_order_status'
export const UPDATE_ORDER_DETAILS_STATUS = 'update_order_details_status'
export const UPDATE_ORDER_DETAILS = 'update_order_details'
export const SET_CURRENT_SCREEN = 'set_current_screen'

//chat 
export const CHAT_SUCCESS = 'chat_success'
export const CHAT_SUPPORT_STATUS = 'chat_support_status'



