import * as ActionTypes from './types';
import apiHelper from "../lib/apiHelper"
import { defaultLoginState } from "../reducers/loginReducer";
import axios from 'react-native-axios';
import globals from "../lib/globals"




export const apiServiceActionLoading = () => ({
	type: ActionTypes.LOGIN_SERVICE_LOADING
});
export const apiServiceOtpActionLoading = () => ({
	type: ActionTypes.OTP_SERVICE_LOADING
});
export const apiServiceActionError = (error) => ({
	type: ActionTypes.LOGIN_SERVICE_ERROR,
	error: error
});

export const otpServiceError = (error) => ({
	type: ActionTypes.OTP_SERVICE_ERROR,
	error: error
});

export function loginServiceActionSuccess(responseData) {
	return {
		type: ActionTypes.LOGIN_SUCCESS,
		responseData: responseData
	};
}
export function chatActionSuccess(responseData) {
	return {
		 type: ActionTypes.CHAT_SUCCESS,
		 responseData: responseData
	};
}
export function getOtpSuccess(responseData) {
	return {
		type: ActionTypes.OTP_SUCCESS,
		responseData: responseData
	};
}
export function updateProfileSuccess(responseData, userData) {
	return {
		type: ActionTypes.PROFILE_UPDATE_SUCCESS,
		responseData: responseData,
		userData: userData
	};
}


export function updatedLogoutStatus() {
	return {
		type: ActionTypes.UPDATE_LOGOUT_STATUS
	};
}

export function languageSelectionSuccess(responseData) {
	return {
		type: ActionTypes.LANGUAGE_SELECTION_SUCCESS,
		responseData: responseData
	};
}



export function doLogin(params) {
	return async dispatch => {
		dispatch(apiServiceActionLoading());
		await axios.post(apiHelper.loginAPI(), params)
			.then(response => {
				dispatch(loginServiceActionSuccess(response.data.data));
			}).catch(error => {
				//alert('error')
				dispatch(apiServiceActionError(error.response.data.error));
			});
	}
}

export function getOtp(params) {
	return async dispatch => {
		dispatch(apiServiceOtpActionLoading());
		await axios.post(apiHelper.getOtpAPI(), params)
			.then(response => {

				console.debug( response )

				dispatch(getOtpSuccess(response.data));
			}).catch(error => {
				console.log(' error response::--->>' + JSON.stringify(error))
				dispatch(otpServiceError(error.response.data.error));
			});
	}
}
export function resendOtp(params) {
	return async dispatch => {
		dispatch(apiServiceActionLoading());
		await axios.post(apiHelper.resendOtpAPI(), params)
			.then(response => {
				dispatch(getOtpSuccess(response.data));
			}).catch(error => {
				dispatch(otpServiceError(error.response.data.error));
			});
	}
}

export function saveSelectedLanguage() {
	return async dispatch => {
		//dispatch(apiServiceActionLoading());
		dispatch(languageSelectionSuccess(true));
	}
}

export function resetLoginErrorMeesage() {
	return dispatch => {
		return dispatch({
			type: ActionTypes.RESET_LOGIN_ERROR_MESSAGE
		});
	};
}

export function doLogout(resetNavigation: Function) {
	return dispatch => {
		dispatch({
			type: ActionTypes.LOGOUT_SUCCESS,
			...defaultLoginState,
			resetNavigation
		});
	};
}

export function enableLogin() {
	return dispatch => {
		return dispatch({
			type: ActionTypes.ENABLE_LOGIN_SUCCESS
		});
	};
}

export function updateprofileUpdateStatus() {
	return {
		type: ActionTypes.UPDATE_PROFILE_UPDATE_STATUS
	};
}

export function chatSupportstatus() {
	return {
		type: ActionTypes.CHAT_SUPPORT_STATUS
	};
}




export function updateProfile(response, userData) {
	return async dispatch => {
		dispatch(updateProfileSuccess(response, userData));
	}
}

export function chatSupport( params, token) {
	return async dispatch => {
		dispatch(apiServiceActionLoading());
		await axios.post(apiHelper.ChatAPI(), params, apiHelper.getAPIHeader(token))
			.then(response => {
				console.log('updated::' + JSON.stringify(response.data))
				 dispatch(chatActionSuccess(response.data));
			}).catch(error => {
				console.log('error')
				 dispatch(apiServiceActionError(error.response.data.error));
			});
	}
}

export function setCurrentScreen(currentScreen) {
	return {
		type: ActionTypes.SET_CURRENT_SCREEN,
		responseData: currentScreen
	};
}

export function resetError() {
	return {
		type: ActionTypes.RESET_ERROR
	};
}
