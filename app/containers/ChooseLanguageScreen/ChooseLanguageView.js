import React from "react";
import PropTypes from "prop-types";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ImageBackground,
  Platform,
} from "react-native";
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";
import { images, styles } from "./styles";

const ChooseLanguageView = (props) => {
  const { selectLanguage } = props;
  return (
    <View style={styles.screenMain}>
      <ImageBackground style={{ flex: 1 }} source={images.blurImage}
        blurRadius={ Platform.OS == 'ios' ? 10 : 3 }  
      >
        <View style={styles.imageContainer}>
          <Image source={images.driverImage} style={styles.logo} />
        </View>

        <View style={styles.languageContainer}>
          <View style={{ alignItems: "center" }}>
            <View>
              <Text style={styles.chooseText}>
                {appTexts.SELECT_LANGUAGE.Chooseyourlanguage}
              </Text>
            </View>
            <View style={styles.borderView} />
          </View>
          <View style={styles.buttonWrapper}>
            <TouchableOpacity
              style={styles.engButton}
              onPress={() => {
                selectLanguage("EN");
              }}
            >
              <Text style={styles.engText}>{appTexts.SELECT_LANGUAGE.En}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.arButton}
              onPress={() => {
                selectLanguage("AR");
              }}
            >
              <Text style={styles.arText}>{appTexts.SELECT_LANGUAGE.AR}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ImageBackground>
    </View>
  );
};

ChooseLanguageView.propTypes = {
  selectLanguage: PropTypes.func,
};

export default ChooseLanguageView;
