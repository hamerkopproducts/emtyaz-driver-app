import { StyleSheet } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import globals from "../../lib/globals";
let headerButtonContainerWidth =
  globals.SCREEN_SIZE.width - globals.MARGIN.marginTen * 2;
let headerButtonWidth =
  (headerButtonContainerWidth - globals.MARGIN.marginTen * 2) / 3;
const images = {
  driverImage: require("../../assets/images/chooseLanguage/Driver.png"),
  blurImage: require("../../assets/images/chooseLanguage/blur.png")
};

const styles = StyleSheet.create({
  screenMain:{
      flex:1,
      backgroundColor: globals.COLOR.screenBackground
  },
  screenMainContainer:{
    position: 'absolute',
    top: 0,
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
    alignItems: 'center',
    justifyContent: 'center'
  },
  bgImage: {
    position: 'absolute',
    top: 0,
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height
  },
  imageContainer: {
    flex: 0.4,
    //backgroundColor:'red',
    // width: globals.SCREEN_SIZE.width,
    // height: globals.SCREEN_SIZE.height,
    alignItems: 'center',
    paddingTop: hp('5%')
    //paddingTop:'10%'

  },
  languageContainer: {
    flex: 1,
    //backgroundColor:'green',
    // width: globals.SCREEN_SIZE.width,
    // height: globals.SCREEN_SIZE.height,
    paddingTop: hp('16%')
    //alignItems: 'center',
    //paddingTop:'25%'

  },
  screenContainer: {
    flex: 1,
    backgroundColor: globals.COLOR.transparent,
    marginBottom: globals.INTEGER.screenBottom
  },
  logo: {
    width: 100,
    height: 100,
    resizeMode: 'contain'
  },
  chooseText: {
    // color:'black',
    // fontSize:16
    //fontSize: hp('2.4%'),
    fontSize:15,
    fontFamily: globals.FONTS.avenirMedium,
    color: "#1d2226"
  },
  borderView: {
    width: "16%",
    height: 2,
    //borderRadius: 5,
    marginTop: 10,
    marginBottom: 5,
    backgroundColor: "#ff8001",
    alignItems: 'center', justifyContent: 'center'
  },
  buttonWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    //paddingTop: '10%',
    paddingTop: hp('3%'),
    paddingLeft: '6%',
    paddingRight: '6%'
  },
  engButton: {
    width: 142,
    height: 50,
    borderRadius: 15,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: 'white',
    borderColor: '#d0d0d0',
    borderWidth: 1,
    marginRight:10
  },
  arButton: {
    marginLeft: 10,
    width: 142,
    height: 50,
    borderRadius: 15,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: '#ff8001',
  },
  buttononview: {
    alignItems: "center",
    justifyContent: "center",
    //marginTop: hp('2.5%'),

  },
  engText: {
    color: '#ff8001',
    fontFamily: globals.FONTS.avenirRoman,
    fontSize: hp('2.2%'),
  },
  arText: {
    color: 'white',
    fontFamily: globals.FONTS.avenirRoman,
    fontSize: hp('2.2%'),
  },
 blurImage:{
     width:'100%',
      height:150,
      
     //resizeMode:'contain',
     alignSelf:'center'
    //paddingLeft:'5%',transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]
    
  },
});

export { images, styles };
