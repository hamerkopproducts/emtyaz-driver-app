import React, { useEffect, useState } from 'react';

import NetInfo from "@react-native-community/netinfo";
import { connect } from 'react-redux';
import ScanView from './ScanView';
import { bindActionCreators } from "redux";
import * as loginActions from "../../actions/loginActions";
import appTexts from "../../lib/appTexts";
import functions from "../../lib/functions"


const ScanScreen = (props) => {

  

  //will focus
  useEffect(() => {
    return props.navigation.addListener('focus', () => {
      if (props.currentScreen !== 'Scan') {
        props.setCurrentScreen('Scan')
      }
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
  
        } else {
          functions.displayAlert(null, appTexts.ALERT_MESSAGES.noInternet);
        }
      });
    });
  }, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    if (props.currentScreen !== 'Scan') {
      props.setCurrentScreen('Scan')
    }
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
      
      } else {
        functions.displayAlert(null, appTexts.ALERT_MESSAGES.noInternet);
      }
    });
  }

  //unmount
  useEffect(() => { return () => { handleComponentUnmount(); } }, []);

  const handleComponentUnmount = () => {
   
  };
 
  //updated
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {
  
  };
  
  return (
    <ScanView
      navigateToNotification={() => props.navigation.navigate('NotificationScreen') }
    />
  );

};


const mapStateToProps = (state, props) => {
  return {
    currentScreen: state.loginReducer.currentScreen
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    setCurrentScreen: loginActions.setCurrentScreen
  }, dispatch)
};

const scanScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(ScanScreen);

scanScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default scanScreenWithRedux;
