import React from 'react';
import { View, Text } from 'react-native';
import globals from "../../lib/globals";
import { styles } from "./styles";

import Header from "../../components/Header";

const ScanView = (props) => {
	const {
		navigateToNotification
	} = props;

	return (
	
				<View style={styles.screenMain}>
			<Header
				isLogoRequired={true}
				isNotificationRequired={true}
				isLanguageButtonRequired={true}
				customHeaderStyle={{
					height: globals.INTEGER.headerHeight,
					alignItems: "center",
					backgroundColor: globals.COLOR.headerColor
				}}
				onRightButtonPress={navigateToNotification}
			/>
					<View style={styles.screenContainer}>
						
					</View>
				</View>
			

	);
};

ScanView.propTypes = {
	
};

export default ScanView;
