import React, { useEffect, useState } from "react";

import NetInfo from "@react-native-community/netinfo";
import { connect } from "react-redux";
import NotificationView from "./NotificationView";
import { bindActionCreators } from "redux";
import globals from "../../lib/globals";
import functions from "../../lib/functions";

const NotificationScreen = (props) => {

  //will focus
  useEffect(() => {
    return props.navigation.addListener("focus", () => {
      NetInfo.fetch().then((state) => {
        if (state.isConnected) {
        } else {
          functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
        }
      });
    });
  }, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
      } else {
        functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
      }
    });
  };

  //unmount
  useEffect(() => {
    return () => {
      handleComponentUnmount();
    };
  }, []);

  const handleComponentUnmount = () => {};

  //updated
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {};

  return (
    <NotificationView
      onBackClick={() => props.navigation.goBack()}
      navigateTochat={(id) => {
        props.navigation.navigate('ChatScreen', {caht_id: id});
      }}
      navigateToDetail={(id) => {
        props.navigation.navigate('OrderDetailsScreen', 
          {
            'fromScreen': 'notification',
            'selectedItem': {id: id},
            'selectedTabIndex': 1
          });
      }}
    />
  );
};

const mapStateToProps = (state, props) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
    },
    dispatch
  );
};

const profileScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(NotificationScreen);

profileScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null,
});

export default profileScreenWithRedux;
