import React, { Component } from "react";
import { Button, Text, View, TouchableOpacity, Image,TextInput } from "react-native";
import Modal from "react-native-modal";
import { styles } from "./styles";
import LinearGradient from "react-native-linear-gradient";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";
import DropDownPicker from 'react-native-dropdown-picker';
export default class RejectorderPopup extends Component {
  state = {
    isModalVisible: false,
  };

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };

  render() {
    return (
      <View>
        <View style={styles.thirdwrapper}>
          <View style={styles.Signupwrapper}>
            <TouchableOpacity onPress={this.toggleModal}>
              <Text
                style={{
                  color: "#bd2453",
                  fontSize: 15,
                  justifyContent: "center",
                  textAlign: "center",
                }}
              >
                Click
              </Text>
            </TouchableOpacity>
          </View>
        </View>


        <Modal
          isVisible={this.state.isModalVisible}
          style={styles.modalMainContent}
        >
          <View style={styles.modalmainView}>
            <View style={{flexDirection:'row',justifyContent:'center'}}>
               <View
              style={{
                width: "25%",
                height: 3,
                borderRadius: 5,
                marginTop: hp('0.5%'),
                //marginBottom: hp('1%'),
                backgroundColor: 'pink',
              }}
            />
            </View>
            <View style={styles.buttonwrapper}>
              <Image
                source={require("../../assets/images/temp/close.png")}
                style={{ resizeMode: "contain", width: 30, height: 30}}
              />
             </View>
             <View style={styles.textWarpper}>
           

<Text style={styles.headText}>{appTexts.REJECT.Heading}</Text>
             </View>
             <View style={styles.textWarpper}>
<Text style={styles.cancelText}>{appTexts.REJECT.Headreason}</Text>
             </View>
             <View style={styles.formWrapper}>
               <View style={styles.subjectWrapper}>
               <View style={{ width: wp('90%') }}>
                      {/* <Text style={styles.mintext}>{min}</Text> */}
                      <DropDownPicker
                        items={[
                          { label: '45 Minssasass sass', value: '45 Minssasass sass ', },

                        ]}

                        labelStyle={{
                          fontSize: 12,
                          //fontWeight: 'bold',
                          textAlign: 'left',
                          color: 'black',
                       

                        }}
                        //arrowStyle={{marginRight: 10}}
                        defaultValue={this.state.country}
                        containerStyle={{ height:30, }}
                        style={{
                          backgroundColor: 'white',
                          // borderColor: 'green',
                          // borderWidth: 1,
                          borderTopLeftRadius: 0, borderTopRightRadius: 0,
                          borderBottomLeftRadius: 0, borderBottomRightRadius: 0,


                        }}
                        itemStyle={{
                          justifyContent: 'flex-start',
                        }}
                        dropDownStyle={{ backgroundColor: 'white', }}
                        onChangeItem={item => this.setState({
                          country: item.value
                        })}
                      />
                    </View>
                 
              
             </View>
             <View style={styles.messageWrapper}>
                 <Text style={styles.subText}>{appTexts.HELPPOPUP.Message}</Text>
                 <View style={{marginTop:15}}>
                 <TextInput style = {styles.input}
               underlineColorAndroid = "lightgray"
               //placeholder = "Password"
               //placeholderTextColor = "#9a73ef"
               autoCapitalize = "none"
              />
              </View>
             </View>
             </View>

             


            <View style={styles.buttoninsidewrapper}>
             
              <TouchableOpacity
                style={styles.buttononview}
                onPress={() => this.props.onPress()}
              >
                <LinearGradient
                  start={{ x: 0, y: 0 }}
                  end={{ x: 1, y: 2.45 }}
                  locations={[0, 3]}
                  colors={["#ff8001", "#fbc203"]}
                  style={styles.yButtonon}
                >
                   <Text
                    style={styles.yellowbuttonText
                    }
                  >
                  {appTexts.HELPPOPUP.Send}
                  </Text>
                </LinearGradient>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}
