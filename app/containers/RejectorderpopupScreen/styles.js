import { StyleSheet,I18nManager } from "react-native";
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
  } from "react-native-responsive-screen";
import globals from "../../lib/globals";
let headerButtonContainerWidth =
  globals.SCREEN_SIZE.width - globals.MARGIN.marginTen * 2;
let headerButtonWidth =
  (headerButtonContainerWidth - globals.MARGIN.marginTen * 2) / 3;
const images = {};
const styles = StyleSheet.create({
  screenMain: {
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
    flexDirection: "column",
  },
  thirdwrapper: {
    flex: 1,
    justifyContent: "flex-start",
    paddingTop: "50%",
  },
  Signupwrapper: {
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    paddingTop: "2%",
  },
  modalMainContent: {
    //justifyContent: "center",
    justifyContent:'flex-end',
    margin: 0
  },
  modalmainView: {
    backgroundColor: "white",
    //width: wp("90%"),
    padding: "4%",
    //borderRadius: 10,
    borderTopRightRadius:15,
    borderTopLeftRadius:15,
    borderColor: "rgba(0, 0, 0, 0.1)",
  },
  
  buttonwrapper: {
    flexDirection: "row",
     justifyContent:'flex-end'

  },
  buttoninsidewrapper: {
    flexDirection:  "row",
    justifyContent: "space-around",
    paddingTop: "3%",
    paddingBottom: "7%",
  },
  whiteButtonon: {
    width: 130,
    height: 42,
    borderRadius: 22,
    alignItems: "center",
    justifyContent: "center",
    borderColor:'#d0d0d0',
    borderWidth:1
    
},
yButtonon:{
  width: 140,
    height: 50,
    borderRadius: 15,
    alignItems: "center",
    justifyContent: "center",
    shadowOpacity: 0.58,
    shadowRadius: 16.00,
    elevation:5,
    // borderColor:'#d0d0d0',
    // borderWidth:1
},
buttononview: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: hp('2.5%'),
    

},
textWarpper:{
flexDirection:'row',
justifyContent:'center',
paddingTop:hp('1%'),
paddingBottom:hp('1.5%')
},
headText:{
  fontSize: hp('2%'),
  fontFamily: globals.FONTS.avenirHeavy,
},
cancelText:{
  fontSize: hp('1.6%'),
  fontFamily: globals.FONTS.avenirMedium,
  paddingLeft:'1%',
  paddingRight:'1%'
},
whitebuttonText:{
  fontSize: hp('2.1%'),
  fontFamily: globals.FONTS.avenirMedium,
  color:'#FF8001'
},
yellowbuttonText:{
  fontSize: hp('2.1%'),
  fontFamily: globals.FONTS.avenirMedium,
  color:'white'
},
subText:{
  fontSize: hp('2.1%'),
  color:"#6f6f6f",
  paddingLeft:'1%',
  fontFamily: globals.FONTS.avenirMedium,
},subjectWrapper:{
paddingTop:hp('2%'),
paddingBottom:hp('2%')
//marginTop:-3

},messageWrapper:{
  paddingTop:hp('2%'),
},
input: {
  //marginTop:-4
  // margin: 15,
  // height: 40,
  // borderColor: '#7a42f4',
  // borderWidth: 1
  fontFamily: globals.FONTS.avenirLight,
  fontSize: hp('2.2%'),
  textAlign: I18nManager.isRTL ? "right" : "left",
},
});

export { images, styles };
