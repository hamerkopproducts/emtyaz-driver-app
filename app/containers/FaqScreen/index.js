import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  I18nManager,
} from "react-native";

import {styles,images } from "./styles";
import appTexts from "../../lib/appTexts";
import {
  heightPercentageToDP as hp
} from "react-native-responsive-screen";

import ServiceWrapperAwait from '../../service/ServiceWrapperAwait';
import Loader from "../../components/Loader";
import Accordian from './Accordian';

export default class FaqScreen extends Component {

  _isMounted = false;

  constructor(props) {
    super(props);
    this.state = {
      faqData: [],
      loader: false,
    }
  }

  componentDidMount() {
    this._isMounted = true;
    this.faqData();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  faqData = async () => {
    const sAsyncWrapper = new ServiceWrapperAwait();
    this.setState({loader: true});
    const response = await sAsyncWrapper.get('driver/faq', 
      {language_attach: false, is_auth_required: false}
    );
    try {
      if (typeof response.success != 'undefined' && response.success == true) {
        let data = [...response.data];
        if (this._isMounted) {
          this.setState({
            faqData: data,
            loader: false
          });
        }
      }
    } catch (err) {
      this.setState({loader: false});
      console.debug('-->err', err);
    }
  }

  renderAccordians=()=> {
    const items = [];
    for (item of this.state.faqData) {
      const lang = I18nManager.isRTL ? 'ar' : 'en';
      let title = '';
      try {
        title = item.lang[lang].question;
      } catch(err) {
        title = '';
      }
      let answer = '';
      try {
        answer = item.lang[lang].answer;
      } catch(err) {
        answer = '';
      }
        items.push(
            <Accordian 
                title = {title}
                data = {answer}
            />
        );
    }
    return items;
  }

  render() {
    return (
      <View style={styles.screenMain}>

        {this.state.loader && <Loader />}

        <View style={styles.headerWrapper}>
          <TouchableOpacity
            style={styles.imageWrapper}
            onPress={() => {
              this.props.navigation.goBack();
            }}
          >
            <Image
              source={require("../../assets/images/header/back.png")}
              style={styles.arrowImage}
            />
          </TouchableOpacity>
          <View style={styles.verifyWrapper}>
            <Text style={styles.verifyText}>
              {" "}
              {appTexts.PROFILELISTING.Faq}
            </Text>
          </View>
        </View>
        <View style={{ height: hp(".5%"), backgroundColor: "#f8f8f8" }}></View>
        <View style={styles.formWrapper}>
          { this.renderAccordians() }
        </View>
      </View>
    );
  }
}
