import { StyleSheet,I18nManager } from "react-native";
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
  } from "react-native-responsive-screen";
import globals from "../../lib/globals";
let headerButtonContainerWidth =
  globals.SCREEN_SIZE.width - globals.MARGIN.marginTen * 2;
let headerButtonWidth =
  (headerButtonContainerWidth - globals.MARGIN.marginTen * 2) / 3;
  const images = {
    up:require('../../assets/images/profileicon/up-arrow.png'),
    down:require('../../assets/images/profileicon/down-arrow.png'),
  };
  
const styles = StyleSheet.create({
  screenMain: {
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
    flexDirection: "column",
  },
  fq:{
    height:20,
    width:20,
  },
  imageContainer:{
    flex:0.4,
    //backgroundColor:'red',
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
    alignItems: 'center',
    paddingTop:'10%'
    
  },
  languageContainer:{
    flex:1,
    //backgroundColor:'green',
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
    //alignItems: 'center',
    paddingTop:'25%'
    
  },
  screenContainer:{
    flex:1,
    backgroundColor: globals.COLOR.transparent,
    marginBottom: globals.INTEGER.screenBottom
  },
  headerWrapper:{
   flex:.1,
   backgroundColor:'white',
   flexDirection:'row',
   alignItems:'center'

  },
  formWrapper:{
flex:1,
paddingTop:hp('0.4%'),
backgroundColor:'white',
alignItems:'center',
justifyContent:'center'
// marginLeft:'1%',
// marginRight:'1%'
  },
  sendPhone:{
    flexDirection:'row',
    paddingTop:'7%'
   },
   otpWrapper:{
    flexDirection: "row",
    justifyContent: "space-around",
    width: "80%",
    paddingTop: 5,
   },  engButton: {
    width: 142, 
    height:50,
    borderRadius: 15,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor:'#ff8001',
    
},
  borderView:{ 
    width: "12%",
    height: 2,
    //borderRadius: 5,
    marginTop: 10,
    marginBottom: 5,
    backgroundColor: "#ff8001",
    alignItems:'center',justifyContent:'center'
  },
  textInput:{
    paddingLeft: 0,
    height: 40,
    fontSize: 13,
    width: "15%",
    borderColor: "gray",
    borderBottomWidth: 1.5,
    textAlign: "center",
  },
      verifyText: {
        fontSize:16,
        color:'#000000',
        //fontFamily: globals.FONTS.avenirMedium,
        fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.avenirMedium,
},
verifyWrapper:{
 paddingLeft:'20%'
  
},
editText:{
  color:'#ff8001',
  paddingLeft:'6%',
  fontSize:16
},
imageWrapper: {
   paddingLeft:'5%'

},
arrowImage:{
  width:45,
  height:45,
  resizeMode:'contain',
  paddingLeft:'5%',
  transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]
  
},
digitCode:{
  fontSize:17
},
phoneNumber:{
  fontSize:15
},
enterDigit:{
  paddingTop:'25%'
},
reseondCode:{
  paddingTop:'9%'
},
buttonWrapper:{
  paddingTop:'20%'
},
submitText:{
  color:'white'
},
contentWrapper:{
  paddingLeft:'5%',
  paddingRight:'2%',
  paddingTop:hp('2%')
},
headingText:{
  fontSize:hp('2.1%'),
  color:'#232020',
  fontFamily: globals.FONTS.avenirMedium,
},descriptionText:{
  paddingTop:hp('1.5%'),
  fontSize:hp('1.8%'),
  color:'#232020',
  lineHeight:22,
  fontFamily: globals.FONTS.avenirMedium,
},
renderContents:{
  //backgroundColor: "#e3f1f1",
          //padding: 10,
          // borderBottomColor:'red',
          // borderBottomWidth:1
          //fontStyle: "italic",
          //fontFamily: globals.FONTS.avenirMedium,
          fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.avenirMedium,
          paddingTop:'4%',
          paddingRight:'4%',
          paddingBottom:'4%',
          paddingLeft:'4%',
          color:'#707070',
          fontSize:13,
          lineHeight:22,
          textAlign: 'left'
},
renderHeadings:{
  flexDirection: "row",
  //padding: 10,
  justifyContent: "space-between",
  alignItems: "center",
  //backgroundColor: "white",
   borderBottomColor:'lightgray',
   borderBottomWidth:.5,
  //width: "90%",
  paddingTop:'2%',
  //paddingRight:'2%',
  //marginTop:'3%',
  paddingBottom:'2%',
  marginLeft:'3.3%',
  marginRight:'3.3%'
  // paddingLeft:'2%'
},
rendercontentText:{
  textAlign: 'left',
  //fontFamily: globals.FONTS.avenirMedium,
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.avenirMedium,
   width: '95%',color:"#232020",fontSize:14,paddingTop:hp('1%'),paddingBottom:hp('1.1%'),
   textAlign: 'left'
   

},
arrowI:{
  fontSize: 18,
  transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]
}
});

export { images, styles };