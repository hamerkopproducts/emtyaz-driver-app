import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  Text,
  Image,
  LayoutAnimation,
  Platform,
  UIManager,
} from "react-native";
import { styles, images } from "./styles";
import HTML from "react-native-render-html";

export default class Accordian extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props.data,
      expanded: false,
    };

    if (Platform.OS === "android") {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  render() {
    const { expanded } = this.state;
    return (
      <View style={{flex: 1}}>
        <TouchableOpacity
          ref={this.accordian}
          onPress={() => this.toggleExpand()}
        >
          <View style={styles.renderHeadings}>
            <Text style={styles.rendercontentText}> { this.props.title } </Text>
            {expanded ? (
            <Image style={styles.fq} source={images.up}/>
            ) : (
            <Image style={styles.fq} source={images.down}/>
            )}
        </View>
        </TouchableOpacity>
        {this.state.expanded && (
            // <Text style={styles.renderContents}>{ this.props.data }</Text>
            <HTML containerStyle={styles.renderContents} source={{ html: this.props.data }} />
        )}
      </View>
    );
  }

  toggleExpand = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({ expanded: !this.state.expanded });
  };
}
