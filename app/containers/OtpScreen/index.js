import React, { useState, useEffect } from 'react';
import { BackHandler,I18nManager,Keyboard } from 'react-native';
import { connect } from 'react-redux';
import OtpView from './OtpView';
import { bindActionCreators } from "redux";

import * as loginActions from "../../actions/loginActions";
import NetInfo from "@react-native-community/netinfo";
import functions from "../../lib/functions";
import appTexts from "../../lib/appTexts";
import messaging from "@react-native-firebase/messaging";

const OtpScreen = (props) => {

  //Initialising states
  const [otp1, setOtp1] = useState('');
  const [otp2, setOtp2] = useState('');
  const [otp3, setOtp3] = useState('');
  const [otp4, setOtp4] = useState('');
  const [fcm_token, setFcmToken] = useState('');

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    requestUserPermission();
    BackHandler.addEventListener('hardwareBackPress', backPressed = () => {
      BackHandler.exitApp();
      return true;
    });
  };

  const requestUserPermission = async () => {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    if (enabled) {
      getFcmToken();
    }
  }

  const getFcmToken = async () => {
    const fcmToken = await messaging().getToken();
    if (fcmToken) {
      setFcmToken(fcmToken)
    } else {
      console.log("Failed", "No token received");
    }
  }

  //updated
  useEffect(() => handleComponentUpdated());
  const handleComponentUpdated = () => {
    if (props.otpAPIReponse && props.otpAPIReponse.success) {
      props.getOtpSuccess(null);
      functions.displayToast('success', 'top', appTexts.ALERT_MESSAGES.success, appTexts.ALERT_MESSAGES.otpSendSuccess + ' ' + props.route.params.mobileNumber);
    }else if (props.error && props.error.msg) {
      functions.displayToast('error', 'top', appTexts.ALERT_MESSAGES.error, props.error.msg);
      props.resetError();
    }
    if (props.isLogged) {
      props.navigation.navigate('TabNavigator')
    }
  };

  const loginButtonPress = () => {
    if (otp1.trim() === '' || otp2.trim() === '' || otp3.trim() === '' || otp4.trim() === '') {
      functions.displayToast('error', 'top', appTexts.ALERT_MESSAGES.error, appTexts.ALERT_MESSAGES.specifyOtp);
    }
    else{
      Keyboard.dismiss()
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        let apiParam = {
          "phone": props.route.params.mobileNumber,
          "otp": otp1 + otp2 + otp3 + otp4,
          "fcm_token": fcm_token
        }
        props.doLogin(apiParam);
      } else {
        functions.displayToast('error', 'top', appTexts.ALERT_MESSAGES.error, appTexts.ALERT_MESSAGES.noInternet);
      }
    });
  }
  };
  const resendButtonPress = () => {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        let apiParam = {
          "language": I18nManager.isRTL ? 'ar' : 'en',
          "phone": props.route.params.mobileNumber
        }
        props.resendOtp(apiParam);
      } else {
        functions.displayToast('error', 'top', appTexts.ALERT_MESSAGES.error, appTexts.ALERT_MESSAGES.noInternet);
      }
    });
  }
  
  const onBackButtonPress = () => {
    props.navigation.goBack();
  };
  return (
    <OtpView 
    loginButtonPress={loginButtonPress} 
      resendButtonPress={resendButtonPress} 
      onBackButtonPress={onBackButtonPress} 
      otp1={otp1} 
      otp2={otp2}
      otp3={otp3}
      otp4={otp4}
      setOtp1={setOtp1}
      setOtp2={setOtp2}
      setOtp3={setOtp3}
      setOtp4={setOtp4}
      isLoading={props.isLoading}
      phoneNumber={props.route.params.mobileNumber}
    />
  );

};


const mapStateToProps = (state, props) => {
  return {
    isLogged: state.loginReducer.isLogged,
    isLoading: state.loginReducer.isLoading,
    otpAPIReponse: state.loginReducer.otpAPIReponse,
    error: state.loginReducer.error
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    doLogin: loginActions.doLogin,
    resendOtp: loginActions.resendOtp,
    getOtpSuccess: loginActions.getOtpSuccess,
    resetError: loginActions.resetError
  }, dispatch)
};

const otpScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(OtpScreen);

otpScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default otpScreenWithRedux;
