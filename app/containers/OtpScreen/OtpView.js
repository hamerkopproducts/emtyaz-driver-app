import React, { useRef } from "react";
import PropTypes from "prop-types";
import {
  Button,
  Text,
  View,
  TouchableOpacity,
  Image,
  StatusBar,
  TextInput,
  Keyboard
} from "react-native";
import Modal from "react-native-modal";
import { styles } from "./styles";
import LinearGradient from "react-native-linear-gradient";
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";
import Loader from "../../components/Loader";

const LoginView = (props) => {
  const otp1Input = useRef(null);
  const otp2Input = useRef(null);
  const otp3Input = useRef(null);
  const otp4Input = useRef(null);
  const { 
    loginButtonPress,
    onBackButtonPress,
    resendButtonPress,
    otp1,
    otp2,
      otp3,
      otp4,
      setOtp1,
      setOtp2,
      setOtp3,
      setOtp4,
    isLoading,
    phoneNumber
   } = props;
  return (
    <View style={styles.screenMain}>
      <StatusBar
        barStyle="dark-content"
        backgroundColor={globals.COLOR.screenBackground}
      />
      {isLoading && <Loader />}
      <View style={styles.headerWrapper}>
        <TouchableOpacity style={styles.imageWrapper} onPress={()=>{onBackButtonPress()}}>
          <Image
            source={require("../../assets/images/header/back.png")}
            style={styles.arrowImage}
          />
        </TouchableOpacity>
        <View style={styles.verifyWrapper}>
          <Text style={styles.verifyText}>{appTexts.OTP.Heading}</Text>
        </View>
      </View>
      <View style={styles.formWrapper}>
        <View style={styles.enterDigit}>
          <Text style={styles.digitCode}>{appTexts.OTP.Enter}</Text>
        </View>

        <View style={styles.sendPhone}>
          <Text style={styles.phoneNumber}>{"+966-"+phoneNumber}</Text>
          {/* <Text style={styles.editText}>{appTexts.OTP.Edit}</Text> */}
        </View>
        <View style={styles.verificationCode}>
          <Text style={styles.verificationcodeText}>
            {appTexts.OTP.Verificationcode}
          </Text>
          {/* <Text style={styles.editText}>{appTexts.OTP.Edit}</Text> */}
        </View>
        <View style={styles.otpWrapper}>
          <TextInput
            maxLength={1}
            style={styles.textInput}
            placeholderTextColor="#282828"
            keyboardType="number-pad"
            autoFocus={true}
            ref={otp1Input}
            value={otp1}
            onChangeText={val => {
              setOtp1(val);
              if (val != '') {
                otp2Input.current.focus();
              }
            }}
          />
          <TextInput
            maxLength={1}
            style={styles.textInput}
            placeholderTextColor="#282828"
            keyboardType="number-pad"
            ref={otp2Input}
            value={otp2}
            onChangeText={val => {
              setOtp2(val);
              if (val != '') {
                otp3Input.current.focus();
              }
            }}
            onKeyPress={({ nativeEvent }) => {
              if (nativeEvent.key === 'Backspace' && otp2 == '') {
                otp1Input.current.focus();
              }
            }}
          />
          <TextInput
            maxLength={1}
            style={styles.textInput}
            placeholderTextColor="#282828"
            keyboardType="number-pad"
            ref={otp3Input}
            value={otp3}
            onChangeText={val => {
              setOtp3(val);
              if (val != '') {
                otp4Input.current.focus();
              }
            }}
            onKeyPress={({ nativeEvent }) => {
              if (nativeEvent.key === 'Backspace' && otp3 == '') {
                otp2Input.current.focus();
              }
            }}
          />
          <TextInput
            maxLength={1}
            style={styles.textInput}
            placeholderTextColor="#282828"
            keyboardType="number-pad"
            ref={otp4Input}
            value={otp4}
            onChangeText={val => {
              setOtp4(val);
              if (val != '') {
                Keyboard.dismiss();
              }
            }}
            onKeyPress={({ nativeEvent }) => {
              if (nativeEvent.key === 'Backspace' && otp4 == '') {
                otp3Input.current.focus();
              }
            }}
          />
        </View>
        <View style={styles.reseondCode}>
          <Text style={styles.resedText}>{appTexts.OTP.Resend}</Text>
          <TouchableOpacity
            onPress={() => {
              resendButtonPress();
            }}
          >
          <Text style={styles.resedtextOrange}>{appTexts.OTP.Resends}</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.buttonWrapper}>
          <TouchableOpacity
            onPress={() => {
              loginButtonPress();
            }}
          >
            <View style={styles.engButton}>
              <Text style={styles.submitText}>{appTexts.OTP.Submit}</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

LoginView.propTypes = {
  loginButtonPress: PropTypes.func,
  onBackButtonPress: PropTypes.func,
  resendButtonPress: PropTypes.func,
  otp1: PropTypes.string,
  otp2: PropTypes.string,
  otp3: PropTypes.string,
  otp4: PropTypes.string,
  setOtp1: PropTypes.func,
  setOtp2: PropTypes.func,
  setOtp3: PropTypes.func,
  setOtp4: PropTypes.func,
  isLoading:PropTypes.bool,
  phoneNumber: PropTypes.string
};

export default LoginView;
