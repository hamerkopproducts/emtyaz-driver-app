import { StyleSheet,I18nManager } from "react-native";
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
  } from "react-native-responsive-screen";
import globals from "../../lib/globals";
let headerButtonContainerWidth =
  globals.SCREEN_SIZE.width - globals.MARGIN.marginTen * 2;
let headerButtonWidth =
  (headerButtonContainerWidth - globals.MARGIN.marginTen * 2) / 3;
const images = {};
const styles = StyleSheet.create({
  screenMain: {
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
    flexDirection: "column",
  },
  imageContainer:{
    flex:0.4,
    //backgroundColor:'red',
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
    alignItems: 'center',
    paddingTop:'10%'
    
  },
  languageContainer:{
    flex:1,
    //backgroundColor:'green',
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
    //alignItems: 'center',
    paddingTop:'25%'
    
  },
  screenContainer:{
    flex:1,
    backgroundColor: globals.COLOR.transparent,
    marginBottom: globals.INTEGER.screenBottom
  },
  headerWrapper:{
   flex:.1,
   paddingTop:hp('3%'),
   backgroundColor:'white',
   flexDirection:'row',
   alignItems:'center'

  },
  formWrapper:{
flex:1,
backgroundColor:'white',
alignItems:'center'
  },
  sendPhone:{
    flexDirection:'row',
    //paddingTop:'7%'
    paddingTop:hp('1.5%')
   },
   verificationCode:{
    paddingTop:hp('2.2%')
   },
   otpWrapper:{
    flexDirection: "row",
    justifyContent: "space-around",
    width: "80%",
    paddingTop:hp('3.4%'),
    transform: [{ scaleX: I18nManager. isRTL ? -1 : 1 }],
   
   },  engButton: {
    width: 142, 
    height:50,
    borderRadius: 15,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor:'#ff8001',
    
},
resedText:{
  fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic :  globals.FONTS.avenirMedium,
  fontSize:hp('2.1%'),
  //color:"#40475a"
  color:"gray"
},
resedtextOrange:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic :  globals.FONTS.avenirMedium,
  fontSize:hp('2.1%'),
  //color:"#40475a"
  color:"#ff8001",
  paddingLeft:'3%',
  paddingRight:'3%'
},
  borderView:{ 
    width: "12%",
    height: 2,
    //borderRadius: 5,
    marginTop: 10,
    marginBottom: 5,
    backgroundColor: "#ff8001",
    alignItems:'center',justifyContent:'center'
  },
  textInput:{
    paddingLeft: 0,
    height: 50,
    //fontSize: 13,
    fontSize:hp('3.3%'),
    width: "17%",
    borderColor: "#c1c1c1",
    borderBottomWidth: 1.7,
    textAlign: "center",
    fontFamily: globals.FONTS.avenirHeavy,
    transform: [{ scaleX: I18nManager. isRTL ? -1 : 1 }],
    //fontFamily: globals.FONTS.avenirLight,
  },
    verifyText: {
      fontSize:16,
      //fontSize:hp('2.4%'),
      color:'#232020',
      fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.avenirMedium,
},
verifyWrapper:{
 paddingLeft:'10%'
  
},
editText:{
  color:'#ff8001',
  paddingLeft:'6%',
  paddingRight:'6%',
  //fontSize:16,
  fontFamily: globals.FONTS.avenirHeavy,
  fontSize:hp('2.2%'),
},
imageWrapper: {
   paddingLeft:'6%'

},
arrowImage:{
  width:45,
  height:45,
  resizeMode:'contain',
  paddingLeft:'5%',transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]
  
},
digitCode:{
  //fontSize:hp('2.4%'),
  fontSize:15,
  color:'#232020',
  //fontFamily: globals.FONTS.avenirHeavy,
  fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.avenirMedium,
},
phoneNumber:{
  fontFamily: globals.FONTS.avenirMedium,
  //fontSize:hp('2.1%'),
  fontSize:14,
  color:'black'
},
verificationcodeText:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic :  globals.FONTS.avenirMedium,
  fontSize:hp('2.1%'),
  color:'gray'
},
enterDigit:{
  //paddingTop:'25%'
  paddingTop:hp('20%')
},
reseondCode:{
  //paddingTop:'9%'
  paddingTop:hp('6%'),
  flexDirection:'row'
},
buttonWrapper:{
  //paddingTop:'20%'
  paddingTop:hp('7%')
},
submitText:{
  color:'white',
  fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.avenirMedium,
  fontSize:hp('2.2%')
}
});

export { images, styles };
