import React from "react";
import PropTypes from "prop-types";
import { View, FlatList, Text } from "react-native";
import globals from "../../lib/globals";
import { styles } from "./styles";
import Header from "../../components/Header";
import TopTab from "../../components/TopTab";
import ListItemCard from "../../components/ListItemCard";
import DateTimePicker from "../../components/DateTimePicker";
import appTexts from "../../lib/appTexts";
import Loader from "../../components/Loader";
const MyOrdersView = (props) => {
  const {
    tabIndex,
    listData,
    firstTabCount,
    secondTabCount,
    onListItemPress,
    onTabChange,
    onCalendarFilterPress,
    isShowDatePicker,
    selectedDate,
    filterDate,
    setSelectedDate,
    cancelClick,
    doneClick,
    isLoading,
    navigateToNotification,
    refreshing,
    refresh,
    clearFilter,
  } = props;

  return (
    <View style={styles.screenMain}>
      <Header
        isLogoRequired={true}
        isNotificationRequired={true}
        isLanguageButtonRequired={true}
        customHeaderStyle={{
          height: globals.INTEGER.headerHeight,
          alignItems: "center",
          backgroundColor: globals.COLOR.headerColor,
        }}
        onRightButtonPress={navigateToNotification}
      />

      {isLoading && <Loader />}
      <View style={styles.screenDesignContainer}>
        <TopTab
          tabIndex={tabIndex}
          firstTabText={appTexts.STRING.delivered}
          secondTabText={appTexts.STRING.returned}
          firstTabCount={firstTabCount}
          secondTabCount={secondTabCount}
          onTabChange={onTabChange}
          isCalenderRequired={true}
          onCalendarFilterPress={onCalendarFilterPress}
          filterDate={filterDate}
          clearFilter={clearFilter}
        />
        <View style={styles.screenContainerWithMargin}>
          <FlatList
            ListEmptyComponent={
              <View style={styles.noDataContainer}>
                <Text style={styles.noDataText}>{appTexts.STRING.nodata}</Text>
              </View>
            }
            style={styles.flatListStyle}
            data={listData}
            extraData={listData}
            refreshing={refreshing}
            onRefresh={refresh}
            /*onEndReachedThreshold={0.5}
						onEndReached={({ distanceFromEnd }) => {
							if (listData.length >= (currentPage * pageLimit)) {
								loadMoreItems();
							}
						}}*/
            keyExtractor={(item) => item.id.toString()}
            showsVerticalScrollIndicator={false}
            renderItem={({ item, index }) => (
              <ListItemCard
                tabIndex={tabIndex}
                itemData={item}
                itemOnPress={onListItemPress}
              />
            )}
          />
        </View>
        <DateTimePicker
          selectedDate={selectedDate != "" ? selectedDate : new Date()}
          setSelectedDate={setSelectedDate}
          mode={"date"}
          isShowDatePicker={isShowDatePicker}
          cancelClick={cancelClick}
          doneClick={doneClick}
        />
      </View>
    </View>
  );
};

MyOrdersView.propTypes = {
  tabIndex: PropTypes.number,
  listData: PropTypes.array,
  firstTabCount: PropTypes.number,
  secondTabCount: PropTypes.number,
  onListItemPress: PropTypes.func,
  onCalendarFilterPress: PropTypes.func,
  isShowDatePicker: PropTypes.bool,
  filterDate: PropTypes.any,
  selectedDate: PropTypes.any,
  setSelectedDate: PropTypes.func,
  cancelClick: PropTypes.func,
  doneClick: PropTypes.func,
  isLoading: PropTypes.bool,
  currentPage: PropTypes.number,
  pageLimit: PropTypes.number,
  loadMoreItems: PropTypes.func,
};

export default MyOrdersView;
