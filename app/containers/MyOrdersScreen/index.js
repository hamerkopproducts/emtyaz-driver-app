import React, { useEffect, useState } from 'react';

import NetInfo from "@react-native-community/netinfo";
import { connect } from 'react-redux';
import MyOrdersView from './MyOrdersView';
import { bindActionCreators } from "redux";

import * as orderActions from "../../actions/orderActions";
import * as loginActions from "../../actions/loginActions";
import appTexts from "../../lib/appTexts";
import functions from "../../lib/functions"
import moment from 'moment';

const MyOrdersScreen = (props) => {

  //Initialising states
  const [tabIndex, setTabIndex] = useState(0);
  const [showDatePicker, setShowDatePicker] = useState(false);
  const [date, setDate] = useState(new Date())
  const [filterDate, setFilterDate] = useState(new Date())
  const [refreshing, setRefreshing] = useState(false);

  //will focus
  useEffect(() => {
    return props.navigation.addListener('focus', () => {
      if (props.currentScreen !== 'MyOrders') {
        props.setCurrentScreen('MyOrders')
      }
      NetInfo.fetch().then(state => {
          if (state.isConnected) {
            setDate(new Date());
            setFilterDate(new Date());
            props.getDeliveredOrders(0, moment(new Date()).format('DD-MM-YYYY'), props.userData.access_token)
            props.getReturnedOrders(0, moment(new Date()).format('DD-MM-YYYY'), props.userData.access_token)
          } else {
            functions.displayToast('error', 'top', appTexts.ALERT_MESSAGES.error, appTexts.ALERT_MESSAGES.noInternet);
          }
      });
    });
  }, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    if (props.currentScreen !== 'MyOrders') {
      props.setCurrentScreen('MyOrders')
    }
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        props.getDeliveredOrders(0, moment(new Date()).format('DD-MM-YYYY'),props.userData.access_token)
        props.getReturnedOrders(0, moment(new Date()).format('DD-MM-YYYY'),props.userData.access_token)
      } else {
        functions.displayToast('error', 'top', appTexts.ALERT_MESSAGES.error, appTexts.ALERT_MESSAGES.noInternet);
      }
    });
  }

  //unmount
  useEffect(() => { return () => { handleComponentUnmount(); } }, []);

  const handleComponentUnmount = () => {

  };

  //updated
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {

  };
  const onListItemPress = (itemData) => {
    props.navigation.navigate('OrderDetailsScreen', { 'fromScreen': 'orders', 'selectedItem': itemData, 'selectedTabIndex': tabIndex });
  };
  const onTabChange = (index) => {
    if (tabIndex !== index) {
      setTabIndex(index);
      //setDate(new Date());
      //setFilterDate(new Date());
      let _date = '';
        if(date) {
          _date = moment(date).format('DD-MM-YYYY');
        }
      if (index === 0) {
        props.getDeliveredOrders(0, _date, props.userData.access_token)
      } else if (index === 1) {
        props.getReturnedOrders(0, _date, props.userData.access_token)
      }
    }
  };

  const refresh = () => {
    setRefreshing(true);
    setTimeout(() => {
      setRefreshing(false);
      let _date = '';
        if(date) {
          _date = moment(date).format('DD-MM-YYYY');
        }
      if (tabIndex === 0) {
        props.getDeliveredOrders(0, _date, props.userData.access_token)
      } else if (tabIndex === 1) {
        props.getReturnedOrders(0, _date, props.userData.access_token)
      }
    }, 500);
    
  };
  
  const onCalendarFilterPress = () => {
    setDate(filterDate);
    setShowDatePicker(true);
  };
  const cancelClick = () => {
    setDate(filterDate);
    setShowDatePicker(false);
  };
  const doneClick = () => {
    let newDate = date;
    if(newDate == '') {
      newDate = new Date();
      setDate(new Date());
    }
    setFilterDate(newDate);
    setShowDatePicker(false);
    let _date = '';
    if(date) {
      _date = moment(date).format('DD-MM-YYYY');
    }
    props.getDeliveredOrders(0, _date, props.userData.access_token)
    props.getReturnedOrders(0, _date, props.userData.access_token)
  };

  const clearFilter = () => {
    setFilterDate('');
    setDate('');
    props.getDeliveredOrders(0, '', props.userData.access_token)
    props.getReturnedOrders(0, '', props.userData.access_token)
  };


  return (
    <MyOrdersView
      listData={tabIndex === 0 ? 
        (props.deliveredOrders !== null && props.deliveredOrders.data !== null ? props.deliveredOrders.data : []) : 
        (props.returnedOrders !== null && props.returnedOrders.data !== null ? props.returnedOrders.data : [])}
      onListItemPress={onListItemPress}
      tabIndex={tabIndex}
      firstTabCount={props.deliveredOrders !== null && props.deliveredOrders.data !== null ? props.deliveredOrders.data.length : 0}
      secondTabCount={props.returnedOrders !== null && props.returnedOrders.data !== null ? props.returnedOrders.data.length : 0}
      onTabChange={onTabChange}
      onCalendarFilterPress={onCalendarFilterPress}
      isShowDatePicker={showDatePicker}
      selectedDate={date}
      filterDate={filterDate}
      setSelectedDate={setDate}
      cancelClick={cancelClick}
      doneClick={doneClick}
      isLoading={props.isLoading}
      navigateToNotification={() => props.navigation.navigate('NotificationScreen') }
      refreshing={refreshing}
      refresh={refresh}
      clearFilter={clearFilter}
    />
  );

};


const mapStateToProps = (state, props) => {
  return {
    deliveredOrders: state.orderReducer.deliveredOrders,
    returnedOrders: state.orderReducer.returnedOrders,
    userData: state.loginReducer.userData,
    currentScreen: state.loginReducer.currentScreen,
    isLoading: state.orderReducer.isLoading
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    getDeliveredOrders: orderActions.getDeliveredOrders,
    getReturnedOrders: orderActions.getReturnedOrders,
    setCurrentScreen: loginActions.setCurrentScreen
  }, dispatch)
};

const myOrdersScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(MyOrdersScreen);

myOrdersScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default myOrdersScreenWithRedux;
