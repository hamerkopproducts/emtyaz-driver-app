import { StyleSheet, I18nManager } from "react-native";
import globals from "../../lib/globals"

let headerButtonContainerWidth = globals.SCREEN_SIZE.width - (globals.MARGIN.marginTen*2);
let headerButtonWidth = (headerButtonContainerWidth-(globals.MARGIN.marginTen*2)) / 3;

const images = {
  
};

const styles = StyleSheet.create({
  screenMain: {
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
    flexDirection: 'column',
    backgroundColor:'white'
  },
  screenDesignContainer: {
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height
  },
  screenContainerWithMargin: {
    width: globals.INTEGER.screenWidthWithMargin,
    height: globals.INTEGER.screenContentHeight - globals.INTEGER.topTabHieght,
    marginLeft: globals.INTEGER.leftRightMargin,
  },
 containerWithMarginForCalendar: {
    width: globals.INTEGER.screenWidthWithMargin,
    height: globals.INTEGER.screenContentHeight,
    marginLeft: globals.INTEGER.leftRightMargin,
    position:'absolute',
    top:0,
    alignItems:'center',
    justifyContent:'center'
  },
  headerButtonContianer: {
    flexDirection: 'row',
    width: headerButtonContainerWidth,
    height: globals.INTEGER.heightFifty,
    alignItems: 'center'
  },
  headerButton: {
    width: headerButtonWidth,
    height: globals.INTEGER.heightThirty
  },
  headerButtonMargin: {
    marginLeft: globals.MARGIN.marginTen
  },
  categoryItemRow: {
    width: '100%',
    flexDirection: 'row'
  },
  flatListStyle: {
    width: '100%'
  },
  noDataContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 50
  },
  noDataText: {
    textAlign: 'center',
    color: globals.COLOR.lightTextColor,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.avenirLight,
    fontSize: 12
  }
});

export { images, styles };
