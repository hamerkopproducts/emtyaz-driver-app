import React, { Component } from "react";
import { Button, Text, View, TouchableOpacity, Image,TextInput } from "react-native";
import Modal from "react-native-modal";
import { styles } from "./styles";
import LinearGradient from "react-native-linear-gradient";
import appTexts from "../../lib/appTexts";
export default class HelpPopup extends Component {
  state = {
    isModalVisible: true,
  };

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };

  render() {
    return (
      <View>
        {/* <View style={styles.thirdwrapper}>
          <View style={styles.Signupwrapper}>
            <TouchableOpacity onPress={this.toggleModal}>
              <Text
                style={{
                  color: "#bd2453",
                  fontSize: 15,
                  justifyContent: "center",
                  textAlign: "center",
                }}
              >
                Click
              </Text>
            </TouchableOpacity>
          </View>
        </View> */}


        <Modal
          isVisible={this.state.isModalVisible}
          style={styles.modalMaincontentHelp}
        >
          <View style={styles.modalmainviewHelp}>
            <View style={styles.buttohelpnwrapper}>
              <Image
                source={require("../../assets/images/temp/close.png")}
                style={{ resizeMode: "contain", width: 30, height: 30}}
              />
             </View>
             <View style={styles.helptextWarpper}>
<Text style={styles.helpheadText}>{appTexts.HELPPOPUP.Heading}</Text>
             </View>
             <View style={styles.formWrapper}>
               <View style={styles.helpsubjectWrapper}>
                 <Text style={styles.helpsubText}>{appTexts.HELPPOPUP.Subject}</Text>
                 <View style={{marginTop:0}}>
<TextInput style = {styles.hinput}
               underlineColorAndroid = "lightgray"
               //placeholder = "Password"
               //placeholderTextColor = "#9a73ef"
               autoCapitalize = "none"
              />
              </View>
             </View>
             <View style={styles.helpmessageWrapper}>
                 <Text style={styles.helpsubText}>{appTexts.HELPPOPUP.Message}</Text>
                 <View style={{marginTop:15}}>
                 <TextInput style = {styles.hinput}
               underlineColorAndroid = "lightgray"
               //placeholder = "Password"
               //placeholderTextColor = "#9a73ef"
               autoCapitalize = "none"
              />
              </View>
             </View>
             </View>

             


            <View style={styles.helpbuttoninsidewrapper}>
             
              <TouchableOpacity
                style={styles.helpbuttononview}
                onPress={() => this.props.onPress()}
              >
                <LinearGradient
                  start={{ x: 0, y: 0 }}
                  end={{ x: 1, y: 2.45 }}
                  locations={[0, 3]}
                  colors={["#ff8001", "#fbc203"]}
                  style={styles.hylpyButtonon}
                >
                   <Text
                    style={styles.helpyellowbuttonText
                    }
                  >
                 {appTexts.HELPPOPUP.Send}
                  </Text>
                </LinearGradient>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>

        
      </View>
    );
  }
}
