import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  I18nManager
} from "react-native";

import { styles } from "./styles";
import appTexts from "../../lib/appTexts";
import {
  heightPercentageToDP as hp
} from "react-native-responsive-screen";

import ServiceWrapperAwait from '../../service/ServiceWrapperAwait';
import Loader from "../../components/Loader";

export default class Privacy extends Component {

  constructor(props) {
    super(props);

    this.state = {
      privacy_policy: '',
      loader: false
    }

  }

  componentDidMount() {
    this.privacy_policy();
  }

  privacy_policy = async () => {
    const sAsyncWrapper = new ServiceWrapperAwait();
    this.setState({loader: true});
    const response = await sAsyncWrapper.get('privacy-policy?type=driver',
      {language_attach: false, is_auth_required: false}
    );
    try {
      if (typeof response.success != 'undefined' && response.success == true) {
        const lang = I18nManager.isRTL ? 'ar' : 'en';
        let data = response.data.lang[lang];
        this.setState({
          privacy_policy: data,
          loader: false
        });
      }
    } catch (err) {
      this.setState({loader: true});
    }
  }

  render() {
    return (
      <View style={styles.screenMain}>

        {this.state.loader && <Loader />}

        <View style={styles.headerWrapper}>
          <TouchableOpacity
            style={styles.imageWrapper}
            onPress={() => {
              this.props.navigation.goBack();
            }}
          >
            <Image
              source={require("../../assets/images/header/back.png")}
              style={styles.arrowImage}
            />
          </TouchableOpacity>
          <View style={styles.verifyWrapper}>
            <Text style={styles.verifyText}>{appTexts.PRIVACY.Heading}</Text>
          </View>
        </View>
        <View style={{ height: hp(".5%"), backgroundColor: "#f8f8f8" }}></View>
        <View style={styles.formWrapper}>
          <View style={styles.contentWrapper}>
            <Text style={styles.headingText}>
              { this.state.privacy_policy.title}
            </Text>
            <Text style={styles.descriptionText}>
              { this.state.privacy_policy.content }
            </Text>
          </View>
        </View>
      </View>
    );
  }

}
