import { StyleSheet,I18nManager } from "react-native";
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
  } from "react-native-responsive-screen";
import globals from "../../lib/globals";
let headerButtonContainerWidth =
  globals.SCREEN_SIZE.width - globals.MARGIN.marginTen * 2;
let headerButtonWidth =
  (headerButtonContainerWidth - globals.MARGIN.marginTen * 2) / 3;
const images = {};
const styles = StyleSheet.create({
  screenMain: {
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
    flexDirection: "column",
  },
  screenContainer:{
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
    flexDirection: "column"
  },
  sliderWrapper:{
    marginTop:20,
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height -  260,
  },
  slide: {
    flex: 1,
    alignItems: "center",
  },
  image: {
    width: wp('83%'),
    height: hp('36%'),
    alignSelf: "center",
    resizeMode:'contain',
    marginTop:hp('4%')
    //marginTop:50
    //height:200,
    //flex:1,
    //flex:.5,
    //width:null
  },
  imageWrapper:{
    width: 45,
    height: 45,
    marginLeft: '5%',
    zIndex:9
    //marginTop:'10%',
  },
  arrowImage:{
    width:45,
    height:45,
    resizeMode:'contain',
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]
  },
  text: {
    color: "#707070",
    //backgroundColor: "transparent",
    textAlign: "center",
    marginLeft: "4%",
    marginRight: "4%",
    lineHeight: 24,
    marginTop:hp('3.5%'),
    //marginTop:50,
    //fontSize:13,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.avenirLight,
    //fontSize: hp('1.9%'),
    fontSize:I18nManager.isRTL ? hp('1.9%'): 13
  },
  loginformWrapper: {
    //flex: 0.4,
    height:200,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    borderTopWidth: 0.5,
    borderRightWidth: 0.5,
    borderLeftWidth: 0.5,
    backgroundColor:globals.COLOR.white,
    borderColor: "#ccc",
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.2,
    shadowRadius: 1,
    elevation: 1,
    paddingLeft: "2%",
    paddingRight: "2%",
  },
  logininsideWrapper: {
    justifyContent: "flex-end",
    alignItems: "center",
    paddingTop:hp('1.6%'),
    //paddingBottom:hp('1.5%')
  },
  loginText: {
    //paddingTop: "5%",
    //fontSize: 16,
    paddingTop:hp('2'),
    // fontSize: hp('2.1%'),
    paddingLeft:'5%',
    paddingRight:'5%',
    textAlign:'center',
    //fontSize:I18nManager.isRTL ?  hp('2%') : hp('2.1%'),
    fontSize:I18nManager.isRTL ? 13 : 15,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.avenirHeavy,
    // fontFamily: globals.FONTS.avenirHeavy,
  },
  formBorder: {
    flexDirection: "row",
    borderWidth: 0.5,
    borderColor: "lightgray",
    borderRadius: 30,
    marginLeft: 20,
    marginRight: 20,
    //marginTop: 22,
    marginTop:hp('3.5%')
  },
  flagWrapper: {
    width: wp('18%'),
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  textInput: {
    height: 40,
    fontSize: 14,
    //lineHeight: 9,
    alignItems: "center",
    fontFamily: globals.FONTS.avenirMedium,
    padding:'6%',
    //color:'red'
  },
  mobileWrapper: {
    borderLeftWidth: 0.5,
    borderLeftColor: "lightgray",
    marginLeft: 8,
    marginTop: 3,
    marginBottom: 3,
    flexBasis: "68%",
    
  },
  mobileformWrapper: {
    height: 40,
    fontSize: 14,
  //  fontSize: hp('2%'),
    marginLeft: 5,
    alignItems: "center",
    color:"gray",
    fontFamily: globals.FONTS.avenirLight,
    textAlign: I18nManager.isRTL ? "right" : "left",
    
  },
  submitWrapper: {
    flexBasis: "15%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    paddingRight: "2%",
  },
  submitButton: {
    width: 36,
    height: 36,
    borderRadius: 18,
    alignItems: "center",
    justifyContent: "center",
    //shadowOpacity: 0.8,
    backgroundColor: globals.COLOR.tabUnderLineColor,
  },
  nextButton:{
    resizeMode: "contain", width: 10, height: 10,transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }],
  }
});

export { images, styles };
