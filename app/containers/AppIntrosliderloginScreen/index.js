import React from "react";
import {
  View,
  Text,
  TextInput,
  SafeAreaView,
  Image,
  TouchableOpacity,
  I18nManager,
  Keyboard
} from "react-native";

import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import AppIntroSlider from "react-native-app-intro-slider";
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";

import functions from "../../lib/functions";
import { styles } from "./styles";
import { connect } from 'react-redux'
import { bindActionCreators } from "redux";

import * as loginActions from "../../actions/loginActions";
import Loader from "../../components/Loader";
import NetInfo from "@react-native-community/netinfo";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import messaging from "@react-native-firebase/messaging";

const data = [
  {
    text:
    appTexts.APP_SLIDER.Content,
    image: require("../../assets/images/chooseLanguage/bg.png"),
  },
  {
    text:
    appTexts.APP_SLIDER.Content,
    image: require("../../assets/images/chooseLanguage/bg.png"),
  },
  {
    text:
    appTexts.APP_SLIDER.Content,
    image: require("../../assets/images/chooseLanguage/bg.png"),
  },
];

class AppIntrosliderloginScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mobileNumber: ''
    };
  }
  _renderItem = ({ item }) => {
    return (
      <View
        style={[
          styles.slide,
          {
            backgroundColor: item.bg,
          },
        ]}
      >
        <SafeAreaView>
          <Image source={item.image} style={styles.image} />
          <Text style={styles.text}>{item.text}</Text>
        </SafeAreaView>
      </View>
    );
  };

  _keyExtractor = (item, index) => index.toString();

  componentDidUpdate() {
    if (this.props.otpAPIReponse && this.props.otpAPIReponse.success) {
      this.props.getOtpSuccess(null);
      this.props.navigation.navigate('OtpScreen', { mobileNumber: this.state.mobileNumber})
    }
    else if (this.props.error && this.props.error.msg){
      functions.displayToast('error', 'top', appTexts.ALERT_MESSAGES.error, this.props.error.msg);
      this.props.resetError();
    }
  }

  componentDidMount() {
    this.requestUserPermission();
  }

  requestUserPermission = async () => {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    if (enabled) {
      console.log('Authorization status:', authStatus);
      // this.getFcmToken();
    }
  }

  submitMobileNumber = () => {
    Keyboard.dismiss();
    if((this.state.mobileNumber).trim() === '' ){
      functions.displayToast('error', 'top', appTexts.ALERT_MESSAGES.error, appTexts.ALERT_MESSAGES.enterValidPhoneNumber);
    }else{
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          let apiParam = {
            "language": I18nManager.isRTL ? 'ar' : 'en',
            "phone": this.state.mobileNumber
          }
          this.props.getOtp(apiParam);
        } else {
          functions.displayToast('error', 'top', appTexts.ALERT_MESSAGES.error, appTexts.ALERT_MESSAGES.noInternet);
        }
      });
    }
  }

  render() {
   
    return (
      <View style={styles.screenMain}>
        {this.props.getOtpLoading && <Loader/>}
       
        <KeyboardAwareScrollView showsVerticalScrollIndicator={false} style={styles.screenContainer} keyboardShouldPersistTaps='always'>
        <View style={styles.sliderWrapper}>
          <TouchableOpacity style={styles.imageWrapper} onPress={() => { this.props.navigation.navigate('ChooseLanguageScreen') }}>
          <Image
            source={require("../../assets/images/header/back.png")}
            style={styles.arrowImage}
          />
        </TouchableOpacity>
        <View style={{ width: "100%", height: '100%', position:'absolute',top:0 }}>
          <AppIntroSlider
            keyExtractor={this._keyExtractor}
            renderItem={this._renderItem}
            showDoneButton={false}
            showNextButton={false}
            dotStyle={{ backgroundColor: "#8FBC8F", marginTop: hp('2.5%') }}
            activeDotStyle={{ backgroundColor: "green", marginTop: hp('2.5%') }}
            data={data}
          />
        </View>
        </View>
     
        <View style={styles.loginformWrapper}>
          <View style={styles.logininsideWrapper}>
            <Text style={styles.loginText}>{appTexts.WALKTHROUGH.Login}</Text>
            <View
              style={{
                width: "15%",
                height: 2,
                borderRadius: 5,
                marginTop: hp('1.8%'),
                //marginBottom: hp('1%'),
                backgroundColor: globals.COLOR.tabUnderLineColor,
              }}
            />
            <View style={styles.formBorder}>
              <View style={styles.flagWrapper}>
                <Image
                  source={require("../../assets/images/Icons/flag.png")}
                  style={{ height: 20, width: 20, marginLeft: 8 }}
                />
                <TextInput
                  editable={false}
                  style={styles.textInput}
                  placeholder={"+966"}
                  keyboardType="number-pad"
                  placeholderTextColor="#1d2226"
                />
              </View>
              <View style={styles.mobileWrapper}>
                <TextInput
                  style={styles.mobileformWrapper}
                  keyboardType={"number-pad"}
                  placeholder={appTexts.WALKTHROUGH.Mobile}
                  placeholderTextColor="#707070"
                  keyboardType="number-pad"
                  ref="mobileNumber"
                  value={this.state.mobileNumber}
                  onChangeText={val => {
                    this.setState({ mobileNumber: val });
                  }}
                />
              </View>
              <View style={styles.submitWrapper}>
                <TouchableOpacity onPress={() => { this.submitMobileNumber() }}>
                  <View style={styles.submitButton}>
                    <Image
                      source={require("../../assets/images/Icons/next.png")}
                      style={styles.nextButton}
                    />
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}
/**
 * Assigning the state to props
 * @param {*} state
 */
function mapStateToProps(state) {
  return {
    otpAPIReponse: state.loginReducer.otpAPIReponse,
    getOtpLoading: state.loginReducer.getOtpLoading,
    error: state.loginReducer.error,
  }
}
/**
 * Dispatch action
 * @param {*} dispatch
 */
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ 
    getOtp: loginActions.getOtp,
    getOtpSuccess: loginActions.getOtpSuccess,
    resetError: loginActions.resetError
  }, dispatch);
}
/**
 * Connecting the LoginScreen component to a Redux store
 */
export default connect(mapStateToProps, mapDispatchToProps)(AppIntrosliderloginScreen)