import React from "react";
import PropTypes from "prop-types";
import { View, FlatList, Text } from "react-native";
import globals from "../../lib/globals";
import { styles } from "./styles";

import Header from "../../components/Header";
import TopTab from "../../components/TopTab";
import ListItemCard from "../../components/ListItemCard";
import PopupModal from "../../components/PopupModal";
import appTexts from "../../lib/appTexts";
import LinkButton from "../../components/LinkButton";
import RoundButton from "../../components/RoundButton";
import Loader from "../../components/Loader";
import InAppNotificationModal from "../NotificationPopup/InAppNotificationModal";

const HomeView = (props) => {
  const {
    tabIndex,
    listData,
    firstTabCount,
    secondTabCount,
    thirdTabCount,
    popupIndex,
    onListItemPress,
    listButtonClick,
    checkBoxClick,
    onTabChange,
    popupNoClick,
    popupYesClick,
    isSelectAllViewVisible,
    selectAllClick,
    isLoading,
    isStatusUpdateLoading,
    changeStatusOfAllSelected,
    selectedOrderId,
    amountForTheItem,
    currentPage,
    pageLimit,
    loadMoreItems,
    isRefreshing,
    onRefresh,
    notifiShow,
    setDisplayNotificationPopup,
    notifyData,
    navigateTochat,
    navigateToNotification,
  } = props;

  return (
    <View style={styles.screenMain}>
      {notifiShow && (
        <InAppNotificationModal
          hide={() => setDisplayNotificationPopup(false)}
          visible={true}
          title={notifyData.title}
          body={notifyData.body}
          redirect={() => navigateTochat(notifyData)}
        />
      )}

      <Header
        isLogoRequired={true}
        isNotificationRequired={true}
        isLanguageButtonRequired={true}
        customHeaderStyle={{
          height: globals.INTEGER.headerHeight,
          alignItems: "center",
          backgroundColor: globals.COLOR.headerColor,
        }}
        onRightButtonPress={navigateToNotification}
      />
      {(isLoading || isStatusUpdateLoading) && <Loader />}
      <View style={styles.screenDesignContainer}>
        <TopTab
          tabIndex={tabIndex}
          firstTabText={appTexts.STRING.readyForDelivery}
          secondTabText={appTexts.STRING.toCollect}
          thirdTabText={appTexts.STRING.toReturn}
          firstTabCount={firstTabCount}
          secondTabCount={secondTabCount}
          thirdTabCount={thirdTabCount}
          onTabChange={onTabChange}
        />
        <View
          style={
            isSelectAllViewVisible
              ? styles.screenContainerWithBanner
              : styles.screenContainerWithMargin
          }
        >
          <FlatList
            ListEmptyComponent={
              <View style={styles.noDataContainer}>
                <Text style={styles.noDataText}>{appTexts.STRING.nodata}</Text>
              </View>
            }
            style={styles.flatListStyle}
            data={listData}
            extraData={listData}
            keyExtractor={(item) => item.id.toString()}
            /*onEndReachedThreshold={0.5}
						onEndReached={({ distanceFromEnd }) => {
							if (listData.length >= (currentPage * pageLimit)) {
								loadMoreItems();
							}
						}}*/
            onRefresh={() => {
              onRefresh();
            }}
            refreshing={isRefreshing}
            showsVerticalScrollIndicator={false}
            renderItem={({ item, index }) => (
              <ListItemCard
                tabIndex={tabIndex}
                itemData={item}
                itemOnPress={onListItemPress}
                listButtonClick={listButtonClick}
                checkBoxClick={checkBoxClick}
                isBottomButtonRequired={true}
              />
            )}
          />
        </View>
        {isSelectAllViewVisible && (
          <View style={styles.bottomBannerView}>
            <LinkButton
              buttonText={appTexts.STRING.selectAll}
              linkColor={globals.COLOR.tabUnderLineColor}
              buttonClick={selectAllClick}
            />
            <RoundButton
              buttonPosition={"center"}
              buttonText={
                tabIndex === 1
                  ? appTexts.STRING.markSelectedAsCollected
                  : appTexts.STRING.markSelectedAsReturned
              }
              buttonClick={changeStatusOfAllSelected}
            />
          </View>
        )}
      </View>

      <PopupModal
        popupNoClick={popupNoClick}
        popupYesClick={popupYesClick}
        popupIndex={popupIndex}
        selectedOrderId={selectedOrderId}
        amountForTheItem={amountForTheItem}
      />
    </View>
  );
};

HomeView.propTypes = {
  tabIndex: PropTypes.number,
  listData: PropTypes.array,
  firstTabCount: PropTypes.number,
  secondTabCount: PropTypes.number,
  thirdTabCount: PropTypes.number,
  popupIndex: PropTypes.number,
  onListItemPress: PropTypes.func,
  listButtonClick: PropTypes.func,
  checkBoxClick: PropTypes.func,
  onTabChange: PropTypes.func,
  popupYesClick: PropTypes.func,
  popupNoClick: PropTypes.func,
  isSelectAllViewVisible: PropTypes.bool,
  selectAllClick: PropTypes.func,
  isLoading: PropTypes.bool,
  isStatusUpdateLoading: PropTypes.bool,
  changeStatusOfAllSelected: PropTypes.func,
  selectedOrderId: PropTypes.string,
  amountForTheItem: PropTypes.string,
  currentPage: PropTypes.number,
  pageLimit: PropTypes.number,
  loadMoreItems: PropTypes.func,
};

export default HomeView;
