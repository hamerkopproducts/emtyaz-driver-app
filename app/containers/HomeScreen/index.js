import React, { useEffect, useState } from "react";

import NetInfo from "@react-native-community/netinfo";
import { connect } from "react-redux";
import HomeView from "./HomeView";
import { bindActionCreators } from "redux";

import * as homeActions from "../../actions/homeActions";
import * as orderActions from "../../actions/orderActions";
import * as loginActions from "../../actions/loginActions";
import functions from "../../lib/functions";
import appTexts from "../../lib/appTexts";

import messaging from "@react-native-firebase/messaging";
import { Notifications } from 'react-native-notifications';
import ServiceWrapperAwait from '../../service/ServiceWrapperAwait';
import Sound from 'react-native-sound';

let page = 0;
let pageLimit = 20;

const HomeScreen = (props) => {
  //Initialising states
  const [tabIndex, setTabIndex] = useState(0);
  const [popupIndex, setPopupIndex] = useState(-1);
  const [selectedItemToDeliver, setSelectedItemToDeliver] = useState([]);
  const [selectedItemToCollect, setSelectedItemToCollect] = useState([]);
  const [selectedItemToReturn, setSelectedItemToReturn] = useState([]);
  const [amountForTheItem, setAmountForTheItem] = useState("");
  const [statusChangedOrderIds, setStatusChangedOrderIds] = useState("");
  const [isRefreshing, setIsRefreshing] = useState(false);

  const [displayNotificationPopup, setDisplayNotificationPopup] = useState(false);
  const [notifyData, setNotifyData] = useState({});

  const notificationRead = async (id) => {
    const sAsyncWrapper = new ServiceWrapperAwait();
    await sAsyncWrapper.get('notification/read/' + id,
      { language_attach: false, is_auth_required: true }
    );
  }

  playSound = () => {
    const callback = (error, sound) => {
      if (error) {
        console.debug('error', error.message);
        return;
      }
      sound.play(() => {
        sound.release();
      });
    };
    const audio = require('../../sound/salon_alarm.mp3');
    const sound = new Sound(audio, error => callback(error, sound));
  }

  initialNotification = async () => {
    const notification = await Notifications.getInitialNotification();
    if (typeof notification != 'undefined' && notification) {
      try {
        const _data = notification.payload;
        if (_data.notification_id) {
          notificationRead(_data.notification_id);
        }
        if (_data.type == 'schedule') {
          props.navigation.navigate("OrderDetailsScreen", {
            fromScreen: "home",
            selectedItem: {
              id: _data.order_id
            },
            selectedTabIndex: 1,
          });
        } else if (_data.type == 'support') {
          props.navigation.navigate('ChatScreen', {caht_id: _data.request_id});
        }
      } catch (err) {
        console.log('open notify err', err);
      }
    }
  }

  //will focus
  useEffect(() => {
    return props.navigation.addListener("focus", () => {
      if (props.currentScreen !== "Home") {
        props.setCurrentScreen("Home");
      }
      NetInfo.fetch().then((state) => {
        if (state.isConnected) {
          props.getOrdersToDeliver(0, props.userData.access_token);
          props.getOrdersToCollect(0, props.userData.access_token);
          props.getOrdersToReturn(0, props.userData.access_token);
          setSelectedItemToDeliver([]);
          setSelectedItemToCollect([]);
          setSelectedItemToReturn([]);
        } else {
          functions.displayToast(
            "error",
            "top",
            appTexts.ALERT_MESSAGES.error,
            appTexts.ALERT_MESSAGES.noInternet
          );
        }
      });
    });
  }, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    
    foregroundState();
    Sound.setCategory('Playback', true);
    Notifications.registerRemoteNotifications();
    initialNotification();

    Notifications.events().registerNotificationOpened((notification, completion) => {
      try {
        const _data = notification.payload;
        if (_data.notification_id) {
          notificationRead(_data.notification_id);
        }
        if (_data.type == 'schedule') {
          props.navigation.navigate("OrderDetailsScreen", {
            fromScreen: "home",
            selectedItem: {
              id: _data.order_id
            },
            selectedTabIndex: 1,
          });
        } else if (_data.type == 'support') {
          props.navigation.navigate('ChatScreen', {caht_id: _data.request_id});
        }
      } catch (err) {
        console.log('open notify err');
      }
      completion();
    });
    
    if (props.currentScreen !== "Home") {
      props.setCurrentScreen("Home");
    }

    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        props.getOrdersToDeliver(0, props.userData.access_token);
        props.getOrdersToCollect(0, props.userData.access_token);
        props.getOrdersToReturn(0, props.userData.access_token);
      } else {
        functions.displayToast(
          "error",
          "top",
          appTexts.ALERT_MESSAGES.error,
          appTexts.ALERT_MESSAGES.noInternet
        );
      }
    });
  };

  const foregroundState = () => {
    const unsubscribe = messaging().onMessage(async (remoteMessage) => {
      playSound();
      const _data = getNotificationData(remoteMessage);
      setNotifyData(_data);
      setDisplayNotificationPopup(true);
    });
    return unsubscribe;
  };

  const getNotificationData = (remoteMessage) => {
    let body = "You have a new notification";
    let title = "Notification";

    if (typeof remoteMessage.notification != "undefined") {
      if (typeof remoteMessage.notification.body != "undefined") {
        body = remoteMessage.notification.body;
      }
      if (typeof remoteMessage.notification.title != "undefined") {
        title = remoteMessage.notification.title;
      }
    } else if (typeof remoteMessage.data != "undefined") {
      if (typeof remoteMessage.data.body != "undefined") {
        body = remoteMessage.data.body;
      } else if (
        typeof remoteMessage.data.notification != "undefined" &&
        typeof remoteMessage.data.notification.body != "undefined"
      ) {
        body = remoteMessage.data.notification.body;
      }
      if (typeof remoteMessage.data.title != "undefined") {
        title = remoteMessage.data.title;
      } else if (
        typeof remoteMessage.data.notification != "undefined" &&
        typeof remoteMessage.data.notification.title != "undefined"
      ) {
        body = remoteMessage.data.notification.title;
      }
    }
    let details = null;
    if (remoteMessage && remoteMessage.data && remoteMessage.data && remoteMessage.data.type) {
      if (remoteMessage.data.type !== 'support') {
        details = {
          type: remoteMessage.data.type,
          order_id: remoteMessage.data.order_id,
        }
      } else if (remoteMessage.data.type === 'support') {
        details = {
          type: remoteMessage.data.type,
          requestId: remoteMessage.data.request_id
        }
      }
    }

    return {
      title: title,
      body: body,
      details: details
    };
  };

  //unmount
  useEffect(() => {
    return () => {
      handleComponentUnmount();
    };
  }, []);

  const handleComponentUnmount = () => {};

  //updated
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {
    if (props.orderStatusUpdateResponse) {
      //functions.displayAlert(null, props.orderStatusUpdateResponse.msg);
      props.updateOrderStatusSuccess(null);
      getSelectedOrderIds();
      if (tabIndex === 0) {
        props.getOrdersToDeliver(0, props.userData.access_token);
        props.getOrdersToReturn(0, props.userData.access_token);
        setPopupIndex(4);
      } else if (tabIndex === 1) {
        props.getOrdersToCollect(0, props.userData.access_token);
        props.getOrdersToDeliver(0, props.userData.access_token);
        props.getOrdersToReturn(0, props.userData.access_token);
        functions.displayToast(
          "success",
          "top",
          appTexts.ALERT_MESSAGES.success,
          appTexts.ALERT_MESSAGES.orderCollectedSuccessfully
        );
      } else if (tabIndex === 2) {
        props.getOrdersToReturn(0, props.userData.access_token);
        props.getOrdersToDeliver(0, props.userData.access_token);
        functions.displayToast(
          "success",
          "top",
          appTexts.ALERT_MESSAGES.success,
          appTexts.ALERT_MESSAGES.orderReturnedSuccessfully
        );
      }
      setSelectedItemToDeliver([]);
      setSelectedItemToReturn([]);
      setSelectedItemToCollect([]);
    }
    if (props.ordersToDeliverAPIComplete) {
      props.updateOderToDeliverStatus();
      if (isRefreshing) {
        setIsRefreshing(false);
      }
    }
    if (props.ordersToCollectAPIComplete) {
      props.updateOderToCollectStatus();
      if (isRefreshing) {
        setIsRefreshing(false);
      }
    }
    if (props.ordersToReturnAPIComplete) {
      props.updateOderToReturnStatus();
      if (isRefreshing) {
        setIsRefreshing(false);
      }
    }
  };
  const onListItemPress = (itemData) => {
    props.navigation.navigate("OrderDetailsScreen", {
      fromScreen: "home",
      selectedItem: itemData,
      selectedTabIndex: tabIndex,
    });
  };
  const onTabChange = (index) => {
    if (tabIndex !== index) {
      setTabIndex(index);
      NetInfo.fetch().then((state) => {
        if (state.isConnected) {
          if (index === 0) {
            props.getOrdersToDeliver(0, props.userData.access_token);
          } else if (index === 1) {
            props.getOrdersToCollect(0, props.userData.access_token);
          } else if (index === 2) {
            props.getOrdersToReturn(0, props.userData.access_token);
          }
        } else {
          functions.displayToast(
            "error",
            "top",
            appTexts.ALERT_MESSAGES.error,
            appTexts.ALERT_MESSAGES.noInternet
          );
        }
      });
      setSelectedItemToCollect([]);
      setSelectedItemToReturn([]);
      setSelectedItemToDeliver([]);
    }
  };

  const loadMoreItems = () => {
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        if (tabIndex === 0) {
          props.getOrdersToDeliver(
            parseInt(props.ordersToDeliver.current_page + 1),
            props.userData.access_token
          );
        } else if (tabIndex === 1) {
          props.getOrdersToCollect(
            parseInt(props.ordersToCollect.current_page + 1),
            props.userData.access_token
          );
        } else if (tabIndex === 2) {
          props.getOrdersToReturn(
            parseInt(props.ordersToReturn.current_page + 1),
            props.userData.access_token
          );
        }
      } else {
        functions.displayToast(
          "error",
          "top",
          appTexts.ALERT_MESSAGES.error,
          appTexts.ALERT_MESSAGES.noInternet
        );
      }
    });
    setSelectedItemToReturn([]);
    setSelectedItemToCollect([]);
    setSelectedItemToDeliver([]);
  };

  const getSelectedOrderIds = () => {
    let tempListData =
      tabIndex === 0
        ? [...props.ordersToDeliver.data]
        : tabIndex === 1
        ? [...props.ordersToCollect.data]
        : [...props.ordersToReturn.data];
    let selectedData =
      tabIndex == 0
        ? selectedItemToDeliver
        : tabIndex == 1
        ? selectedItemToCollect
        : selectedItemToReturn;
    let orderIds = "";
    tempListData.forEach((element) => {
      selectedData.forEach((item) => {
        if (element.id === item) {
          orderIds =
            orderIds === ""
              ? orderIds + element.order_id
              : orderIds + ", " + element.order_id;
        }
      });
    });
    setStatusChangedOrderIds(orderIds);
  };

  const selectAllClick = () => {
    let tempOrderDataObject =
      tabIndex === 1
        ? { ...props.ordersToCollect }
        : { ...props.ordersToReturn };
    let tempListData = tempOrderDataObject.data;
    let tempSelectedItem = [];
    tempListData.forEach((element) => {
      element.isSelected = true;
      tempSelectedItem.push(element.id);
    });
    if (tabIndex === 1) {
      setSelectedItemToCollect(tempSelectedItem);
      tempOrderDataObject.data = tempListData;
      props.updateOderToCollect(tempOrderDataObject);
    } else {
      setSelectedItemToReturn(tempSelectedItem);
      tempOrderDataObject.data = tempListData;
      props.updateOderToReturn(tempOrderDataObject);
    }
  };
  const popupNoClick = () => {
    setSelectedItemToReturn([]);
    setSelectedItemToCollect([]);
    setSelectedItemToDeliver([]);
    setPopupIndex(-1);
  };
  const popupYesClick = () => {
    if (popupIndex === 0) {
      setPopupIndex(3);
    } else if (popupIndex === 4) {
      setSelectedItemToDeliver([]);
      setSelectedItemToReturn([]);
      setSelectedItemToCollect([]);
      setPopupIndex(-1);
    } else {
      callUpdatesStatusAPI();
    }
  };

  const callUpdatesStatusAPI = () => {
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        let apiParams = {
          orders:
            tabIndex == 0
              ? selectedItemToDeliver
              : tabIndex == 1
              ? selectedItemToCollect
              : selectedItemToReturn,
          status:
            tabIndex == 0
              ? "delivered"
              : tabIndex == 1
              ? "collected"
              : "returned",
        };
        setPopupIndex(-1);
        props.updateStatus(apiParams, props.userData.access_token);
      } else {
        functions.displayToast(
          "error",
          "top",
          "",
          appTexts.ALERT_MESSAGES.noInternet
        );
      }
    });
  };

  const changeStatusOfAllSelected = () => {
    setPopupIndex(tabIndex === 1 ? 5 : 6);
  };

  onRefresh = () => {
    setIsRefreshing(true);
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        if (tabIndex === 0) {
          props.getOrdersToDeliver(0, props.userData.access_token);
        } else if (tabIndex === 1) {
          props.getOrdersToCollect(0, props.userData.access_token);
        } else if (tabIndex === 2) {
          props.getOrdersToReturn(0, props.userData.access_token);
        }
      } else {
        functions.displayToast(
          "error",
          "top",
          appTexts.ALERT_MESSAGES.error,
          appTexts.ALERT_MESSAGES.noInternet
        );
      }
    });
    setSelectedItemToCollect([]);
    setSelectedItemToReturn([]);
    setSelectedItemToDeliver([]);
  };

  const listButtonClick = (orderId, amount) => {
    if (tabIndex === 0) {
      setAmountForTheItem(amount);
      let tempSelectedItemToDeliver = [];
      tempSelectedItemToDeliver.push(orderId);
      setSelectedItemToDeliver(tempSelectedItemToDeliver);
    } else if (tabIndex === 1) {
      let tempSelectedItemToCollect = [];
      tempSelectedItemToCollect.push(orderId);
      setSelectedItemToCollect(tempSelectedItemToCollect);
    } else {
      let tempSelectedItemToReturn = [];
      tempSelectedItemToReturn.push(orderId);
      setSelectedItemToReturn(tempSelectedItemToReturn);
    }
    setPopupIndex(tabIndex);
  };

  const checkBoxClick = (itemData) => {
    let tempItemData = itemData;
    let tempOrderDataObject =
      tabIndex === 1
        ? { ...props.ordersToCollect }
        : { ...props.ordersToReturn };
    let tempOrderData = tempOrderDataObject.data;
    let selectedIndex = tempOrderData.indexOf(itemData);
    tempItemData.isSelected =
      tempItemData.isSelected === null ||
      tempItemData.isSelected === undefined ||
      tempItemData.isSelected === false
        ? true
        : false;
    tempOrderData[selectedIndex] = tempItemData;
    if (tabIndex === 1) {
      let tempSelectedItemToCollect = selectedItemToCollect;
      if (tempSelectedItemToCollect.indexOf(itemData.id) !== -1) {
        tempSelectedItemToCollect.splice(
          tempSelectedItemToCollect.indexOf(itemData.id),
          1
        );
      } else {
        tempSelectedItemToCollect.push(itemData.id);
      }
      setSelectedItemToCollect(tempSelectedItemToCollect);
      tempOrderDataObject.data = tempOrderData;
      props.updateOderToCollect(tempOrderDataObject);
    } else {
      let tempSelectedItemToReturn = selectedItemToReturn;
      if (tempSelectedItemToReturn.indexOf(itemData.id) !== -1) {
        tempSelectedItemToReturn.splice(
          tempSelectedItemToReturn.indexOf(itemData.id),
          1
        );
      } else {
        tempSelectedItemToReturn.push(itemData.id);
      }
      setSelectedItemToReturn(tempSelectedItemToReturn);
      tempOrderDataObject.data = tempOrderData;
      props.updateOderToReturn(tempOrderDataObject);
    }
  };
  return (
    <HomeView
      listData={
        tabIndex === 0
          ? props.ordersToDeliver !== null &&
            props.ordersToDeliver.data !== null
            ? props.ordersToDeliver.data
            : []
          : tabIndex === 1
          ? props.ordersToCollect !== null &&
            props.ordersToCollect.data !== null
            ? props.ordersToCollect.data
            : []
          : props.ordersToReturn !== null && props.ordersToReturn.data !== null
          ? props.ordersToReturn.data
          : []
      }
      onListItemPress={onListItemPress}
      listButtonClick={listButtonClick}
      checkBoxClick={checkBoxClick}
      tabIndex={tabIndex}
      firstTabCount={
        props.ordersToDeliver !== null && props.ordersToDeliver.data !== null
          ? props.ordersToDeliver.data.length
          : 0
      }
      secondTabCount={
        props.ordersToCollect !== null && props.ordersToCollect.data !== null
          ? props.ordersToCollect.data.length
          : 0
      }
      thirdTabCount={
        props.ordersToReturn !== null && props.ordersToReturn.data !== null
          ? props.ordersToReturn.data.length
          : 0
      }
      onTabChange={onTabChange}
      popupIndex={popupIndex}
      popupNoClick={popupNoClick}
      popupYesClick={popupYesClick}
      isSelectAllViewVisible={
        popupIndex === -1 &&
        ((tabIndex === 1 && selectedItemToCollect.length > 0) ||
          (tabIndex === 2 && selectedItemToReturn.length > 0))
      }
      selectAllClick={selectAllClick}
      isLoading={props.isLoading}
      isStatusUpdateLoading={props.isStatusUpdateLoading}
      changeStatusOfAllSelected={changeStatusOfAllSelected}
      selectedOrderId={statusChangedOrderIds}
      amountForTheItem={amountForTheItem}
      currentPage={
        tabIndex === 0
          ? props.ordersToDeliver !== null
            ? props.ordersToDeliver.current_page
            : 0
          : tabIndex === 1
          ? props.ordersToCollect !== null
            ? props.ordersToCollect.current_page
            : 0
          : props.ordersToReturn !== null
          ? props.ordersToReturn.current_page
          : 0
      }
      pageLimit={
        tabIndex === 0
          ? props.ordersToDeliver !== null
            ? props.ordersToDeliver.per_page
            : 0
          : tabIndex === 1
          ? props.ordersToCollect !== null
            ? props.ordersToCollect.per_page
            : 0
          : props.ordersToReturn !== null
          ? props.ordersToReturn.per_page
          : 0
      }
      loadMoreItems={loadMoreItems}
      onRefresh={onRefresh}
      isRefreshing={isRefreshing}
      notifiShow={displayNotificationPopup}
      setDisplayNotificationPopup={setDisplayNotificationPopup}
      notifyData={notifyData}
      navigateTochat={(data) => {
        setDisplayNotificationPopup(false);
        if(data.details.type == 'support') {
          props.navigation.navigate('ChatScreen', {caht_id: data.details.requestId});
        }
        if(data.details.type == 'schedule') {
          props.navigation.navigate("OrderDetailsScreen", {
            fromScreen: "home",
            selectedItem: {
              id: data.details.order_id
            },
            selectedTabIndex: tabIndex,
          });
        }
      }}
      navigateToNotification={() => props.navigation.navigate('NotificationScreen') }
    />
  );
};

const mapStateToProps = (state, props) => {
  return {
    ordersToDeliver: state.homeReducer.ordersToDeliver,
    ordersToCollect: state.homeReducer.ordersToCollect,
    ordersToReturn: state.homeReducer.ordersToReturn,
    userData: state.loginReducer.userData,
    currentScreen: state.loginReducer.currentScreen,
    isLoading: state.homeReducer.isLoading,
    isStatusUpdateLoading: state.orderReducer.isLoading,
    orderStatusUpdateResponse: state.orderReducer.orderStatusUpdateResponse,
    ordersToDeliverAPIComplete: state.homeReducer.ordersToDeliverAPIComplete,
    ordersToCollectAPIComplete: state.homeReducer.ordersToCollectAPIComplete,
    ordersToReturnAPIComplete: state.homeReducer.ordersToReturnAPIComplete,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      getOrdersToDeliver: homeActions.getOrdersToDeliver,
      getOrdersToCollect: homeActions.getOrdersToCollect,
      getOrdersToReturn: homeActions.getOrdersToReturn,
      updateOderToDeliver: homeActions.updateOderToDeliver,
      updateOderToCollect: homeActions.updateOderToCollect,
      updateOderToReturn: homeActions.updateOderToReturn,
      updateStatus: orderActions.updateStatus,
      updateOrderStatusSuccess: orderActions.updateOrderStatusSuccess,
      updateOderToDeliverStatus: homeActions.updateOderToDeliverStatus,
      updateOderToCollectStatus: homeActions.updateOderToCollectStatus,
      updateOderToReturnStatus: homeActions.updateOderToReturnStatus,
      setCurrentScreen: loginActions.setCurrentScreen,
    },
    dispatch
  );
};

const homeScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeScreen);

homeScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null,
});

export default homeScreenWithRedux;
