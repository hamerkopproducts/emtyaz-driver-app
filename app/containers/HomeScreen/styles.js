import { StyleSheet, I18nManager } from "react-native";
import globals from "../../lib/globals"

let headerButtonContainerWidth = globals.SCREEN_SIZE.width - (globals.MARGIN.marginTen*2);
let headerButtonWidth = (headerButtonContainerWidth-(globals.MARGIN.marginTen*2)) / 3;
let bottomBannerViewHeight = 110;
const images = {
  
};

const styles = StyleSheet.create({
  screenMain:{
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
      flexDirection:'column',
      backgroundColor:'#ffffff'
  },
  screenDesignContainer:{
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
  },
  screenContainerWithMargin:{
    width: globals.INTEGER.screenWidthWithMargin,
    height: globals.INTEGER.screenContentHeight - globals.INTEGER.topTabHieght,
    marginLeft: globals.INTEGER.leftRightMargin,
  },
  screenContainerWithBanner: {
    width: globals.INTEGER.screenWidthWithMargin,
    height: globals.INTEGER.screenContentHeight - (bottomBannerViewHeight + globals.INTEGER.topTabHieght),
    marginLeft: globals.INTEGER.leftRightMargin,
    paddingBottom:5
  },
  headerButtonContianer:{
    flexDirection: 'row',
    width: headerButtonContainerWidth,
    height: globals.INTEGER.heightFifty,
    alignItems: 'center'
  },
  headerButton:{
    width: headerButtonWidth,
    height: globals.INTEGER.heightThirty
  },
  headerButtonMargin:{
    marginLeft: globals.MARGIN.marginTen
  },
  categoryItemRow:{
    width:'100%',
    flexDirection:'row'
  },
  flatListStyle:{
    width:'100%'
  },
  bottomBannerView:{
    width: globals.SCREEN_SIZE.width,
    height: bottomBannerViewHeight,
    backgroundColor: globals.COLOR.white,
    // shadowColor: globals.COLOR.borderColor,
    // shadowOffset: {
    //   width: 0,
    //   height: 0,
    // },
    // shadowOpacity: 0.5,
    // shadowRadius: 0.5,
    // elevation: 1,
    // borderRadius:10,
    borderTopWidth: 1,
    borderRadius: 0,
    borderColor: '#ddd',
    shadowColor: "#000",
    shadowOffset: { width: 0, height: -2 },
    shadowOpacity: 0.12,
    shadowRadius: 2,
    elevation: 3,
  },
  noDataContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 50
  },
  noDataText: {
    textAlign: 'center',
    color: globals.COLOR.lightTextColor,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.avenirLight,
    fontSize: 12
  }
});

export { images, styles };
