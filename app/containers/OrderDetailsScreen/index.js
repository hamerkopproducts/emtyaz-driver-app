import React, { useEffect, useState } from 'react';

import NetInfo from "@react-native-community/netinfo";
import { connect } from 'react-redux';
import OrderDetailsView from './OrderDetailsView';
import { bindActionCreators } from "redux";
import * as orderActions from "../../actions/orderActions";
import * as homeActions from "../../actions/homeActions";
import appTexts from "../../lib/appTexts";
import functions from "../../lib/functions"


const OrderDetailsScreen = (props) => {

  const [popupIndex, setPopupIndex] = useState(-1);
  const [isForReturn, setIsForReturn] = useState(false);
  //will focus
  useEffect(() => {
    return props.navigation.addListener('focus', () => {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
  
        } else {
          functions.displayAlert(null, appTexts.ALERT_MESSAGES.noInternet);
        }
      });
    });
  }, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        props.getOrderDetails(props.route.params.selectedItem.id, props.userData.access_token);
      } else {
        functions.displayToast('error', 'top', appTexts.ALERT_MESSAGES.error, appTexts.ALERT_MESSAGES.noInternet);
      }
    });
  }

  //unmount
  useEffect(() => { return () => { handleComponentUnmount(); } }, []);

  const handleComponentUnmount = () => {
  };
 
  //updated
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {
    if (props.orderStatusUpdateData) {
      props.updateOrderStatusFromDetails(null);
      if (props.route.params.selectedTabIndex == 0){
        if (isForReturn) {
          setIsForReturn(false);
          onBackButtonClick();
          functions.displayToast('success', 'top', appTexts.ALERT_MESSAGES.success, appTexts.ALERT_MESSAGES.orderReturnedSuccessfully);
        }else{
          console.debug('here')
          setPopupIndex(4);
        }
      } else if(props.route.params.selectedTabIndex == 1) {
        onBackButtonClick();
        functions.displayToast('success', 'top', appTexts.ALERT_MESSAGES.success, appTexts.ALERT_MESSAGES.orderCollectedSuccessfully);
      } else{
        onBackButtonClick();
        functions.displayToast('success', 'top', appTexts.ALERT_MESSAGES.success, appTexts.ALERT_MESSAGES.orderReturnedSuccessfully);
      }
    }
  };
  

  const onBackButtonClick = () => {
    props.navigation.goBack();
  };
  const markStatusClick = () => {
    setPopupIndex(props.route.params.selectedTabIndex);
  };
  const popupNoClick = () => {
    if (isForReturn){
      setIsForReturn(false);
    }
    setPopupIndex(-1);
  };

  const popupYesClick = () => {
    if (popupIndex === 0) {
      callUpdatesStatusAPI();
      // setPopupIndex(3);
    } else if (popupIndex === 4) {
      setPopupIndex(-1);
      onBackButtonClick();
    } else {
      callUpdatesStatusAPI();
    }
  };


  const callUpdatesStatusAPI = () => {
    let apiParams = {
      "orders": [props.orderDetails.id],
      "status": isForReturn || props.route.params.selectedTabIndex == 2 ? "returned" : (props.route.params.selectedTabIndex == 1 ? "collected" : "delivered")
    };
    setPopupIndex(-1);
    props.updateStatusFromDetails(apiParams, props.userData.access_token);
  };
  
  const markAsReturnButtonClick = () => {
    setIsForReturn(true);
    setPopupIndex(-1);
  };
  
  return (
    props.orderDetails && props.orderDetails !== null?
      <OrderDetailsView 
        onBackButtonClick={onBackButtonClick} 
        fromScreen={props.route.params.fromScreen} 
        detailsData={props.orderDetails} 
        selectedTabIndex={props.route.params.selectedTabIndex} 
        isLoading={props.isLoading}
        markStatusClick={markStatusClick}
        popupIndex={popupIndex}
        popupNoClick={popupNoClick}
        popupYesClick={popupYesClick}
        isForReturn={isForReturn}
        markAsReturnButtonClick={markAsReturnButtonClick}
        navigateToNotification={() => props.navigation.navigate('NotificationScreen') }
      />
    :null
  );

};


const mapStateToProps = (state, props) => {
  return {
    userData: state.loginReducer.userData,
    orderDetails: state.orderReducer.orderDetails,
    isLoading: state.orderReducer.isLoading,
    getOrdersToDeliver: homeActions.getOrdersToDeliver,
    getOrdersToCollect: homeActions.getOrdersToCollect,
    getOrdersToReturn: homeActions.getOrdersToReturn,
    orderStatusUpdateData: state.orderReducer.orderStatusUpdateData
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    getOrderDetails: orderActions.getOrderDetails,
    updateStatusFromDetails: orderActions.updateStatusFromDetails,
    updateOrderStatusFromDetails: orderActions.updateOrderStatusFromDetails
  }, dispatch)
};

const orderDetailsScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(OrderDetailsScreen);

orderDetailsScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default orderDetailsScreenWithRedux;
