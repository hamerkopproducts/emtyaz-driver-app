import { StyleSheet } from "react-native";
import globals from "../../lib/globals"

let headerButtonContainerWidth = globals.SCREEN_SIZE.width - (globals.MARGIN.marginTen*2);
let headerButtonWidth = (headerButtonContainerWidth-(globals.MARGIN.marginTen*2)) / 3;

const images = {
  
};

const styles = StyleSheet.create({
  screenMain:{
      flex:1,
      flexDirection:'column',
      backgroundColor: globals.COLOR.screenBackground
  },
  screenContainer:{
    marginTop: 5,
    width: globals.INTEGER.screenWidthWithMargin,
    height: globals.INTEGER.screenContentHeight - 5 + globals.INTEGER.footerTabBarHeight,
    marginLeft: globals.INTEGER.leftRightMargin,
    backgroundColor: globals.COLOR.white
  },
  screenContainerWithMargin: {
    marginTop:5,
    width: globals.INTEGER.screenWidthWithMargin,
    height: globals.INTEGER.screenContentHeight - 5,
    marginLeft: globals.INTEGER.leftRightMargin,
    backgroundColor: globals.COLOR.white
  },
  headerButtonContianer:{
    flexDirection: 'row',
    width: headerButtonContainerWidth,
    height: globals.INTEGER.heightFifty,
    alignItems: 'center'
  },
  headerButton:{
    width: headerButtonWidth,
    height: globals.INTEGER.heightThirty
  },
  headerButtonMargin:{
    marginLeft: globals.MARGIN.marginTen
  },
  flatListStyle: {
    marginTop:"1%",
    width:'100%',
    height: '99%'
  },
  bottomButtonContainer: {
    position:'absolute',
    bottom:0,
    height: globals.INTEGER.footerTabBarHeight,
    width: '100%',
    borderTopWidth: 1,
    borderTopColor: globals.COLOR.borderColor,
    backgroundColor: globals.COLOR.headerColor,
    shadowColor: globals.COLOR.borderColor,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  buttonContainer:{
    height: globals.INTEGER.footerTabBarHeight,
    marginLeft: globals.INTEGER.leftRightMargin,
    width: globals.INTEGER.screenWidthWithMargin,
    flexDirection:'row',
    alignItems: 'center',
    justifyContent: 'center'
  }
});

export { images, styles };
