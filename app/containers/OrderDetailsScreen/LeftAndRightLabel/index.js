import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Image, Text ,I18nManager} from 'react-native';
import appTexts from "../../../lib/appTexts";
import globals from "../../../lib/globals";
import { images, styles } from "./styles";


const LeftAndRightLabel = (props) => {
  const {
    leftLabel,
    rightLabel
  } = props;

  let viewAdditionalStyles = leftLabel === appTexts.STRING.orderDetails ?{
    marginTop:20,
    backgroundColor: '#FBFCFD'
  } : leftLabel === appTexts.STRING.orderSummary ? {
    paddingTop: 20,
    paddingBottom:20,
    marginBottom: 20,
    backgroundColor: '#FBFCFD'
    } : leftLabel === appTexts.STRING.totalWithTax ? {
      paddingTop: 20,
      paddingBottom: 20
      } : leftLabel === appTexts.STRING.paymentMethod ? {
        paddingTop: 20,
        paddingBottom: 20
      } :null;


  let leftLabelStyle = leftLabel === appTexts.STRING.orderDetails ||
    leftLabel === appTexts.STRING.orderSummary || leftLabel === appTexts.STRING.totalWithTax ? {
      color: globals.COLOR.textColor,
      fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic: globals.FONTS.avenirHeavy
    } : leftLabel === appTexts.STRING.paymentMethod ? {
      color: globals.COLOR.textColorGreen,
      fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic :globals.FONTS.avenirLight
    } : {
        color: globals.COLOR.lightTextColor,
        fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.avenirLight
      };

  let rightLabelStyle = leftLabel === appTexts.STRING.orderDetails ? {
    color: globals.COLOR.textColorGreen,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiarabicBold: globals.FONTS.avenirHeavy
  } : leftLabel === appTexts.STRING.orderSummary ? {
    color: globals.COLOR.tabUnderLineColor,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiarabicBold: globals.FONTS.avenirHeavy
  } : {
        color: globals.COLOR.textColor,
        fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiarabicBold: globals.FONTS.avenirHeavy
      };
  return (
    <View style={[styles.leftRightLabelRowView, viewAdditionalStyles]}>
      <View style={styles.labelContainer}>
        <Text style={[styles.leftLabel, leftLabelStyle]}>{leftLabel}</Text>
      </View>
      <View style={styles.labelContainer}>
        <Text style={[styles.rightLabel, rightLabelStyle]}>{rightLabel}</Text>
      </View>

    </View>
  );
};
LeftAndRightLabel.propTypes = {
  leftLabel: PropTypes.string,
  rightLabel: PropTypes.string
};

export default LeftAndRightLabel;