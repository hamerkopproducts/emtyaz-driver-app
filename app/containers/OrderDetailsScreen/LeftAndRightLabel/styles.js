import { StyleSheet,I18nManager } from "react-native";
import globals from "../../../lib/globals";
const images = {};


const styles = StyleSheet.create({
  leftRightLabelRowView: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop:10,
    paddingBottom:10
  },
  labelContainer: {
    width: '50%'
  },
  leftLabel: {
    textAlign: 'left',
    color: globals.COLOR.textColor,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.avenirLight,
    fontSize: 12
  },
  rightLabel: {
    textAlign: 'right',
    color: globals.COLOR.textColor,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic:  globals.FONTS.avenirLight,
    fontSize: 12
  },

});

export { images, styles };
