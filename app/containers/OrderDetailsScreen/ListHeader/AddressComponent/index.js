import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Image, Text, Platform, Linking, Alert } from 'react-native';
import { images, styles } from "./styles";
import { showLocation } from 'react-native-map-link'

const AddressComponent = (props) => {
  const {
    showIcon,
    addressType,
    addressDetails,
    loc
  } = props;

  let lng = '';
  let lat = '';
  if(loc) {
    try {
      lat = JSON.parse(loc).latitude;
      lng = JSON.parse(loc).longitude;
    } catch(err) {
      lng = '';
      lat = '';
    }
  }

  const callNumber = phone => {
    let phoneNumber = phone;
    if (Platform.OS !== 'android') {
      phoneNumber = `telprompt:${phone}`;
    } else  {
      phoneNumber = `tel:${phone}`;
    }
    Linking.canOpenURL(phoneNumber)
    .then(supported => {
      if (!supported) {
        Alert.alert('Phone number is not available');
      } else {
        return Linking.openURL(phoneNumber);
      }
    })
    .catch(err => console.log(err));
  };

  return (

    <View style={styles.addressView}>
      <View style={styles.addressHeadingView}>
        <View style={styles.addressLabelView}>
          <Text style={styles.lightLabel}>{addressType}</Text>
        </View>
        {showIcon &&
          <View style={styles.addressIconView}>
            <View style={styles.locationIconContainer}>
            <TouchableOpacity
                    onPress={() => {
                      showLocation({
                        latitude: lat,
                        longitude: lng,
                        googleForceLatLon: false,
                        alwaysIncludeGoogle: true,
                        dialogTitle: 'Delivery',
                        cancelText: 'Cancel',
                        appsWhiteList: ['google-maps']
                      })
                    }}
                    style={{ width: '80%' }}
                  >
              <Image source={images.locationIcon} style={styles.locationIcon} />
              </TouchableOpacity>
            </View>
            <View style={styles.callIconContainer}>
              <TouchableOpacity onPress={() => callNumber(addressDetails.delivery_phone) }>
                <Image source={images.callIcon} style={styles.locationIcon} />
              </TouchableOpacity>
            </View>
          </View>}
      </View>
      <View style={styles.addressInfoView}>
        {showIcon && <Text style={styles.mediumLabel}>{addressDetails.delivery_name}</Text>}
        <Text style={styles.mediumLabel}>{showIcon ? addressDetails.delivery_address : addressDetails.billing_address}</Text>
        <Text style={styles.mediumLabel}>{showIcon ? addressDetails.delivery_city : addressDetails.billing_city}</Text>
        <Text style={styles.mediumLabel}>{showIcon ? addressDetails.delivery_country : addressDetails.billing_country}</Text>
        <Text style={styles.mediumLabel}>{showIcon ? addressDetails.delivery_zip : addressDetails.billing_zip}</Text>
        <Text style={styles.mediumLabel}>{showIcon ? addressDetails.delivery_phone : addressDetails.billing_phone}</Text>
      </View>
    </View>
  );
};
AddressComponent.propTypes = {
  showIcon: PropTypes.bool,
  addressType: PropTypes.string,
  addressDetails:PropTypes.object
};

export default AddressComponent;