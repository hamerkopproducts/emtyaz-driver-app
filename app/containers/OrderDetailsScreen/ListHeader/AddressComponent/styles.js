import { StyleSheet,I18nManager } from "react-native";
import globals from "../../../../lib/globals";
const images = {
  locationIcon: require("../../../../assets/images/listCard/locationIcon.png"),
  callIcon: require("../../../../assets/images/listCard/call.png"),
};


const styles = StyleSheet.create({
  addressView: {
    paddingTop:10,
    paddingBottom:10,
  },
  addressHeadingView: {
    flexDirection:'row',
    paddingTop: 5,
    paddingBottom: 5,
  },
  addressInfoView: {
  },
  addressLabelView:{
    width:'80%',
    justifyContent:'center'
    
  },
  addressIconView: {
    width: '20%',
    flexDirection: 'row'
  },
  mediumLabel: {
    width: '100%',
    textAlign: 'left',
    marginTop: 2,
    color: globals.COLOR.textColor,
    fontFamily: globals.FONTS.avenirMedium,
    fontSize: 12
  },
  lightLabel: {
    width: '100%',
    textAlign: 'left',
    color: globals.COLOR.lightTextColor,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.avenirLight,
    fontSize: 12
  },
  locationIconContainer: {
    width: '50%',
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  callIconContainer: {
    width: '50%',
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  locationIcon: {
    width: 30,
    height: 30
  },
});

export { images, styles };
