import { StyleSheet,I18nManager } from "react-native";
import globals from "../../../lib/globals";

const images = {
  
};

const styles = StyleSheet.create({
  headerContainer: {
   paddingTop:0
  },
  statusLabel:{
    paddingTop: 10,
    paddingBottom:10,
    width:'100%',
    textAlign:'center',
    color: globals.COLOR.textColorGreen,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.avenirMedium,
    fontSize: 14
  }
  
});

export { images, styles };
