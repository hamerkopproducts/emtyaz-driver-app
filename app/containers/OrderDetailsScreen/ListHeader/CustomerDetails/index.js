import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Image, Text } from 'react-native';
import { images, styles } from "./styles";
import appTexts from "../../../../lib/appTexts";

const CustomerDetails = (props) => {
  const {
    customerName,
  } = props;

  return (
    <View style={styles.orderInfoRowView}>
    <View style={styles.orderInfoView}>
      <View style={styles.scheduleContainer}>
          <Text style={styles.lightLabel}>{appTexts.STRING.customerName}</Text>
          <Text style={styles.boldLabel}>{customerName}</Text>
      </View>
    </View>
    </View>
  );
};
CustomerDetails.propTypes = {
  customerName:PropTypes.string
};

export default CustomerDetails;