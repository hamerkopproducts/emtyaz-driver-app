import React, { Component } from "react";
import PropTypes from "prop-types";
import { View } from "react-native";
import { styles } from "./styles";
import appTexts from "../../../lib/appTexts";
import globals from "../../../lib/globals";
import AddressComponent from "./AddressComponent";
import OrderInfoComponent from "./OrderInfoComponent";
import CustomerDetails from "./CustomerDetails";
import LeftAndRightLabel from "../LeftAndRightLabel";
import DotedDivider from "../../../components/DotedDivider";
import { Text } from "native-base";

const ListHeader = (props) => {
  const { detailsData, fromScreen, selectedTabIndex } = props;
  const status_id = detailsData.status_id;
  return (
    <View style={styles.headerContainer}>
      <Text
        style={[
          styles.statusLabel,
          (status_id != 5 && status_id != 6) && { color: globals.COLOR.tabUnderLineColor },
          (status_id === 5 || status_id === 6) && { color: globals.COLOR.redTextColor },
        ]}
      >
        {detailsData.status}
      </Text>
      <DotedDivider />
      <OrderInfoComponent
        orderId={detailsData.order_id}
        deliverySchedule={
          detailsData.delivery_schedule_date + "," + detailsData.time_slot
        }
      />
      <DotedDivider />
      <CustomerDetails customerName={detailsData.customer_name} />
      <DotedDivider />
      <AddressComponent
        loc={detailsData.delivery_loc_coordinates}
        showIcon={true}
        addressType={appTexts.STRING.deliveryAddress}
        addressDetails={detailsData.DeliveryAddress}
      />
      <DotedDivider />
      <AddressComponent
        showIcon={false}
        addressType={appTexts.STRING.billingAddress}
        addressDetails={detailsData.billingAddress}
      />
      <LeftAndRightLabel
        leftLabel={appTexts.STRING.orderDetails}
        rightLabel={detailsData.item_count + " " + appTexts.STRING.items}
      />
    </View>
  );
};
ListHeader.propTypes = {
  detailsData: PropTypes.object,
  fromScreen: PropTypes.string,
  selectedTabIndex: PropTypes.number,
};

export default ListHeader;
