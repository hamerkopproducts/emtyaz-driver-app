import { StyleSheet,I18nManager } from "react-native";
import globals from "../../../../lib/globals";
const images = {};
 

const styles = StyleSheet.create({
  orderInfoRowView:{
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  orderInfoView: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    paddingTop:15,
    paddingBottom:15
  },
  scheduleContainer: {
    width: '50%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  locationContainer: {
    width: '50%',
    justifyContent: 'center',
    alignItems: 'center'
  }, 
  labelContainer:{
    justifyContent: 'center',
    alignItems: 'center',
    position:'absolute',
    right:0
  },
  boldLabel: {
    width: '100%',
    textAlign: 'left',
    color: globals.COLOR.textColor,
    fontFamily: globals.FONTS.avenirHeavy,
    fontSize: 12
  },
  lightLabel: {
    width: '100%',
    textAlign: 'left',
    paddingBottom: 5,
    color: globals.COLOR.lightTextColor,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.avenirLight,
    fontSize: 12
  },
  mediumLabel:{
    width: '100%',
    textAlign: 'left',
    color: globals.COLOR.textColorGreen,
    fontFamily: globals.FONTS.avenirMedium,
    fontSize: 12
  }
});

export { images, styles };
