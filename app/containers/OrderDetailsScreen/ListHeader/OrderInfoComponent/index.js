import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Image, Text } from 'react-native';
import { images, styles } from "./styles";
import appTexts from "../../../../lib/appTexts";
import DotedDivider from "../../../../components/DotedDivider";

const OrderInfoComponent = (props) => {
  const {
    orderId,
    deliverySchedule
  } = props;

  return (
    <View style={styles.orderInfoRowView}>
    <View style={styles.orderInfoView}>
      <View style={styles.scheduleContainer}>
          <Text style={styles.lightLabel}>{appTexts.STRING.orderId}</Text>
          <Text style={styles.boldLabel}>{orderId}</Text>
      </View>
      <View style={styles.locationContainer}>
        <View style={styles.labelContainer}>
            <Text style={styles.lightLabel}>{appTexts.STRING.deliverySchedule}</Text>
            <Text style={styles.mediumLabel}>{deliverySchedule}</Text>
        </View>
      </View>
    </View>
    </View>
  );
};
OrderInfoComponent.propTypes = {
  orderId:PropTypes.string,
  deliverySchedule: PropTypes.string,
};

export default OrderInfoComponent;