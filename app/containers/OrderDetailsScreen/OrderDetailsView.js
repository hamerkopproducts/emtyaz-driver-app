import React from "react";
import PropTypes from "prop-types";
import { View, FlatList } from "react-native";
import globals from "../../lib/globals";
import { styles } from "./styles";
import OrderDetailsListItem from "../../components/OrderDetailsListItem";

import Header from "../../components/Header";
import ListHeader from "./ListHeader";
import ListFooter from "./ListFooter";
import RoundButton from "../../components/RoundButton";
import appTexts from "../../lib/appTexts";
import Loader from "../../components/Loader";
import PopupModal from "../../components/PopupModal";

const OrderDetailsView = (props) => {
  const {
    fromScreen,
    detailsData,
    selectedTabIndex,
    onBackButtonClick,
    isLoading,
    markStatusClick,
    popupIndex,
    isForReturn,
    popupYesClick,
    popupNoClick,
    markAsReturnButtonClick,
    navigateToNotification
  } = props;

  console.debug( detailsData , 'Details Data' );

  _keyExtractor = (item, index) => index.toString();

  return (
    <View style={styles.screenMain}>
      <Header
        onBackButtonClick={onBackButtonClick}
        isLogoRequired={false}
        headerTitle={appTexts.STRING.myOrders}
        isBackButtonRequired={true}
        customHeaderStyle={{
          height: globals.INTEGER.headerHeight,
          alignItems: "center",
          backgroundColor: globals.COLOR.headerColor,
        }}
        onRightButtonPress={navigateToNotification}
      />
      {isLoading && <Loader />}
      <View
        style={
          fromScreen === "home"
            ? styles.screenContainerWithMargin
            : styles.screenContainer
        }
      >
        {detailsData && detailsData !== null ? (
          <FlatList
            ListHeaderComponent={
              <ListHeader
                fromScreen={fromScreen}
                detailsData={detailsData}
                selectedTabIndex={selectedTabIndex}
              />
            }
            ListFooterComponent={
              <ListFooter
                fromScreen={fromScreen}
                detailsData={detailsData}
                selectedTabIndex={selectedTabIndex}
                markAsReturnButtonClick={markAsReturnButtonClick}
              />
            }
            style={styles.flatListStyle}
            data={detailsData.ProductDetails}
            extraData={detailsData.ProductDetails}
            keyExtractor={this._keyExtractor}
            showsVerticalScrollIndicator={false}
            renderItem={({ item, index }) => (
              <OrderDetailsListItem itemData={item} />
            )}
          />
        ) : null}
      </View>
      {fromScreen === "home" && (
        <View style={styles.bottomButtonContainer}>
          <View style={styles.buttonContainer}>
            <RoundButton
              buttonPosition={"center"}
              buttonText={
                selectedTabIndex === 0
                  ? appTexts.STRING.markAsDelivered
                  : (selectedTabIndex === 1
                  ? appTexts.STRING.markAsCollected
                  : appTexts.STRING.markSelectedAsReturned)
              }
              buttonClick={markStatusClick}
            />
          </View>
        </View>
      )}
      <PopupModal
        popupNoClick={popupNoClick}
        popupYesClick={popupYesClick}
        popupIndex={isForReturn ? 2 : popupIndex}
        selectedOrderId={detailsData.order_id}
        amountForTheItem={detailsData.grant_total}
      />
    </View>
  );
};

OrderDetailsView.propTypes = {
  selectedTabIndex: PropTypes.number,
  fromScreen: PropTypes.string,
  detailsData: PropTypes.object,
  onBackButtonClick: PropTypes.func,
  isLoading: PropTypes.bool,
  popupIndex: PropTypes.number,
  isForReturn: PropTypes.bool,
  markStatusClick: PropTypes.func,
  popupYesClick: PropTypes.func,
  popupNoClick: PropTypes.func,
  markAsReturnButtonClick: PropTypes.func,
};

export default OrderDetailsView;
