import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Image, Text } from 'react-native';
import { images, styles } from "./styles";
import LeftAndRightLabel from "../LeftAndRightLabel";
import HeadingAndParagraph from "./HeadingAndParagraph";
import ReturnButtonView from "./ReturnButtonView";
import DividerLine from "../../../components/DividerLine";
import DotedDivider from "../../../components/DotedDivider";
import appTexts from "../../../lib/appTexts";
const ListFooter = (props) => {
  const {
    detailsData,
    fromScreen,
    selectedTabIndex,
    markAsReturnButtonClick
  } = props;

  return (

    <View style={styles.headerContainer}>
      <LeftAndRightLabel leftLabel={appTexts.STRING.orderSummary} rightLabel={appTexts.STRING.currency + ' ' +detailsData.grant_total} />
      <LeftAndRightLabel leftLabel={appTexts.STRING.subtotal} rightLabel={appTexts.STRING.currency + ' ' +detailsData.sub_total} />
      {detailsData.discount > 0 &&
        <LeftAndRightLabel leftLabel={appTexts.STRING.discount} rightLabel={appTexts.STRING.currency + ' ' + detailsData.discount} />
      }
      {detailsData.delivery_charge > 0 &&
        <LeftAndRightLabel leftLabel={appTexts.STRING.deliveryCharge} rightLabel={appTexts.STRING.currency + ' ' +detailsData.delivery_charge} />
      }
      {detailsData.vat > 0 &&
        <LeftAndRightLabel leftLabel={appTexts.STRING.vatPercentage} rightLabel={appTexts.STRING.currency + ' ' +detailsData.vat.toFixed(2)} />
      }
      <DividerLine/>
      <LeftAndRightLabel leftLabel={appTexts.STRING.totalWithTax} rightLabel={appTexts.STRING.currency + ' ' +detailsData.grant_total} />
      <DotedDivider/>
      <LeftAndRightLabel leftLabel={appTexts.STRING.paymentMethod} rightLabel={detailsData.payment_method} />
      {(detailsData.order_note != '' && detailsData.order_note != null) &&
        <HeadingAndParagraph headingLabel={appTexts.STRING.note} paragraphText={detailsData.order_note}/>
      }

      {fromScreen === 'home' && (selectedTabIndex === 0 ) ? <DotedDivider />:null}
      {fromScreen === 'home' && (selectedTabIndex === 0) ? <ReturnButtonView linkButtonClick={markAsReturnButtonClick}/> :null}
    </View>


  );
};
ListFooter.propTypes = {
  detailsData:PropTypes.object,
  fromScreen: PropTypes.string,
  selectedTabIndex: PropTypes.number,
  markAsReturnButtonClick: PropTypes.func
};

export default ListFooter;