import { StyleSheet,I18nManager } from "react-native";
import globals from "../../../../lib/globals";

const images = {

};

const styles = StyleSheet.create({
  mainContainer: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewContainer: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  leftViewContainer: {
    width: '70%'
  },
  rightViewContainer: {
    width: '30%',
    justifyContent: 'flex-end',
    alignItems: 'flex-end'
  },
  labelText: {
    width: '100%',
    textAlign: 'left',
    color: globals.COLOR.textColor,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.avenirLight,
    fontSize: 12
  },
  linkView: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end'
  }
});

export { images, styles };
