import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Text } from 'react-native';
import { images, styles } from "./styles";
import appTexts from "../../../../lib/appTexts";
import globals from "../../../../lib/globals";
import LinkButton from "../../../../components/LinkButton";

const ReturnButtonView = (props) => {
  const {
    linkButtonClick
  } = props;

  return (

    <View style={styles.mainContainer}>
      <View style={styles.viewContainer}>
        <View style={styles.leftViewContainer}>
          <Text style={styles.labelText}>{appTexts.STRING.returnReasonText}</Text>
        </View>
        <View style={styles.rightViewContainer}>
          <View style={styles.linkView}>
            <LinkButton buttonText={appTexts.STRING.markAsReturned} linkColor={globals.COLOR.linkTextColor} isUnderLine={true} buttonClick={linkButtonClick}/>
          </View>
        </View>
      </View>
    </View>


  );
};
ReturnButtonView.propTypes = {
  linkButtonClick:PropTypes.func
};

export default ReturnButtonView;