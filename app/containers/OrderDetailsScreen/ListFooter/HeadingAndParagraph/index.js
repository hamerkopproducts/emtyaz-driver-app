import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Image, Text } from 'react-native';
import { images, styles } from "./styles";


const HeadingAndParagraph = (props) => {
  const {
    headingLabel,
    paragraphText
  } = props;

  return (

    <View style={styles.paragraphContainer}>
      <Text style={styles.mediumLabel}>{headingLabel}</Text>
      <Text style={styles.lightLabel}>{paragraphText}</Text>
    </View>


  );
};
HeadingAndParagraph.propTypes = {
  headingLabel:PropTypes.string,
  paragraphText: PropTypes.string
};

export default HeadingAndParagraph;