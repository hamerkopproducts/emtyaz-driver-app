import { StyleSheet,I18nManager } from "react-native";
import globals from "../../../../lib/globals";

const images = {

};

const styles = StyleSheet.create({
    paragraphContainer: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop:20,
    paddingBottom: 20
  },
  mediumLabel: {
    width: '100%',
    textAlign: 'left',
    paddingBottom: 10,
    color: globals.COLOR.textColor,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.avenirHeavy,
    fontSize: 12
  },
  lightLabel: {
    width: '100%',
    textAlign: 'left',
    color: globals.COLOR.textColor,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic :  globals.FONTS.avenirLight,
    fontSize: 12
  },

});

export { images, styles };
