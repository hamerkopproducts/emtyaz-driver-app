import { StyleSheet,I18nManager } from "react-native";
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
  } from "react-native-responsive-screen";
import globals from "../../lib/globals";
let headerButtonContainerWidth =
  globals.SCREEN_SIZE.width - globals.MARGIN.marginTen * 2;
let headerButtonWidth =
  (headerButtonContainerWidth - globals.MARGIN.marginTen * 2) / 3;
const images = {
  faqIcon: require("../../assets/images/profileicon/faq.png"),
  privacyIcon: require("../../assets/images/profileicon/privacy.png"),
  termsIcon: require("../../assets/images/profileicon/Terms.png"),
  supportIcon: require("../../assets/images/profileicon/support.png"),
  logoutIcon:require("../../assets/images/profileicon/logout.png"),
  arrowIcon:require("../../assets/images/profileicon/forward_pink.png")
};
const styles = StyleSheet.create({
  screenMain: {
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
    flexDirection: "column",
  },
  imageContainer:{
    flex:0.4,
    //backgroundColor:'red',
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
    alignItems: 'center',
    paddingTop:'10%'
    
  },
  languageContainer:{
    flex:1,
    //backgroundColor:'green',
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
    //alignItems: 'center',
    paddingTop:'25%'
    
  },
  screenContainer:{
    flex:1,
    // backgroundColor: globals.COLOR.transparent,
    backgroundColor:'white',
    marginBottom: globals.INTEGER.screenBottom
  },
  headerWrapper:{
   flex:.1,
   //backgroundColor:'pink',
   flexDirection:'row',
   alignItems:'center'

  },
  formWrapper:{
flex:1,
backgroundColor:'white',

  },
  sendPhone:{
    flexDirection:'row',
    paddingTop:'7%'
   },
   otpWrapper:{
    flexDirection: "row",
    justifyContent: "space-around",
    width: "80%",
    paddingTop: 5,
   },  engButton: {
    width: 142, 
    height:50,
    borderRadius: 15,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor:'#ff8001',
    
},
  borderView:{ 
    width: "12%",
    height: 2,
    //borderRadius: 5,
    marginTop: 10,
    marginBottom: 5,
    backgroundColor: "#ff8001",
    alignItems:'center',justifyContent:'center'
  },
  textInput:{
    paddingLeft: 0,
    height: 50,
    fontSize: 13,
    width: "15%",
    //borderColor: "gray",
    //borderBottomWidth: 1.5,
    textAlign: "center",
  },
      verifyText: {
        fontSize:hp('2.4%'),
        color:'#232020',
        fontFamily: globals.FONTS.avenirMedium,
},
verifyWrapper:{
 paddingLeft:'12%'
  
},
editText:{
  color:'#ff8001',
  paddingLeft:'6%',
  fontSize:16
},
imageWrapper: {
   paddingLeft:'5%'

},
// arrowImage:{
//   width:45,
//   height:45,
//   resizeMode:'contain',
//   paddingLeft:'5%'
  
// },
digitCode:{
  fontSize:17
},
phoneNumber:{
  fontSize:15
},
enterDigit:{
  paddingTop:'25%'
},
reseondCode:{
  paddingTop:'9%'
},
buttonWrapper:{
  paddingTop:'20%'
},
submitText:{
  color:'white'
},
contentWrapper:{
 
  paddingTop:hp('2%'),
  //borderBottomWidth:1,
  //borderBottomColor:'red',
  //paddingLeft:4
},
headingText:{
  fontSize:hp('2.1%'),
  color:'#232020',
  fontFamily: globals.FONTS.avenirMedium,
},descriptionText:{
  paddingTop:hp('1.5%'),
  fontSize:hp('1.8%'),
  color:'#232020',
  lineHeight:22,
  fontFamily: globals.FONTS.avenirMedium,
},
profileWrapper:{
  flexDirection:'row',
  justifyContent:'space-between',
  paddingLeft:'1%',
  paddingRight:'1%',
 
  
  //alignItems:'center'
  
},
editSection:{
  flexDirection:'row',
  justifyContent:'flex-start'
},
imageSection:{
marginLeft:'3%',
//borderRadius: 40,
borderWidth: 1,
borderColor: 'lightgray',
borderRadius: 14
},
editButton:{
  paddingRight:'4%',
  paddingTop:hp('1.5%')
},
detailsWrapper:{
  flexDirection:'column',flexWrap:'wrap',justifyContent:'center',paddingLeft:'5%'
},
logo:{
  width: 85,
  height: 85,
  resizeMode: 'cover',
  borderRadius:14
},
modallogo:{
  width: 100,
  height: 97,
  resizeMode: 'cover',
  borderRadius:18,
  alignSelf:'center'
},
modalimageWarpper:{
//backgroundColor:'red'
alignSelf:'center',
borderWidth: 1,
borderColor: 'lightgray',
borderRadius: 18
},
Name:{
  fontFamily: globals.FONTS.avenirHeavy,
  fontSize:16
  //fontSize:I18nManager.isRTL ? hp('2.3%'): hp('2.2%'),
},
Email:{
  fontSize:12,
  fontFamily: globals.FONTS.avenirMedium,
  //fontSize:I18nManager.isRTL ? hp('1.7%'): hp('1.6%'),
  color:'#707070'
},

Phone:{
  fontSize:12,
  fontFamily: globals.FONTS.avenirMedium,
  //fontSize:I18nManager.isRTL ? hp('1.7%'):hp('1.6%'),
  color:'#707070'
},
editName:{
  //fontFamily: globals.FONTS.avenirHeavy,
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiarabicBold : globals.FONTS.avenirHeavy,
  //fontSize:hp('1.7%'),
  fontSize:12,
  color:'#ff8001'
},
listWrapper:{
  flexDirection:'row',
  justifyContent:'space-between',
  alignItems:'center',
 //paddingTop:hp('3%'),
 paddingBottom:hp('5%'),
 paddingLeft:'3%'
},
mainList:{
paddingTop:hp('3%')
},
imageMain:{
  resizeMode: "contain", width: 30, height: 30,
},
contentHeading:{
  fontFamily: globals.FONTS.avenirMedium,
  fontSize:hp('2%'),
  paddingLeft:'4%',
  paddingRight:'4%'
  //color:'#707070'
},
contentsWrapper:{
//paddingLeft:'8%'
},
imagemainWrapper:{
  //paddingLeft:'5%'
},
arrowWrapper:{
  flexDirection:'row',
  paddingRight:'3%'
},
arrowImage:{resizeMode: "contain",  width: 16,
height: 16,transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]},


modalMainContent: {
  //justifyContent: "center",
  justifyContent:'flex-end',
  margin: 0
},
modalmainView: {
  backgroundColor: "white",
  //width: wp("90%"),
  padding: "4%",
  //borderRadius: 10,
  borderTopRightRadius:15,
  borderTopLeftRadius:15,
  borderColor: "rgba(0, 0, 0, 0.1)",
},
buttonwrapper: {
  flexDirection: "row",
   justifyContent:'flex-end'

},
textWarpper:{
  flexDirection:'row',
  justifyContent:'center',
  paddingTop:hp('1%'),
  paddingBottom:hp('1.5%')
  },
  modaltextWarpper:{
    flexDirection:'row',
    justifyContent:'center',
    //paddingTop:hp('1%'),
    paddingBottom:hp('2.5%')
  },
  headText:{
    fontSize:16,
    //fontSize: hp('2.5%'),
    //fontFamily: globals.FONTS.avenirHeavy,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.avenirHeavy,
  },
  whitebuttonText:{
    fontSize: hp('2.1%'),
    fontFamily: globals.FONTS.avenirMedium,
    color:'#FF8001'
  },
  yellowbuttonText:{
    fontSize: hp('2.1%'),
    //fontFamily: globals.FONTS.avenirMedium,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.avenirMedium,
    color:'white'
  },
  subText:{
    textAlign: 'left',
    fontSize: hp('2%'),
    color:"#6f6f6f",
    paddingLeft:'1%',
    paddingRight:'2%',
    //fontFamily: globals.FONTS.avenirMedium,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.avenirMedium,
  },subjectWrapper:{
  paddingTop:hp('2%'),
  //marginTop:-3
  
  },messageWrapper:{
    paddingTop:hp('2%'),
  },
  input: {
  
    height: 50,
    fontFamily: globals.FONTS.avenirMedium,
    //fontSize: hp('2.4%'),
    fontSize:15,
    color:'#2e2e2e',
    //backgroundColor:'red',
    textAlign: I18nManager.isRTL ? "right" : "left",
  },
  fixedInput:{
    fontFamily: globals.FONTS.avenirMedium,
    fontSize:15,
    //fontSize: hp('2.1'),
    padding:'10%',
    //padding:6,
    
    
    
  },
  
  numberInput:{
    fontFamily: globals.FONTS.avenirMedium,
    textAlign: I18nManager.isRTL ? "right" : "left",
    height:50,
    fontSize:15,
    //fontSize: hp('2.4%'),
    alignItems: "center",
    //borderBottomColor: "lightgray",
    //borderBottomWidth: 1,
  },
  mobilnumberWrapper:{
    flexDirection:'row'
  },
  flagWarpper:{
    flexBasis:'20%',
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    
  },phonenumberWrapper:{
    flexBasis:'80%'
  },
  buttoninsidewrapper: {
    flexDirection:  "row",
    justifyContent: "space-around",
    paddingTop: "3%",
    paddingBottom: "7%",
  },yButtonon:{
    width: 140,
      height: 50,
      borderRadius: 15,
      alignItems: "center",
      justifyContent: "center",
      // shadowOpacity: 0.58,
      // shadowRadius: 16.00,
      // elevation:5,
      // borderColor:'#d0d0d0',
      // borderWidth:1
  },
  buttononview: {
      alignItems: "center",
      justifyContent: "center",
      marginTop: hp('2.5%'),
      
  
  },
  SectionStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    //borderWidth: .5,
    borderBottomColor:'lightgray',
    borderBottomWidth:1,
    height: 41,
    borderRadius: 5 ,
    //margin: 10
},

modalMaincontentHelp: {
  //justifyContent: "center",
  justifyContent:'flex-end',
  margin: 0
},

modalmainviewHelp: {
  backgroundColor: "white",
  //width: wp("90%"),
  padding: "4%",
  //borderRadius: 10,
  borderTopRightRadius:15,
  borderTopLeftRadius:15,
  borderColor: "rgba(0, 0, 0, 0.1)",
},
buttohelpnwrapper: {
  flexDirection: "row",
  paddingLeft:'12%'
  //paddingRight:'4%'
  //paddingLeft:'30%'
  //backgroundColor:'red',
  //alignContent:'flex-end'
  //alignItems: 'center'
  //alignSelf:"flex-end",
  //paddingRight:'10%'
   //justifyContent:'flex-end'

},
helptextWarpper:{
  flexDirection:'row',
  justifyContent:'center',
  paddingTop:hp('2%'),
  paddingBottom:hp('1.5%'),
  //paddingLeft:'19%'
  },
  helpheadText:{
    //fontSize: hp('2.6%'),
    fontSize:16,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.avenirHeavy,
    //fontFamily: globals.FONTS.avenirHeavy,
    //textAlign:'center'
  },
  helpsubjectWrapper:{
    paddingTop:hp('2%'),
    //marginTop:-3
    
    },
    helpsubText:{
      textAlign: 'left',
      fontSize: hp('2.1%'),
      color:"#6f6f6f",
      paddingLeft:'1%',
      fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.avenirMedium,
    },
    hinput: {
      //marginTop:-4
      // margin: 15,
      // height: 40,
      // borderColor: '#7a42f4',
      // borderWidth: 1
      fontFamily: globals.FONTS.avenirMedium,
      fontSize: hp('2.6%'),
      textAlign: I18nManager.isRTL ? "right" : "left",
      color:"black",
    },
    helpmessageWrapper:{
      paddingTop:hp('2%'),
    },
    helpbuttoninsidewrapper: {
      flexDirection:  "row",
      justifyContent: "space-around",
      paddingTop: "3%",
      paddingBottom: "7%",
    },
    helpbuttononview: {
      alignItems: "center",
      justifyContent: "center",
      marginTop: hp('2.5%'),
      
  
  },
  hylpyButtonon:{
    width: 140,
      height: 50,
      borderRadius: 15,
      alignItems: "center",
      justifyContent: "center",
      // shadowOpacity: 0.58,
      // shadowRadius: 16.00,
      // elevation:5,
      // borderColor:'#d0d0d0',
      // borderWidth:1
  },
  helpyellowbuttonText:{
    fontSize: hp('2.1%'),
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.avenirMedium,
    color:'white'
  },
  phoneWrapper:{
    alignItems: "center",
    justifyContent: "center",
   // flex:1
  },
  newflagWrapper:{
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",    
    //width: wp("20%")
    backgroundColor: 'lightgray'
  },
  phoneNumberContainer:{
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    //backgroundColor:'green'
  },
  newinputWrapper:{
    alignItems: "flex-start",
    justifyContent: "flex-start",
     marginLeft: "2%" ,
       width: "79%"
  }

});

export { images, styles };
