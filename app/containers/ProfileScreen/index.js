import React, { useEffect, useState } from 'react';
import { Keyboard } from "react-native";
import NetInfo from "@react-native-community/netinfo";
import { connect } from 'react-redux';
import ProfileView from './ProfileView';

import { bindActionCreators } from "redux";
import * as loginActions from "../../actions/loginActions";
import appTexts from "../../lib/appTexts";
import functions from "../../lib/functions"
import Toast from 'react-native-toast-message';
import ServiceWrapperAwait from '../../service/ServiceWrapperAwait';

const ProfileScreen = (props) => {

  //Initialising states
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isHelpModalVisible, setIsHelpModalVisible] = useState(false);

  const [loader, setLoader] = useState(false);
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [phone, setPhone] = useState('');
  const [photo, setPhoto] = useState('');
  const [imageData, setPhotoData] = useState({ logo: null });

  const [popupIndex, setPopupIndex] = useState(-1);
  const [subject,setSubject] = useState('');
  const [message,setMessage] = useState('');

  const driverData = async () => {
    const sAsyncWrapper = new ServiceWrapperAwait();
    setLoader(true);
    const response = await sAsyncWrapper.get('profile', 
      {language_attach: false, is_auth_required: true}
    );
    setLoader(false);
    try {
      if (response.success == true) {
          setName(response.data.driver.name);
          setEmail(response.data.driver.email);
          setPhone(response.data.driver.phone);
          setPhoto(response.data.driver.photo);
      }
    } catch (err) {
      console.debug('-->err', err);
    }
  }

  //will focus
  useEffect(() => {
    return props.navigation.addListener('focus', () => {
      if (props.currentScreen !== 'MyProfile') {
        props.setCurrentScreen('MyProfile')
      }
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
  
        } else {
          functions.displayToast('error', 'top', 'Network Error', appTexts.ALERT_MESSAGES.noInternet);
        }
      });
    });
  }, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    if (props.currentScreen !== 'MyProfile') {
      props.setCurrentScreen('MyProfile')
    }
    driverData();
  }

  //unmount
  useEffect(() => { return () => { handleComponentUnmount(); } }, []);

  const handleComponentUnmount = () => {
   
  };
 
  //updated
  useEffect(() => handleComponentUpdated(),);

  const handleComponentUpdated = () => {

    if(props.chatData && props.chatData.success == true) {

    console.debug(props.chatData)

      props.chatSupportstatus();
      Toast.show({
        type: 'success',
        position: 'top',
        text1: appTexts.ALERT_MESSAGES.success,
        text2: props.chatData.msg,
        visibilityTime: 2000,
        autoHide: true,
        topOffset: 30,
        bottomOffset: 40,
        onHide: () => { toggleHelpModal(), setSubject(''),setMessage('')}
      });
    }

    if (props.isLoggedOut) {
      props.updatedLogoutStatus()
      props.navigation.navigate('AuthStackNavigator', { screen: 'AppIntrosliderloginScreen' });
    }
    if (props.profileUpdate && props.profileUpdate.success){
      props.updateprofileUpdateStatus();
      Toast.show({
        type: 'success',
        position: 'top',
        text1: appTexts.ALERT_MESSAGES.success,
        text2: appTexts.ALERT_MESSAGES.profileUpdatedSuccess,
        visibilityTime: 2000,
        autoHide: true,
        topOffset: 30,
        bottomOffset: 40,
        onHide: () => {toggleModal()}
      });
    }
  };

  const validateEmail = (emailField) => {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (reg.test(emailField) == false) {
      return false;
    }

    return true;
  };

  const updateProfile = async(_data) => {
    const sAsyncWrapper = new ServiceWrapperAwait();
    setLoader(true);
    const response = await sAsyncWrapper.post('profile/update', _data, 'multipart/form-data');
    try {
      const data = new Promise((resolve, reject) => {
        setLoader(false);
        try {
          resolve(response);
        } catch (err) {
          reject(err);
        }
      });
  
      return data;
    } catch (err) {
      setLoader(false);
    }
  }

  const saveUserData = async(_data, userData) => {
    data = await updateProfile(_data);
    if(data.success == true) {
      driverData();
      props.updateProfile( data, userData);
    } else {
      functions.displayToast('error', 'top', 'Error' , data.error.msg);
    }
  }

  const onUpdatePress = () => {
    //setIsModalVisible(false);
    if (name.trim() === '' || name === null){
      functions.displayToast('error', 'top', appTexts.ALERT_MESSAGES.error, appTexts.ALERT_MESSAGES.nameRequired);
      return;
    } else if (email.trim() === '' || email === null){
      functions.displayToast('error', 'top', appTexts.ALERT_MESSAGES.error, appTexts.ALERT_MESSAGES.emailRequired);
      return;
    } else if (validateEmail(email.trim()) === false){
      functions.displayToast('error', 'top', appTexts.ALERT_MESSAGES.error, appTexts.ALERT_MESSAGES.validEmailRequired);
      return;
    }
    else if (phone.trim() === '' || phone === null) {
      functions.displayToast('error', 'top', appTexts.ALERT_MESSAGES.error, appTexts.ALERT_MESSAGES.phoneRequired);
      return;
    }else{
      Keyboard.dismiss();
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          let userData = props.userData;
          userData.user.name = name.trim();
          userData.user.email = email.trim();

          let _data = new FormData();
          if(imageData.logo != null) {
            _data.append('photo', {
              name: imageData.nameImage,
              type: imageData.typeImage,
              uri: Platform.OS == 'android' ? imageData.logo.uri : imageData.logo.uri.replace("file://", "")
            });
          }
          _data.append('name', name.trim());
          _data.append('email', email.trim());
          saveUserData(_data, userData);
        } else {
          functions.displayToast('error', 'top', appTexts.ALERT_MESSAGES.error, appTexts.ALERT_MESSAGES.noInternet);
          return;
        }
      });
    }
  };

  const onChatPress = () => {
      if (subject.trim() === '' || subject === null){
      functions.displayToast('error', 'top', appTexts.ALERT_MESSAGES.error, appTexts.ALERT_MESSAGES.subjectRequired);
      return;
    } else if (message.trim() === '' || message === null){
      functions.displayToast('error', 'top', appTexts.ALERT_MESSAGES.error, appTexts.ALERT_MESSAGES.messageRequired);
      return;
    }
    else{
      Keyboard.dismiss();
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
            let apiParams = {
            "subject": subject.trim(),
            "message": message.trim(),
            };
          props.chatSupport( apiParams, props.userData.access_token);
        } else {
          functions.displayToast('error', 'top', appTexts.ALERT_MESSAGES.error, appTexts.ALERT_MESSAGES.noInternet);
          return;
        }
      });
    }
  };

  
  
  const logoutButtonPress = () => {
    setPopupIndex(7);
  };
  const privacyPolicyPress = () => {
    props.navigation.navigate('PrivacyScreen');
  };
  const faqPress = () => {
    props.navigation.navigate('FaqScreen');
  };
  const termsAndConditionPress = () => {
    props.navigation.navigate('TermsScreen');
  };
  const toggleHelpModal = () => {
    setIsHelpModalVisible(!isHelpModalVisible)
  };
  const toggleModal = () => {
    setName(props.userData.user.name)
    setEmail(props.userData.user.email)
    setPhone(props.userData.user.phone)
    setIsModalVisible(!isModalVisible)
  };

  const popupNoClick = () => {
    setPopupIndex(-1);
  };
  const popupYesClick = () => {
    setPopupIndex(-1);
    props.doLogout();
  };
  
  return (
    <ProfileView 
      logoutButtonPress={logoutButtonPress} 
      privacyPolicyPress={privacyPolicyPress}
      faqPress={faqPress}
      termsAndConditionPress={termsAndConditionPress}
      toggleHelpModal={toggleHelpModal}
      isModalVisible={isModalVisible} 
      isHelpModalVisible={isHelpModalVisible} 
      toggleModal={toggleModal}
      name={name}
      email={email}
      phone={phone}
      setName={setName}
      setEmail={setEmail}
      setPhone={setPhone}
      photo={photo}
      userDetails={props.userData && props.userData.user ? props.userData.user : null}
      onUpdatePress={onUpdatePress}
      imageData={imageData}
      setPhotoData={setPhotoData}
      popupIndex={popupIndex}
      popupNoClick={popupNoClick}
      popupYesClick={popupYesClick}
      subject={subject}
      setSubject={setSubject}
      setMessage={setMessage}
      message={message}
      isLoading={props.isLoading || loader}
      onChatPress={onChatPress}
      navigateToNotification={() => props.navigation.navigate('NotificationScreen') }
      />
  );

};


const mapStateToProps = (state, props) => {
  return {
    isLoggedOut: state.loginReducer.isLoggedOut,
    userData: state.loginReducer.userData,
    currentScreen: state.loginReducer.currentScreen,
    profileUpdate: state.loginReducer.profileUpdate,
    isLoading: state.loginReducer.isLoading,
    chatData: state.loginReducer.chatData  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    doLogout: loginActions.doLogout,
    updateProfile: loginActions.updateProfile,
    updatedLogoutStatus: loginActions.updatedLogoutStatus,
    updateprofileUpdateStatus: loginActions.updateprofileUpdateStatus,
    setCurrentScreen: loginActions.setCurrentScreen,
    chatSupport: loginActions.chatSupport,
    chatSupportstatus: loginActions.chatSupportstatus
  }, dispatch)
};

const profileScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfileScreen);

profileScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default profileScreenWithRedux;
