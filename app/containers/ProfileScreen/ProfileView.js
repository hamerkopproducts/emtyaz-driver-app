import React, { useState } from "react";
import PropTypes from "prop-types";
import { View, Text, TouchableOpacity, Image, TextInput } from "react-native";
import globals from "../../lib/globals";
import { styles, images } from "./styles";
import Modal from "react-native-modal";

import LinearGradient from "react-native-linear-gradient";
import appTexts from "../../lib/appTexts";
import ProfileItem from "./ProfileItem";
import DividerLine from "../../components/DividerLine";
import Toast from "react-native-toast-message";
import PopupModal from "../../components/PopupModal";
import functions from "../../lib/functions"

import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Header from "../../components/Header";
import Loader from "../../components/Loader";

import FastImageLoader from "../../components/FastImage/FastImage";
import { launchImageLibrary } from "react-native-image-picker";
import { CropView } from "react-native-image-crop-tools";
import RoundButton from "../../components/RoundButton";

const ProfileView = (props) => {
  const {
    logoutButtonPress,
    faqPress,
    privacyPolicyPress,
    termsAndConditionPress,
    toggleHelpModal,
    toggleModal,
    isModalVisible,
    isHelpModalVisible,
    name,
    email,
    phone,
    photo,
    setName,
    setEmail,
    setPhone,
    imageData,
    setPhotoData,
    userDetails,
    onUpdatePress,
    popupIndex,
    popupNoClick,
    popupYesClick,
    subject,
    setSubject,
    message,
    setMessage,
    onChatPress,
    isLoading,
    navigateToNotification,
  } = props;

  const pickLogo = async () => {
    const options = {
      title: "Select profile picture",
    };
    try {
      launchImageLibrary(options, (res) => {
        if (res.didCancel) {
          console.log("User cancelled image picker");
        } else if (res.error) {
          console.log("ImagePicker Error: ", res.error);
        } else {
          if (res.fileSize / 1000000 > 10) {
            functions.displayToast(
              "error",
              "top",
              "Error",
              "Image size should not exceed 10MB"
            );
            return false;
          }
          const ext = res.uri.substring(res.uri.lastIndexOf("."));

          setPhotoData({
            logo: res,
            logoUrl: null,
            selected: true,
            cropped: false,
            typeImage: res.type,
            nameImage: res.fileName,
          });
        }
      });
    } catch (err) {
      console.debug(err);
    }
  };

  const cropLogo = () => {
    const quality = Platform.OS == "ios" ? 0.5 : 50;
    cropViewRef.saveImage(true, quality);

    return false;
  };

  return (
    <View style={styles.screenMain}>
      <Header
        navigation={props.navigation}
        isLogoRequired={true}
        isNotificationRequired={true}
        isLanguageButtonRequired={true}
        customHeaderStyle={{
          height: globals.INTEGER.headerHeight,
          alignItems: "center",
          backgroundColor: globals.COLOR.headerColor,
        }}
        onRightButtonPress={navigateToNotification}
      />
      <View style={styles.screenContainer}>
        <View style={{ flex: 1, marginTop: hp("2%") }}>
          <View
            style={{ height: hp(".5%"), backgroundColor: "#f8f8f8" }}
          ></View>
          <View style={styles.formWrapper}>
            <View style={styles.contentWrapper}>
              <View style={styles.profileWrapper}>
                <View style={styles.editSection}>
                  <View style={styles.imageSection}>
                    {photo != null && (
                      <Image
                        source={{
                          uri: photo,
                        }}
                        style={styles.logo}
                      />
                    )}
                    {photo == null && (
                      <Image
                        source={require("../../assets/images/profileicon/profile-image.jpg")}
                        style={styles.logo}
                      />
                    )}
                  </View>
                  <View style={styles.detailsWrapper}>
                    <Text style={styles.Name}>
                      {userDetails ? userDetails.name : ""}
                    </Text>
                    <Text style={styles.Phone}>
                      {userDetails ? "+966 " + userDetails.phone : ""}
                    </Text>
                    <Text style={styles.Email}>
                      {userDetails ? userDetails.email : ""}
                    </Text>
                  </View>
                </View>
                <View style={styles.editButton}>
                  <TouchableOpacity
                    onPress={() => {
                      toggleModal();
                    }}
                  >
                    <Text style={styles.editName}>
                      {" "}
                      {appTexts.PROFILELISTING.Edit}
                      {/* Edit */}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={{ justifyContent: "center", flexDirection: "row" }}>
                <View
                  style={{
                    width: "95%",
                    borderWidth: 0.3,
                    borderColor: "lightgray",
                    borderRadius: 0.3,
                    borderStyle: "dashed",
                    marginTop: hp("1.3%"),
                    marginBottom: hp("1%"),
                  }}
                />
              </View>

              <View style={styles.mainList}>
                <ProfileItem
                  itemText={appTexts.PROFILELISTING.Faq}
                  itemImage={images.faqIcon}
                  arrowImage={images.arrowIcon}
                  onItemClick={faqPress}
                />
                <ProfileItem
                  itemText={appTexts.PROFILELISTING.Privacy}
                  itemImage={images.privacyIcon}
                  arrowImage={images.arrowIcon}
                  onItemClick={privacyPolicyPress}
                />
                <ProfileItem
                  itemText={appTexts.PROFILELISTING.Terms}
                  itemImage={images.termsIcon}
                  arrowImage={images.arrowIcon}
                  onItemClick={termsAndConditionPress}
                />
                <ProfileItem
                  itemText={appTexts.PROFILELISTING.Support}
                  itemImage={images.supportIcon}
                  arrowImage={images.arrowIcon}
                  onItemClick={toggleHelpModal}
                />
                <ProfileItem
                  itemText={appTexts.PROFILELISTING.Logout}
                  itemImage={images.logoutIcon}
                  onItemClick={logoutButtonPress}
                />
              </View>
            </View>
          </View>

          <Modal
            isVisible={isModalVisible}
            style={styles.modalMainContent}
            animationIn="fadeIn"
            animationOut="fadeOut"
          >
            {isLoading && <Loader />}
            <View style={styles.modalmainView}>
              <KeyboardAwareScrollView
                scrollEnabled={true}
                contentContainerStyle={{ justifyContent: "flex-end" }}
              >
                <View
                  style={{ flexDirection: "row", justifyContent: "center" }}
                >
                  <View
                    style={{
                      width: "25%",
                      height: 3,
                      borderRadius: 5,
                      marginTop: hp("0.5%"),
                      //marginBottom: hp('1%'),
                      backgroundColor: "#FFF0F5",
                    }}
                  />
                </View>
                <TouchableOpacity
                  style={styles.buttonwrapper}
                  onPress={() => {
                    toggleModal();
                  }}
                >
                  <Image
                    source={require("../../assets/images/Icons/close.png")}
                    style={{ resizeMode: "contain", width: 30, height: 30 }}
                  />
                </TouchableOpacity>
                <View style={styles.modaltextWarpper}>
                  <Text style={styles.headText}>
                    {appTexts.EDITPROFILE.Heading}
                  </Text>
                </View>
                <View style={styles.modalimageWarpper}>
                  <TouchableOpacity
                    onPress={() => {
                      pickLogo();
                    }}
                  >
                    {imageData.selected == false &&
                      imageData.cropped == true && (
                        <>
                          {imageData.logo != null && (
                            <>
                              {imageData.selected == false &&
                                imageData.cropped == true && (
                                  <Image
                                    source={{ uri: imageData.logo.uri }}
                                    style={styles.modallogo}
                                  />
                                )}
                            </>
                          )}
                        </>
                      )}

                    {imageData.logo == null && (
                      <>
                        {photo != null && (
                          <Image
                            source={{
                              uri: photo,
                            }}
                            style={styles.modallogo}
                          />
                        )}
                        {photo == null && (
                          <Image
                            source={require("../../assets/images/profileicon/profile-image.jpg")}
                            style={styles.modallogo}
                          />
                        )}
                      </>
                    )}
                    <Image
                      source={require("../../assets/images/Icons/camera.png")}
                      style={{
                        height: 26,
                        width: 26,
                        marginLeft: 6,
                        position: "absolute",
                        alignSelf: "center",
                        top: 82,
                      }}
                    />
                  </TouchableOpacity>
                </View>

                <View style={[styles.modalimageWarpper, { borderWidth: 0 }]}>
                  {imageData.selected == true && imageData.cropped == false && (
                    <CropView
                      style={{ width: 200, height: 200, margin: 25 }}
                      sourceUrl={imageData.logo.uri}
                      ref={(ref) => (cropViewRef = ref)}
                      onImageCrop={(res) =>
                        setPhotoData({
                          logo: res,
                          cropped: true,
                          selected: false,
                          typeImage: imageData.typeImage,
                          nameImage: imageData.nameImage,
                        })
                      }
                      keepAspectRatio
                      aspectRatio={{ width: 1, height: 1 }}
                    />
                  )}

                  {imageData.selected == true && imageData.cropped == false && (
                    <TouchableOpacity
                      style={{ alignSelf: "center" }}
                      onPress={() => cropLogo()}
                    >
                      <RoundButton buttonText={"Crop"} buttonClick={cropLogo} />
                    </TouchableOpacity>
                  )}
                </View>

                <View style={styles.formWrappers}>
                  <View style={styles.subjectWrapper}>
                    <Text style={styles.subText}>
                      {appTexts.EDITPROFILE.Name}
                    </Text>
                    <View style={{ marginTop: -10 }}>
                      <TextInput
                        value={name}
                        style={styles.input}
                        autoCapitalize="none"
                        onChangeText={(val) => {
                          setName(val);
                        }}
                      />
                      <DividerLine color={"lightgray"} />
                    </View>
                  </View>
                  <View style={styles.messageWrapper}>
                    <Text style={styles.subText}>
                      {appTexts.EDITPROFILE.Email}
                    </Text>
                    <View style={{ marginTop: -10 }}>
                      <TextInput
                        value={email}
                        style={styles.input}
                        autoCapitalize="none"
                        onChangeText={(val) => {
                          setEmail(val);
                        }}
                      />
                      <DividerLine color={"lightgray"} />
                    </View>
                  </View>

                  <View style={styles.messageWrapper}>
                    <Text style={styles.subText}>
                      {appTexts.EDITPROFILE.Phone}
                    </Text>

                    <View style={styles.phoneNumberContainer}>
                      <View style={styles.phoneWrapper}>
                        <View style={styles.newflagWrapper}>
                          <Image
                            source={require("../../assets/images/Icons/flag.png")}
                            style={{ height: 20, width: 20 }}
                          />
                          <TextInput
                            editable={false}
                            style={[styles.input, { width: wp("12%") }]}
                            placeholder={"+966"}
                            placeholderTextColor="#282828"
                          />
                        </View>
                        <DividerLine color={"lightgray"} />
                      </View>
                      <View style={styles.newinputWrapper}>
                        <TextInput
                          style={[
                            styles.input,
                            {
                              backgroundColor: "lightgray",
                              width: "100%",
                              paddingLeft: 5,
                            },
                          ]}
                          value={phone}
                          keyboardType="number-pad"
                          editable={false}
                          onChangeText={(val) => {
                            setPhone(val);
                          }}
                        />
                        <DividerLine color={"lightgray"} />
                      </View>
                    </View>
                  </View>
                </View>
                <View style={styles.buttoninsidewrapper}>
                  <TouchableOpacity
                    style={styles.buttononview}
                    onPress={() => {
                      onUpdatePress();
                    }}
                  >
                    <LinearGradient
                      start={{ x: 0, y: 0 }}
                      end={{ x: 1, y: 2.45 }}
                      colors={["#ff8001", "#fbc203"]}
                      style={styles.yButtonon}
                    >
                      <Text style={styles.yellowbuttonText}>
                        {appTexts.EDITPROFILE.Update}
                      </Text>
                    </LinearGradient>
                  </TouchableOpacity>
                </View>
              </KeyboardAwareScrollView>
            </View>
            <Toast ref={(ref) => Toast.setRef(ref)} />
          </Modal>
          <Modal
            isVisible={isHelpModalVisible}
            style={styles.modalMaincontentHelp}
            animationIn="fadeIn"
            animationOut="fadeOut"
          >
            {isLoading && <Loader />}
            <View style={styles.modalmainviewHelp}>
              <KeyboardAwareScrollView
                scrollEnabled={true}
                contentContainerStyle={{ justifyContent: "flex-end" }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    paddingTop: Platform.OS === "ios" ? "20%" : null,
                    //justifyContent: "center",
                    justifyContent: "space-around",
                    alignItems: "center",
                    paddingBottom: "3%",
                    paddingLeft: "20%",
                  }}
                >
                  <View style={styles.helptextWarpper}>
                    <Text style={styles.helpheadText}>
                      {appTexts.HELPPOPUP.Heading}
                    </Text>
                  </View>
                  <TouchableOpacity
                    style={styles.buttohelpnwrapper}
                    onPress={() => {
                      toggleHelpModal();
                    }}
                  >
                    <Image
                      source={require("../../assets/images/temp/close.png")}
                      style={{ resizeMode: "contain", width: 30, height: 30 }}
                    />
                  </TouchableOpacity>
                </View>

                <View style={styles.formWrappers}>
                  <View style={styles.helpsubjectWrapper}>
                    <Text style={styles.helpsubText}>
                      {appTexts.HELPPOPUP.Subject}
                    </Text>
                    <View style={{ marginTop: 0 }}>
                      <TextInput
                        style={styles.hinput}
                        value={subject}
                        autoCapitalize="none"
                        onChangeText={(val) => {
                          setSubject(val);
                        }}
                      />
                      <DividerLine color={"lightgray"} />
                    </View>
                  </View>
                  <View style={styles.helpmessageWrapper}>
                    <Text style={styles.helpsubText}>
                      {appTexts.HELPPOPUP.Message}
                    </Text>
                    <View style={{ marginTop: 15 }}>
                      <TextInput
                        style={styles.hinput}
                        autoCapitalize="none"
                        textAlignVertical={"top"}
                        textBreakStrategy={"highQuality"}
                        value={message}
                        onChangeText={(val) => {
                          setMessage(val);
                        }}
                      />
                      <DividerLine color={"lightgray"} />
                    </View>
                  </View>
                </View>
                <View style={styles.helpbuttoninsidewrapper}>
                  <TouchableOpacity
                    style={styles.helpbuttononview}
                    onPress={() => {
                      onChatPress();
                    }}
                  >
                    <LinearGradient
                      start={{ x: 0, y: 0 }}
                      end={{ x: 1, y: 2.45 }}
                      colors={["#ff8001", "#fbc203"]}
                      style={styles.hylpyButtonon}
                    >
                      <Text style={styles.helpyellowbuttonText}>
                        {appTexts.HELPPOPUP.Send}
                      </Text>
                    </LinearGradient>
                  </TouchableOpacity>
                </View>
              </KeyboardAwareScrollView>
            </View>
            <Toast ref={(ref) => Toast.setRef(ref)} />
          </Modal>
        </View>
      </View>
      <PopupModal
        popupNoClick={popupNoClick}
        popupYesClick={popupYesClick}
        popupIndex={popupIndex}
      />
    </View>
  );
};

ProfileView.propTypes = {
  isModalVisible: PropTypes.bool,
  isHelpModalVisible: PropTypes.bool,
  logoutButtonPress: PropTypes.func,
  privacyPolicyPress: PropTypes.func,
  termsAndConditionPress: PropTypes.func,
  toggleHelpModal: PropTypes.func,
  faqPress: PropTypes.func,
  toggleModal: PropTypes.func,
  popupIndex: PropTypes.number,
  popupYesClick: PropTypes.func,
  popupNoClick: PropTypes.func,
};

export default ProfileView;
