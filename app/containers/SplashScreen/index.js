import React, { Component } from "react";
import { View, Image } from "react-native";
import { styles } from "./styles";
import { connect } from 'react-redux'
import { bindActionCreators } from "redux";
class Splash extends Component {

	navigateToScreen = async () => {
		try {
			this.props.navigation.navigate('ChooseLanguageScreen');
			if (this.props.selectedLanguage) {
				if (this.props.isLogged === true) {
					this.props.navigation.navigate('TabNavigator', { screen: this.props.currentScreen });
				} else {
					this.props.navigation.navigate('AppIntrosliderloginScreen');
				}
			}else{
				if (this.props.isLogged === true) {
					this.props.navigation.navigate('TabNavigator', { screen: this.props.currentScreen });
				} else {
					this.props.navigation.navigate('ChooseLanguageScreen');
				}
				
			}
		} catch (e) {
			if (this.props.isLogged === true) {
				this.props.navigation.navigate('TabNavigator', { screen: this.props.currentScreen });
			} else {
				this.props.navigation.navigate('ChooseLanguageScreen');
			}
		}
	}

	componentDidMount() {
		setTimeout(() => {
			this.navigateToScreen();
		}, 2000);
	}

  render() {
    return (
      <View style={styles.screenMain}>
			<View style={styles.screenMainContainer}>
			 <Image source={require('./images/Driver.png')}style={styles.logo}/>
			</View>
		</View>
    );
  }
}
/**
 * Assigning the state to props
 * @param {*} state
 */
function mapStateToProps(state) {
	return {
		isLogged: state.loginReducer.isLogged,
		selectedLanguage: state.loginReducer.selectedLanguage,
		currentScreen: state.loginReducer.currentScreen

	}
}
/**
 * Dispatch action
 * @param {*} dispatch
 */
function mapDispatchToProps(dispatch) {
	return bindActionCreators({
		
	}, dispatch);
}
/**
 * Connecting the LoginScreen component to a Redux store
 */
export default connect(mapStateToProps, mapDispatchToProps)(Splash)