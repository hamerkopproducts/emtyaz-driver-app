import { StyleSheet } from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

export default class StyleSheetFactory {
  static getSheet(isRTL) {
    return StyleSheet.create({
      buttonon: {
        minWidth: 120,
        paddingLeft: 20,
        paddingRight: 20,
        height: 40,
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        shadowOpacity: 0.58,
        shadowRadius: 16.0,
        elevation: 5,
      },
      buttononview: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: hp('2.5%'),
      },
      buttonontext: {
        color: '#ffffff',
        //fontFamily: "Dosis-SemiBold",
        fontFamily: isRTL ? 'NotoKufiArabic-Bold' : 'Montserrat-Medium',
        fontSize: 16,
        // transform: [{ scaleX: isRTL ? -1 : 1 }],
      },
      buttononForReject: {
        minWidth: 120,
        paddingLeft: 10,
        paddingRight: 10,
        height: 40,
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        // shadowOpacity: 0.58,
        // shadowRadius: 16.00,
        // elevation:5,
        borderColor: '#B8B8B8',
        borderWidth: 1,
      },
      buttonontextReject: {
        color: '#000000',
        //fontFamily: "Dosis-SemiBold",
        fontFamily: isRTL ? 'NotoKufiArabic-Bold' : 'Montserrat-Medium',
        fontSize: 16,
        // transform: [{scaleX: isRTL ? -1 : 1}],
      },
    });
  }
}
