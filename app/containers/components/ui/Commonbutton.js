import { Image, Text, TouchableOpacity, I18nManager } from 'react-native';
import React, { Component } from 'react';

import LinearGradient from 'react-native-linear-gradient';

import StyleSheetFactory from './styles';

class Commonbutton extends Component {

  render() {
    const styles = StyleSheetFactory.getSheet(I18nManager.isRTL);
    return (
      <TouchableOpacity
        disabled={typeof this.props.disabled && this.props.disabled == true}
        {...this.props}
        style={[
          styles.buttononview,
          this.props.btnStyle && this.props.btnStyle
        ]}
        onPress={() => this.props.onPress()}>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 2.45 }}
          locations={[0, 0.9]}
          colors={
            this.props.colors ? this.props.colors : ['#bd2453', '#5f2366']
          }
          style={[
            styles.buttonon,
            { ...this.props.style },
            {
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            },
          ]}>
          <Text
            style={[
              styles.buttonontext,
              { color: typeof this.props.textColor != 'undefined' ? this.props.textColor : '#fff' },
              { ...this.props.size },
            ]}>
            {this.props.buttontext}
          </Text>
          {this.props.iconName && (
            <Image
              source={this.props.iconName}
              resizeMode="contain"
              style={{
                width: 15,
                height: 11,
                marginLeft: 10,
                transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }],
              }}
            />
          )}
        </LinearGradient>
      </TouchableOpacity>
    );
  }
}

export default (Commonbutton);
