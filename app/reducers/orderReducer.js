import * as ActionTypes from '../actions/types'

const defaultState = {
  deliveredOrders: null,
  returnedOrders: null,
  orderStatusUpdateResponse:null,
  orderStatusUpdateData:null,
  orderDetails:null,
  isLoading: false
};

export default function orderReducer(state = defaultState, action) {
  switch (action.type) {
    case ActionTypes.GET_COMPLETED_ORDER_LOADING:
      return Object.assign({}, state, {
        isLoading: true,
      });
    case ActionTypes.GET_COMPLETED_ORDER_ERROR:
      return Object.assign({}, state, {
        isLoading: false,
        error: action.error
      });
    case ActionTypes.GET_DELIVERED_ORDER_SUCCESS:
      console.log(action.responseData)
      return Object.assign({}, state, {
        isLoading: false,
        deliveredOrders: action.responseData
      });
    case ActionTypes.GET_RETURNED_ORDER_SUCCESS:
      return Object.assign({}, state, {
        isLoading: false,
        returnedOrders: action.responseData
      });
    case ActionTypes.UPDATE_ORDER_STATUS:
      return Object.assign({}, state, {
        isLoading: false,
        orderStatusUpdateResponse: action.responseData
      });
    case ActionTypes.UPDATE_ORDER_DETAILS_STATUS:
      return Object.assign({}, state, {
        isLoading: false,
        orderStatusUpdateData: action.responseData
      });
    case ActionTypes.UPDATE_ORDER_DETAILS:
      return Object.assign({}, state, {
        isLoading: false,
        orderDetails: action.responseData
      });
    default:
      return state;
  }
}
