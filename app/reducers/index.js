import { combineReducers } from 'redux';
import loginReducer from './loginReducer';
import homeReducer from './homeReducer';
import orderReducer from './orderReducer';

const rootReducer = combineReducers({
    loginReducer,
    homeReducer,
    orderReducer
});

export default rootReducer;
