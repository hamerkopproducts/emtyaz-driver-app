import * as ActionTypes from '../actions/types'

const defaultLoginState = {
    userData: null,
    isLogged: false,
    resetNavigation: undefined,
    isLoading: false,
    error: null,
    getOtpLoading: false,
    otpAPIReponse: null,
    selectedLanguage:false,
    isLoggedOut:false,
    profileUpdate:null,
    chatData:null,
    currentScreen: 'Home'
};

export default function loginReducer(state = defaultLoginState, action) {
    switch (action.type) {
        case ActionTypes.LOGIN_SERVICE_LOADING:
            return Object.assign({}, state, {
                isLoading: true,
            });
        case ActionTypes.OTP_SERVICE_LOADING:
            return Object.assign({}, state, {
                getOtpLoading: true
            });
        case ActionTypes.LOGIN_SERVICE_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                error: action.error
            });
        case ActionTypes.OTP_SERVICE_ERROR:
            return Object.assign({}, state, {
                getOtpLoading: false,
                error: action.error
            });
        case ActionTypes.RESET_ERROR:
            return Object.assign({}, state, {
                getOtpLoading: false,
                isLoading: false,
                error:null,
            });
        case ActionTypes.OTP_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                getOtpLoading: false,
                otpAPIReponse: action.responseData
            });
        case ActionTypes.LOGIN_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                getOtpLoading: false,
                userData: action.responseData,
                isLogged: true,
                isLoggedOut: false
            });
            case ActionTypes.CHAT_SUCCESS:
                return Object.assign({}, state, {
                    isLoading: false,
                    chatData: action.responseData,
                
                });
                case ActionTypes.CHAT_SUPPORT_STATUS:
                    return Object.assign({}, state, {
                        isLoading: false,
                        chatData: null,
                    
                    });
        case ActionTypes.PROFILE_UPDATE_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                profileUpdate: action.responseData,
                userData: action.userData
            });
        case ActionTypes.UPDATE_PROFILE_UPDATE_STATUS:
            return Object.assign({}, state, {
                isLoading: false,
                profileUpdate: null
            });
        case ActionTypes.LANGUAGE_SELECTION_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                selectedLanguage: action.responseData
            });
        case ActionTypes.UPDATE_LOGOUT_STATUS:
            return Object.assign({}, state, {
                isLoading: false,
                isLoggedOut: false
            });
        case ActionTypes.SET_CURRENT_SCREEN:
            return Object.assign({}, state, {
                currentScreen: action.responseData
            });
        case ActionTypes.LOGOUT_SUCCESS:
            return {
                ...state,
                userData: null,
                currentScreen: 'Home',
                otpAPIReponse:null,
                isLogged: false,
                isLoading: false,
                getOtpLoading:false,
                error: null,
                success: undefined,
                resetNavigation: action.resetNavigation,
                isLoggedOut: true
            };
        default:
            return state;
    }
}
