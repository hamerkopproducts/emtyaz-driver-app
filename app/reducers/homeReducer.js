import * as ActionTypes from '../actions/types'

const defaultState = {
  ordersToDeliver: null,
  ordersToCollect: null,
  ordersToReturn: null,
  isLoading: false,
  ordersToDeliverAPIComplete: false,
  ordersToCollectAPIComplete: false,
  ordersToReturnAPIComplete: false
};

export default function homeReducer(state = defaultState, action) {
  switch (action.type) {
    case ActionTypes.GET_ORDER_LOADING:
      return Object.assign({}, state, {
        isLoading: true,
      });
    case ActionTypes.GET_ORDER_ERROR:
      return Object.assign({}, state, {
        isLoading: false,
        error: action.error
      });
    case ActionTypes.GET_ORDER_TO_DELIVER_SUCCESS:
      console.log(action.responseData)
      return Object.assign({}, state, {
        isLoading: false,
        ordersToDeliver: action.responseData,
        ordersToDeliverAPIComplete:true
      });
    case ActionTypes.GET_ORDER_TO_COLLECT_SUCCESS:
      return Object.assign({}, state, {
        isLoading: false,
        ordersToCollect: action.responseData,
        ordersToCollectAPIComplete:true
      });
    case ActionTypes.GET_ORDER_TO_RETURN_SUCCESS:
      return Object.assign({}, state, {
        isLoading: false,
        ordersToReturn: action.responseData,
        ordersToReturnAPIComplete:true
      });
    case ActionTypes.RESET_ORDER_TO_DELIVER_STATUS:
      return Object.assign({}, state, {
        ordersToDeliverAPIComplete: false
      });
    case ActionTypes.RESET_ORDER_TO_COLLECT_STATUS:
      return Object.assign({}, state, {
        ordersToCollectAPIComplete: false
      });
    case ActionTypes.RESET_ORDER_TO_RETURN_STATUS:
      return Object.assign({}, state, {
        ordersToReturnAPIComplete: false
      });
    default:
      return state;
  }
}
