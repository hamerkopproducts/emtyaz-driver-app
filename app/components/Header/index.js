import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { StatusBar, View, Text, Image, TouchableOpacity, I18nManager } from 'react-native';
import globals from "../../lib/globals"
import RNRestart from 'react-native-restart';
import { images, styles } from "./styles";

const Header = (props) => {
  const {
    isBackButtonRequired,
    isLanguageButtonRequired,
    isLogoRequired,
    headerTitle,
    onRightButtonPress,
    customHeaderStyle,
    onBackButtonClick,
    isNotificationRequired
  } = props;

  let headerStyle = styles.headerContainer;
  if (customHeaderStyle)
    headerStyle = customHeaderStyle;
  const switchlang = async() => {
    if (I18nManager.isRTL) {
      I18nManager.forceRTL(false);
    } else {
      I18nManager.forceRTL(true);
    }
    setTimeout(() => {
      RNRestart.Restart();
    }, 500);
  }
  return (
    <View style={[headerStyle]}>
      <StatusBar
        backgroundColor={globals.COLOR.headerColor}
        barStyle="dark-content"
      />

      {isBackButtonRequired ? (
        <TouchableOpacity style={styles.leftIconContainer}
          onPress={() => { onBackButtonClick()}}>
          <Image resizeMode="contain" source={images.backButtonImage}
            style={{ alignItems: "stretch",transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }] }} />
        </TouchableOpacity>
      ) : null}
      {isLanguageButtonRequired ? (
        <TouchableOpacity style={styles.leftIconContainer}
          onPress={() => { switchlang()}}>
          <View style={styles.langContainer} >
            <Image resizeMode="contain" source={I18nManager.isRTL ? images.languageFlagImageEN : images.languageFlagImageAR  }
            style={styles.langIcon} />
            <View style={styles.langTextContainer} >
          <Text style={styles.langText}>{I18nManager.isRTL? 'EN':'AR'}</Text>
            </View>
          </View>
        </TouchableOpacity>
      ) : null}
      <View style={styles.headerTitleContainer}>
        {isLogoRequired ? (<Image source={images.logo} resizeMode="contain" style={styles.headerLogo} />)
          :
          (<Text style={styles.headerTitleText}>{headerTitle}</Text>)
        }
      </View>

      <View style={styles.rightIconContainer}>
        {isNotificationRequired ? 
          (<TouchableOpacity style={styles.rightIconContainer} onPress={onRightButtonPress}>
          <Image source={images.bellIcon} style={styles.rightIcon} />
          </TouchableOpacity>
          ):null
        }
      </View>

    </View>
  );
};

Header.propTypes = {
  isBackButtonRequired: PropTypes.bool,
  isLanguageButtonRequired: PropTypes.bool,
  isRightButtonRequired: PropTypes.bool,
  isLogoRequired: PropTypes.bool,
  headerTitle: PropTypes.string,
  isFilterRequired: PropTypes.bool,
  filterTitle: PropTypes.string,
  onRightButtonPress: PropTypes.func,
  onFilterPress: PropTypes.func,
  customHeaderStyle: PropTypes.object,
  onBackButtonClick: PropTypes.func,
  isNotificationRequired:PropTypes.bool

};

export default Header;
