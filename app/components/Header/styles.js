import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
const images = {
  backButtonImage:require("../../assets/images/header/back.png"),
  languageFlagImageEN: require("../../assets/images/header/flag_en.png"),
  languageFlagImageAR: require("../../assets/images/header/flag.png"),
  logo: require("../../assets/images/header/logo.png"),
  bellIcon: require("../../assets/images/header/Bell.png"),
};

const styles = StyleSheet.create({
  headerContainer: {
    flex: 1,
    height: globals.INTEGER.headerHeight,
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: globals.COLOR.headerColor
  },
  leftIconContainer:{
    marginTop:hp("1%"),
    paddingLeft: globals.SCREEN_SIZE.width <=320 ? globals.MARGIN.marginEight : globals.MARGIN.marginFifteen,
    paddingRight:globals.SCREEN_SIZE.width <=320 ? globals.MARGIN.marginEight : globals.MARGIN.marginFifteen,
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    height : "100%",
    left:0
  },
    headerTitleContainer: {
    flex: 1,
    paddingLeft: 10,
    paddingRight: 10,
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    height : "100%",
    alignSelf:'center'
  },
  headerTitleText: {
    textAlign: "center",
    color: globals.COLOR.black,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiarabicBold: globals.FONTS.avenirHeavy,
    //fontSize: globals.SCREEN_SIZE.width * 0.04,
    fontSize:16
  },
 langContainer:{
   justifyContent: "center",
   alignItems: "center",
   flexDirection:'row'
 },
  langIcon:{
    height: 20,
    width: 20,
  },
  langTextContainer:{
    justifyContent: "center",
    alignItems: "center"
  },
  langText: {
    marginLeft:5,
    textAlign: "center",
    color: 'black',
    fontFamily: globals.FONTS.avenirMedium,
    fontSize: 13
  },
  headerLogo:{
    height: 50,
    width:150
  },
  rightIconContainer:{
    marginTop:hp("1%"),
    flex: 1,
    flexDirection: 'row',
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    height : "100%",
    right: 0,
    paddingLeft:10,
    paddingRight:10
  },
  rightIcon: {
    marginRight: globals.MARGIN.marginFifteen,
      height: 20,
      width: 17,
      resizeMode:'cover'
  }
});

export { images, styles  };
