import PropTypes from 'prop-types';
import React from 'react';
import { Modal, View, Text, TouchableOpacity,Image } from 'react-native';
import LinearGradient from "react-native-linear-gradient";
import { images, styles } from "./styles";
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";

import RoundButton from "../RoundButton";
const PopupModal = (props) => {

  const {
    popupIndex,
      popupNoClick,
      popupYesClick,
    selectedOrderId,
    amountForTheItem
      } = props;

  
  return (
    <Modal
            animationType="fade"
            transparent={true}
            onRequestClose={() => null}
      visible={popupIndex === -1 ? false : true}>
            <View style={styles.popupContainer}>
              <View style={styles.popupBackground} />
        <View style={styles.popupContentContainer}>
          <View style={styles.popupDialogContainer}>
            <View style={styles.popupContent}>
              <View style={styles.popupIconContainer}>
                <Image source={popupIndex === 0 ? images.modal1Icon : popupIndex === 1 ? images.modal2Icon : popupIndex === 2 ? images.modal5Icon : popupIndex === 3 ? images.modal3Icon : popupIndex === 4 ? images.modal4Icon : popupIndex === 7 ? images.modal7Icon : images.modal6Icon} style={styles.modalIcon}/>
              </View>
              <View style={styles.popupTextContainer}>
                <Text style={styles.popupDescription}>{popupIndex === 0 ? appTexts.COMMON_POPUP.modalText1 : popupIndex === 1 ? appTexts.COMMON_POPUP.modalText5 : popupIndex === 2 ? appTexts.COMMON_POPUP.modalText6 : popupIndex === 3 ? appTexts.COMMON_POPUP.modalText2 : popupIndex === 4 ? appTexts.COMMON_POPUP.modalText3 : popupIndex === 5 ? appTexts.COMMON_POPUP.modalText4 : popupIndex === 7 ? appTexts.COMMON_POPUP.logoutModal : appTexts.COMMON_POPUP.modalText7}</Text>
                  {popupIndex === 4 ?
                    <Text style={styles.completedText}>
                      {appTexts.COMMON_POPUP.modal3SubText+ ' '}
                      <Text style={styles.amountText}>{selectedOrderId}</Text>
                  </Text> : popupIndex === 3 ? <Text style={styles.amountText}>{appTexts.STRING.currency + ' ' + amountForTheItem}</Text>: null}
              </View>
              {popupIndex === 4 ? <View style={styles.popupButtonContainer}>
                <RoundButton buttonPosition={'center'} buttonText={appTexts.COMMON_POPUP.Continue} buttonClick={popupYesClick} />
              </View>:<View style={styles.popupButtonsContainer}> 
                  <RoundButton buttonPosition={'left'} buttonText={popupIndex === 5 || popupIndex === 6 ? appTexts.COMMON_POPUP.No : appTexts.COMMON_POPUP.No} buttonClick={popupNoClick}/>
                  <RoundButton buttonPosition={'right'} buttonText={popupIndex === 5 || popupIndex === 6 ? appTexts.COMMON_POPUP.Yes : appTexts.COMMON_POPUP.Yes} buttonClick={popupYesClick}/>
            </View>
              } 
             </View>
          </View>
              </View>
            </View>
  </Modal>
  );
};

PopupModal.propTypes = {
  popupIndex: PropTypes.number,
  popupNoClick: PropTypes.func,
  popupYesClick: PropTypes.func,
  selectedOrderId:PropTypes.string,
  amountForTheItem: PropTypes.string
};

export default PopupModal;
