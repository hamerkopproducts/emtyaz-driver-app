import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"

const images = {
  modal1Icon: require("../../assets/images/modal/modal1.png"),
  modal2Icon: require("../../assets/images/modal/modal2.png"),
  modal3Icon: require("../../assets/images/modal/modal3.png"),
  modal4Icon: require("../../assets/images/modal/modal4.png"),
  modal5Icon: require("../../assets/images/modal/modal5.png"),
  modal6Icon: require("../../assets/images/modal/modal6.png"),
  modal7Icon: require("../../assets/images/modal/modal7.png")
};

const styles = StyleSheet.create({
	popupContainer:{
      flex:1,
      flexDirection: 'column',
      backgroundColor: globals.COLOR.transparent
    },
    popupBackground:{
    	flex: 1,
    	flexDirection: 'column',
    	backgroundColor: globals.COLOR.black,
    	opacity:0.7,
    	zIndex: 1
    },
    popupContentContainer:{
    	top:0,
    	position:'absolute',
    	flex: 1,
    	width: '100%',
    	height: '100%',
    	flexDirection: 'column',
    	alignItems: 'center',
    	justifyContent: "center",
      zIndex: 3
    },
    popupDialogContainer:{
        zIndex:5,
        width:"88%",
        borderRadius:10,
      backgroundColor: globals.COLOR.white,
      alignItems: 'center',
      justifyContent: "center"
    },
  popupContent:{
    marginBottom:40,
    marginTop:40,
    width:"90%",
    alignItems: 'center',
    justifyContent: "center",
  },
  popupIconContainer: {
    alignItems: 'center',
    justifyContent: "center",
  },
  popupTextContainer: {
    alignItems: 'center',
    justifyContent: "center"
  },
  modalIcon:{
    width: 180,
    height:180
  },
    popupDescription:{
        marginTop: 14,
        textAlign: 'center',
        fontFamily:  I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.avenirLight,
        fontSize:14
       
    },
  amountTextContainer:{
    alignItems: 'flex-start',
    justifyContent: "flex-start",
    flexDirection:'row',
    paddingLeft: 20,
    paddingRight: 20
  },
  completedText:{
    marginTop: 14,
    textAlign: 'center',
    color: globals.COLOR.lightTextColor,
    fontFamily: globals.FONTS.avenirLight,
    fontSize: 14
  },
  amountText: {
    marginTop: 10,
    textAlign: 'center',
    fontFamily:  I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.avenirHeavy,
    fontSize: 14,
    color: globals.COLOR.textColor,
  },
  popupButtonsContainer:{
    marginTop: 24,
    marginBottom: 10,
    width: '100%',
    height: 42,
    flexDirection: 'row'
  },
    popupButtonContainer:{
      marginTop: 24,
      marginBottom: 10,
      alignItems: 'center',
      justifyContent: "center"
    },
    popupButton:{
        flex: 1,
        alignItems: 'center',
        justifyContent: "center"
    },
});

export { images, styles  };
