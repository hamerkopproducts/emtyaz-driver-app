import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"

const images = {
  calendarIcon: require("../../assets/images/temp/calendern.png"),
  arrowIcon: require("../../assets/images/temp/calenderarrow.png"),
  closeIcon: require("../../assets/images/Icons/close-red.png")
};

const styles = StyleSheet.create({
  container: {
    width: globals.SCREEN_SIZE.width,
    height: globals.INTEGER.topTabHieght,
    paddingLeft: globals.INTEGER.leftRightMargin,
    paddingRight: globals.INTEGER.leftRightMargin,
    flexDirection:'row',
    alignItems: 'center',
    //backgroundColor:'red',
    marginTop:5
  },
  leftTabView:{
    width: '40%',
    height: 30,
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  middleTabView: {
    width: '30%',
    height: 30,
    justifyContent: 'center',
    alignItems: 'center'
  },
  rightTabView: {
    width: '30%',
    height: 30,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  leftHeadingLabelView:{
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  nameLabel:{
    top:0,
    color: globals.COLOR.lightTextColor,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic :globals.FONTS.avenirHeavy,
    fontSize:12
  },
  countLabel: {
    top: 0,
    color: globals.COLOR.textColor,
    fontFamily: globals.FONTS.avenirHeavy,
    fontSize: 12
  },
  linkText:{
    top: 0,
    color: globals.COLOR.linkTextColor,
    fontFamily: globals.FONTS.avenirLight,
    fontSize: 12
  },
  leftIcon:{
    width:20,
    height:20
  },
  underLineStyle:{
    marginTop:5,
    width:'75%',
    height: 4,
    borderRadius:2,
    backgroundColor: globals.COLOR.tabUnderLineColor
  },
  transparentUnderLineStyle:{
    marginTop: 5,
    width: '75%',
    height: 4,
    borderRadius: 2,
    backgroundColor: globals.COLOR.transparent
  },
  underLineWidth: {
    width: '75%'
  },
  calenderFilterContainer:{
    marginLeft: '4%',
    width: '36%',
  },
  calenderFilterView:{
    width: '100%',
    height:30,
    backgroundColor: globals.COLOR.headerColor,
    shadowColor: globals.COLOR.borderColor,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.5,
    shadowRadius: 0.5,
    elevation: 1,
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection:'row'
  },
  calenderIcon:{
    position: 'absolute',
    left: 6,
    width:20,
    height:20
  },
  dateText: {
    textAlign:'left',
    color: globals.COLOR.textColor,
    fontFamily: globals.FONTS.avenirMedium,
    fontSize: 10,
    left:4
  },
  arrowIcon: {
    position:'absolute',
    right: 25,
    width: 10,
    height: 10
  },
  closeIcon: {
    position:'absolute',
    right: 5,
    width: 20,
    height: 20
  },
  closeIconImg: {
    width: 20,
    height: 20
  }
});

export { images, styles };
