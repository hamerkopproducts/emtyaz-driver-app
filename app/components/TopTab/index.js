import React, { Component } from "react";
import PropTypes from "prop-types";
import { View, TouchableOpacity, Image, Text } from "react-native";
import moment from "moment";
import { images, styles } from "./styles";

const TopTab = (props) => {
  const {
    tabIndex,
    firstTabText,
    secondTabText,
    thirdTabText,
    firstTabCount,
    secondTabCount,
    thirdTabCount,
    onTabChange,
    isCalenderRequired,
    onCalendarFilterPress,
    filterDate,
    clearFilter
  } = props;

  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={[styles.leftTabView, isCalenderRequired && { width: "30%" }]}
        onPress={() => {
          onTabChange(0);
        }}
      >
        <View style={styles.leftHeadingLabelView}>
          <Text style={styles.nameLabel}>{firstTabText}</Text>
          <Text style={styles.countLabel}>{" (" + firstTabCount + ")"}</Text>
        </View>
        <View
          style={
            tabIndex === 0
              ? styles.underLineStyle
              : styles.transparentUnderLineStyle
          }
        />
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.middleTabView}
        onPress={() => {
          onTabChange(1);
        }}
      >
        <View style={styles.leftHeadingLabelView}>
          <Text style={styles.nameLabel}>{secondTabText}</Text>
          <Text style={styles.countLabel}>{" (" + secondTabCount + ")"}</Text>
        </View>
        <View
          style={[
            tabIndex === 1
              ? styles.underLineStyle
              : styles.transparentUnderLineStyle,
            styles.underLineWidth,
          ]}
        />
      </TouchableOpacity>
      {isCalenderRequired ? (
        <TouchableOpacity
          style={styles.calenderFilterContainer}
          onPress={() => {
            onCalendarFilterPress();
          }}
        >
          <View style={styles.calenderFilterView}>
            <Image style={styles.calenderIcon} source={images.calendarIcon} />
            {filterDate != '' &&
            <Text style={styles.dateText}>
              {moment(filterDate).format("DD-MM-YYYY")}
            </Text>
            }
            {filterDate == '' &&
            <Text style={styles.dateText}>Select Date</Text>
            }
            <Image style={styles.arrowIcon} source={images.arrowIcon} />
            <TouchableOpacity style={styles.closeIcon} onPress={() =>  clearFilter()}>
              <Image style={styles.closeIconImg} source={images.closeIcon} />
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
      ) : (
        <TouchableOpacity
          style={styles.rightTabView}
          onPress={() => {
            onTabChange(2);
          }}
        >
          <View style={styles.leftHeadingLabelView}>
            <Text style={styles.nameLabel}>{thirdTabText}</Text>
            <Text style={styles.countLabel}>{" (" + thirdTabCount + ")"}</Text>
          </View>
          <View
            style={[
              tabIndex === 2
                ? styles.underLineStyle
                : styles.transparentUnderLineStyle,
              styles.underLineWidth,
            ]}
          />
        </TouchableOpacity>
      )}
    </View>
  );
};
TopTab.propTypes = {
  tabIndex: PropTypes.number,
  firstTabText: PropTypes.string,
  secondTabText: PropTypes.string,
  thirdTabText: PropTypes.string,
  firstTabCount: PropTypes.number,
  secondTabCount: PropTypes.number,
  thirdTabCount: PropTypes.number,
  onTabChange: PropTypes.func,
  isCalenderRequired: PropTypes.bool,
  onCalendarFilterPress: PropTypes.func,
  filterDate: PropTypes.any,
};

export default TopTab;
