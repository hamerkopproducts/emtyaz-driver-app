import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"

const images = {
  itemIcon: require("../../assets/images/temp/main.png")
};

const styles = StyleSheet.create({
  rowContainer: {
    width: '100%',
    backgroundColor: globals.COLOR.headerColor,
    justifyContent: 'center',
    alignItems: 'center'
  },
  rowItemContainer: {
    width: '100%',
    backgroundColor: globals.COLOR.headerColor,
    justifyContent: 'center',
    alignItems: 'flex-start',
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom:10
  },
  orderImageContainer: {
    paddingTop: 10,
    width: '25%',
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  itemImage:{
    marginLeft:0,
    width:70,
    height: 70,
    borderRadius:10,
  },
  orderDataContainer: {
    paddingTop:10,
    width: '75%',
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  labelContainer: {
    paddingBottom:5,
    width:"100%",
   flexDirection:'row'
  },
  middleLabelContainer:{
    paddingBottom: 15,
    width: "100%",
    flexDirection: 'row',
    alignItems:'center'
  },
  mediumLabel: {
    textAlign: 'left',
    color: globals.COLOR.textColor,
    fontFamily: globals.FONTS.avenirMedium,
    fontSize: 12
  },
  lightLabel: {
    marginLeft: 20,
    textAlign: 'left',
    color: globals.COLOR.textColor,
    fontFamily: globals.FONTS.avenirLight,
    fontSize: 12
  },
  leftLabel: {
    textAlign: 'left',
    color: globals.COLOR.textColor,
    fontFamily:  I18nManager.isRTL ? globals.FONTS.notokufiarabicBold : globals.FONTS.avenirHeavy,
    fontSize: 12
  },
  middleLabel: {
    marginLeft:20,
    textAlign: 'left',
    color: globals.COLOR.textColor,
    fontFamily: globals.FONTS.avenirHeavy,
    fontSize: 12
  },
  rightLabel:{
    position:'absolute',
    right:0,
    textAlign: 'left',
    color: globals.COLOR.textColor,
    fontFamily:  I18nManager.isRTL ? globals.FONTS.notokufiarabicBold : globals.FONTS.avenirHeavy,
    fontSize: 12
  }
});

export { images, styles };
