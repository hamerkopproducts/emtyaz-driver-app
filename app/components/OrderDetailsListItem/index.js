import React, { Component } from "react";
import PropTypes from "prop-types";
import { View, I18nManager, Image, Text } from "react-native";
import { images, styles } from "./styles";
import appTexts from "../../lib/appTexts";
import DividerLine from "../DividerLine";

const OrderDetailsListItem = (props) => {
  const { itemData } = props;

  return (
    <View style={styles.rowContainer}>
      <View style={styles.rowItemContainer}>
        <View style={styles.orderImageContainer}>
          <Image source={{ uri: itemData.image }} style={styles.itemImage} />
        </View>
        <View style={styles.orderDataContainer}>
          <View style={styles.labelContainer}>
            <Text style={styles.mediumLabel}>{itemData.category}</Text>
          </View>
          <View style={styles.middleLabelContainer}>
            <Text style={styles.mediumLabel}>{itemData.product_name}</Text>
            <Text style={styles.lightLabel}>
              {I18nManager.isRTL
                ? itemData.size && itemData.size.ar
                  ? itemData.size.ar
                  : ""
                : itemData.size && itemData.size.en
                ? itemData.size.en
                : ""}
            </Text>
          </View>
          <View style={styles.labelContainer}>
            <Text style={styles.leftLabel}>
              {appTexts.STRING.currency + " " + itemData.product_subtotal}
            </Text>
            {/* <Text style={styles.middleLabel}>{"X " + itemData.product_count}</Text> */}
            <Text style={styles.middleLabel}>
              {I18nManager.isRTL
                ? itemData.product_count + " X"
                : "X " + itemData.product_count}
            </Text>
            <Text style={styles.rightLabel}>
              {appTexts.STRING.currency + " " + itemData.product_grandTotal}
            </Text>
          </View>
        </View>
      </View>
      <DividerLine />
    </View>
  );
};
OrderDetailsListItem.propTypes = {
  itemData: PropTypes.object,
};

export default OrderDetailsListItem;
