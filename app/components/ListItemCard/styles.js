import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"

const images = {
  locationIcon: require("../../assets/images/listCard/locationIcon.png"),
  checkBox: require("../../assets/images/listCard/check-box.png"),
  checkBoxSelected: require("../../assets/images/listCard/check-box-active.png"),
};

const styles = StyleSheet.create({
  rowContainer: {
    marginTop:10,
    width: '99%',
    marginLeft:'.5%',
    backgroundColor: globals.COLOR.headerColor,
    // shadowColor: globals.COLOR.borderColor,
    // shadowOffset: {
    //   width: 0,
    //   height: 0,
    // },
    // shadowOpacity: 0.5,
    // shadowRadius: 0.5,
    // elevation: 1,
    // borderRadius:10,
    borderWidth: 0.5,
    borderRadius: 10,
    borderColor: '#ddd',
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:10
  },
  orderIdContainer:{
    marginTop:15,
    width:'100%',
    height:30,
    justifyContent:'flex-start',
    alignItems: 'center',
    paddingLeft: 13,
    paddingRight: 13,
    flexDirection: 'row'
  },
  addressContainer: {
    width: '100%',
    padding: 15,
    justifyContent: 'center',
    alignItems: 'flex-start',
    flexDirection:'row'
  },
  buttonContainer: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 15,
    paddingRight: 15,
    borderBottomLeftRadius:20,
    borderBottomRightRadius:20
  },
  buttonView:{
    width: '92%',
    borderColor: globals.COLOR.borderColor,
    borderTopWidth: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  checkBoxContainer:{
    width:'15%',
    height:'100%',
    alignItems:'flex-end',
    justifyContent:'center'
  },
  scheduleContainer: {
    width: '50%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  locationContainer: {
    width: '50%',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginLeft:10
  },
  locationIconContainer: {
    width: '20%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  locationLabelContainer:{
    marginLeft:10,
    width: '70%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  orderIdLabelContainer:{
    width: '85%',
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  boldLabel:{
    textAlign:'left',
    color: globals.COLOR.textColor,
    fontFamily: globals.FONTS.avenirHeavy,
    fontSize:14
  },
  mediumLabel: {
    width: '100%',
    textAlign: 'left',
    color: globals.COLOR.textColor,
    fontFamily: globals.FONTS.avenirMedium,
    fontSize: 12
  },
  lightLabel: {
    width: '100%',
    textAlign: 'left',
    paddingBottom: 5,
    color: globals.COLOR.lightTextColor,
    fontFamily:  I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.avenirLight,
    fontSize: 12
  },
  buttonLabel: {
    width: '100%',
    textAlign: 'center',
    color: globals.COLOR.lightTextColor,
    fontFamily: globals.FONTS.avenirLight,
    fontSize: 16
  },
  locationIcon:{
    width:30,
    height:30
  },
  labelColorOrange:{
    color: globals.COLOR.tabUnderLineColor
  },
  labelColorGreen: {
    color:globals.COLOR.textColorGreen
  },
  buttonContainer: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonTextStyle: {
    color: globals.COLOR.textColorGreen,
    fontFamily: globals.FONTS.avenirMedium,
    fontSize: 12,
    textAlign: 'center'
  }
});

export { images, styles };
