import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Image, Text } from 'react-native';
import { images, styles } from "./styles";
import appTexts from "../../lib/appTexts";
import globals from "../../lib/globals";
import LinkButton from "../LinkButton";
const ListItemCard = (props) => {
  const {
    itemData,
    tabIndex,
    itemOnPress,
    listButtonClick,
    checkBoxClick,
    isBottomButtonRequired
  } = props;
  return (
    <TouchableOpacity style={styles.rowContainer} onPress={() => { itemOnPress(itemData)}}>
      <View style={styles.orderIdContainer}>
        <View style={styles.orderIdLabelContainer}>
          <Text style={styles.boldLabel}>{itemData.order_id}</Text>
        </View>
        {tabIndex !== 0 && isBottomButtonRequired && <TouchableOpacity style={styles.checkBoxContainer} onPress={() => { checkBoxClick(itemData) }}>
          <Image style={styles.checkBox} source={itemData.isSelected ? images.checkBoxSelected:images.checkBox} />
        </TouchableOpacity>}
      </View>
      <View style={styles.addressContainer}>
        <View style={styles.scheduleContainer}>
          <Text style={styles.lightLabel}>{appTexts.STRING.deliverySchedule}</Text>
          {/* <Text style={styles.mediumLabel}>{itemData.delivery_schedule_date + ', ' + itemData.schedule_time_from + '-' + itemData.schedule_time_to}</Text> */}
              <Text style={styles.mediumLabel}>{itemData.delivery_schedule_date + ','}{itemData.time_slot}</Text>
        </View>
        <View style={styles.locationContainer}>
          {isBottomButtonRequired && <View style={styles.locationIconContainer}>
            <Image source={images.locationIcon} style={styles.locationIcon} />
          </View>}
          <View style={styles.locationLabelContainer}>
            <Text style={styles.lightLabel}>{appTexts.STRING.deliveryLocation}</Text>
            <Text style={styles.mediumLabel}>{itemData.delivery_loc}</Text>
          </View>
        </View>
      </View>
      {isBottomButtonRequired &&
      <View style={styles.buttonContainer}>
        <View style={styles.buttonView}>
          <LinkButton 
            buttonText={
              tabIndex === 0 ? appTexts.STRING.markAsDelivered : (tabIndex === 1 ? appTexts.STRING.markAsCollected : appTexts.STRING.markAsReturned)} 
              linkColor={tabIndex === 0 ? globals.COLOR.textColorGreen : (tabIndex === 1 ? globals.COLOR.tabUnderLineColor : globals.COLOR.redTextColor)} 
              buttonClick={listButtonClick} 
              orderId={itemData.id} 
              amount={itemData.grant_total ? itemData.grant_total:''}
            />
        </View>
      </View>}

    </TouchableOpacity>
  );
};
ListItemCard.propTypes = {
  itemData: PropTypes.object,
  itemOnPress: PropTypes.func,
  listButtonClick: PropTypes.func,
  checkBoxClick: PropTypes.func,
  tabIndex: PropTypes.number,
  isBottomButtonRequired:PropTypes.bool
};

export default ListItemCard;