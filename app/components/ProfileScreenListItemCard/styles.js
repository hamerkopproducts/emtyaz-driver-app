import { StyleSheet } from "react-native";
import globals from "../../lib/globals"

const images = {
  locationIcon: require("../../assets/images/listCard/locationIcon.png"),
  checkBox: require("../../assets/images/listCard/check-box.png"),
  checkBoxSelected: require("../../assets/images/listCard/check-box-active.png"),
};

const styles = StyleSheet.create({
  rowContainer: {
    marginTop:20,
    marginBottom: 2,
    width: '99%',
    marginLeft:'.5%',
    backgroundColor: globals.COLOR.headerColor,
    shadowColor: globals.COLOR.borderColor,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
    borderRadius:20,
    justifyContent: 'center',
    alignItems: 'center'
  },
  orderIdContainer:{
    width:'100%',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 15,
    paddingBottom:5,
    flexDirection: 'row'
  },
  addressContainer: {
    width: '100%',
    padding: 15,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection:'row'
  },
  buttonContainer: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    borderColor: globals.COLOR.borderColor,
    borderTopWidth:1,
    borderBottomLeftRadius:20,
    borderBottomRightRadius:20
  },
  checkBoxContainer:{
    width:'15%',
    height:40,
    alignItems:'flex-end',
    justifyContent:'center'
  },
  scheduleContainer: {
    width: '50%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  locationContainer: {
    width: '50%',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  locationIconContainer: {
    width: '20%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  locationLabelContainer:{
    marginLeft:15,
    width: '70%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  rejectContainer: {
    width: '50%',
    padding: 15,
    justifyContent: 'center',
    alignItems: 'center',
    borderRightWidth:0.5,
    borderColor: globals.COLOR.borderColor
  },
  aceeptContainer: {
    width: '50%',
    padding: 15,
    justifyContent: 'center',
    alignItems: 'center',
    borderLeftWidth: 0.5,
    borderColor: globals.COLOR.borderColor
  },
  boldLabel:{
    width: '85%',
    textAlign:'left',
    color: globals.COLOR.textColor,
    fontFamily: globals.FONTS.avenirHeavy,
    fontSize:16
  },
  mediumLabel: {
    width: '100%',
    textAlign: 'left',
    paddingBottom:10,
    color: globals.COLOR.lightTextColor,
    fontFamily: globals.FONTS.avenirLight,
    fontSize: 12
  },
  lightLabel: {
    width: '100%',
    textAlign: 'left',
    color: globals.COLOR.textColor,
    fontFamily: globals.FONTS.avenirLight,
    fontSize: 12
  },
  buttonLabel: {
    width: '100%',
    textAlign: 'center',
    color: globals.COLOR.lightTextColor,
    fontFamily: globals.FONTS.avenirLight,
    fontSize: 16
  },
  locationIcon:{
    width:40,
    height:40
  },
  labelColorOrange:{
    color: globals.COLOR.tabUnderLineColor
  },
  labelColorGreen: {
    color:globals.COLOR.textColorGreen
  }
});

export { images, styles };
