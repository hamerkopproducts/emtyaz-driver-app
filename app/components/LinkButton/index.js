import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity,View, Text } from 'react-native';
import { styles } from "./styles";


const LinkButton = (props) => {
  const {
    isUnderLine,
    linkColor,
    buttonText,
    buttonClick,
    orderId,
    amount
  } = props;

  
  return (
    <TouchableOpacity style={styles.buttonContainer} onPress={() => { orderId && orderId !== null ? buttonClick(orderId, amount) : buttonClick()}}>
      <Text style={[styles.buttonTextStyle, { color: linkColor }, isUnderLine && { textDecorationLine: 'underline',fontSize:12 } ]}>{buttonText}</Text>
    </TouchableOpacity>
  );
};
LinkButton.propTypes = {
  isUnderLine: PropTypes.bool,
  linkColor: PropTypes.string,
  buttonText: PropTypes.string,
  orderId: PropTypes.number,
  amount: PropTypes.string,
  buttonClick: PropTypes.func,
};

export default LinkButton;