import { StyleSheet } from "react-native";
import globals from "../../lib/globals"
const styles = StyleSheet.create({
  dividerStyle:{
    width:'100%',
    height:0.6,
    backgroundColor: globals.COLOR.borderColor,
  }
});

export { styles  };
