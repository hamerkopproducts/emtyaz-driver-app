import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { View } from 'react-native';
import { styles } from "./styles";

const DividerLine = (props) => {
	const {
		color
	} = props;

  return (
	  <View style={[styles.dividerStyle, color && { color: color}]}/>
  );
};

DividerLine.propTypes = {
	color: PropTypes.string
};

export default DividerLine;
