import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text} from 'react-native';
import { images, styles } from "./styles";
import { connect } from "react-redux";

const FooterTabItem = (props) => {
  const {
        tabBarIndex,
        isFocused
      } = props;



 let tabBarImage;
 if(parseInt(tabBarIndex) === 0){
  if(isFocused)
    tabBarImage = images.homeIconSelected;
  else
    tabBarImage = images.homeIconUnSelected;
 }else if(parseInt(tabBarIndex) === 1){
  if(isFocused)
    tabBarImage = images.orderIconSelected;
  else
    tabBarImage = images.orderIconUnSelected;
 }else if(parseInt(tabBarIndex) === 2){
  if(isFocused)
    tabBarImage = images.scanIconSelected;
  else
    tabBarImage = images.scanIconUnSelected;
 }else if(parseInt(tabBarIndex) === 3){
  if(isFocused)
    tabBarImage = images.profileIconSelected;
  else
    tabBarImage = images.profileIconUnSelected;
 }
  return (
    <View style={[styles.tabBarItem]}>
      <View style={styles.tabBarIconContainer}>
        <Image resizeMode="contain" source={tabBarImage}/>
      </View>
    </View>
  );
};
FooterTabItem.propTypes = {
  tabBarIndex: PropTypes.number,
  isFocused: PropTypes.bool
};

export default connect(
  (state) => ({ 
    notificationsCount: 0
  })
)(FooterTabItem);