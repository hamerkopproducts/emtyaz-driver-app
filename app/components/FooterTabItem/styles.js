import { StyleSheet } from "react-native";
import globals from "../../lib/globals"

const images = {
  homeIconSelected : require("../../assets/images/footerTabItem/icn-home-active.png"),
  homeIconUnSelected : require("../../assets/images/footerTabItem/icn-home.png"),
  orderIconSelected : require("../../assets/images/footerTabItem/icn-order-active.png"),
  orderIconUnSelected : require("../../assets/images/footerTabItem/icn-order.png"),
  scanIconSelected : require("../../assets/images/footerTabItem/icn-scan-active.png"),
  scanIconUnSelected : require("../../assets/images/footerTabItem/icn-scan.png"),
  profileIconSelected : require("../../assets/images/footerTabItem/icn-profile-active.png"),
  profileIconUnSelected : require("../../assets/images/footerTabItem/icn-profile.png")
};

const styles = StyleSheet.create({
  tabBarItem: {
    width: (globals.SCREEN_SIZE.width / 4),
    height: globals.INTEGER.footerTabBarHeight,
    justifyContent: 'center',
    alignItems: 'center'
  },
  tabBarIconContainer:{
    width: (globals.SCREEN_SIZE.width / 4),
    height: globals.INTEGER.footerTabBarHeight-50,
    justifyContent: 'center',
    alignItems: 'center'
  },
  badgeCountContainer:{
    position: 'absolute',
    top: (globals.INTEGER.footerTabBarHeight/2)-15,
    left: ((globals.SCREEN_SIZE.width / 4)/2),
    width: 18,
    height: 18,
    backgroundColor: 'red',
    borderRadius: 9,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 2
  },
  badgeCount:{
    color:globals.COLOR.white,
    fontFamily: globals.FONTS.helveticaNeueLight,
    textAlign: 'center',
    fontSize: globals.SCREEN_SIZE.width * 0.030
  },
  tabLabel:{
    top:0,
    color:'black',
    fontSize:10
  }
});

export { images, styles  };
