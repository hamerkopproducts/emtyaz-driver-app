import { StyleSheet } from "react-native";
import globals from "../../lib/globals"
const styles = StyleSheet.create({
  dividerStyle:{
    height: 1, 
    borderWidth: 0.3,
    borderStyle: 'dashed',
    borderColor: globals.COLOR.borderColor,
    width:'100%',
    borderRadius: 0.3,
  }
});

export { styles  };
