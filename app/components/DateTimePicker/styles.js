import { StyleSheet } from "react-native";
import globals from "../../lib/globals"

const styles = StyleSheet.create({
  popupContainer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: globals.COLOR.transparent
  },
  popupBackground: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: globals.COLOR.black,
    opacity: 0.7,
    zIndex: 1
  },
  popupContentContainer: {
    top: 0,
    position: 'absolute',
    flex: 1,
    width: '100%',
    height: '100%',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: "center",
    zIndex: 3
  },
  popupDialogContainer: {
    zIndex: 5,
    width: "88%",
    borderRadius: 10,
    backgroundColor: globals.COLOR.white,
    alignItems: 'center',
    justifyContent: "center",

  },
  popupContent: {
    marginBottom: 10,
    marginTop: 10,
    width: "80%",
    alignItems: 'center',
    justifyContent: "center",
  },
  popupButtonContainer: {
    marginTop: 10,
    marginBottom: 10,
    width: '100%',
    height: 42,
    flexDirection: 'row'
  },
  popupButton: {
    flex: 1,
    alignItems: 'center',
    justifyContent: "center"
  },
});

export {styles };
