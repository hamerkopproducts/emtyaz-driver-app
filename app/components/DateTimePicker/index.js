import PropTypes from 'prop-types';
import React from 'react';
import { Modal, View } from 'react-native';
import DatePicker from 'react-native-date-picker'
import { styles } from "./styles";
import appTexts from "../../lib/appTexts";

import RoundButton from "../RoundButton";
const DateTimePicker = (props) => {

  const {
    selectedDate,
    setSelectedDate,
    mode,
    isShowDatePicker,
    cancelClick,
    doneClick
  } = props;


  return (
    <Modal
      animationType="fade"
      transparent={true}
      onRequestClose={() => null}
      visible={isShowDatePicker}>
      <View style={styles.popupContainer}>
        <View style={styles.popupBackground} />
        <View style={styles.popupContentContainer}>
          <View style={styles.popupDialogContainer}>
            <View style={styles.popupContent}>
              <DatePicker
                date={selectedDate}
                onDateChange={(date) => { setSelectedDate(date)}}
                mode={mode}
              />
              <View style={styles.popupButtonContainer}>
                <RoundButton buttonPosition={'left'} buttonText={appTexts.COMMON_POPUP.cancel} buttonClick={cancelClick} />
                <RoundButton buttonPosition={'right'} buttonText={appTexts.COMMON_POPUP.done} buttonClick={doneClick} />
              </View>
            </View>
          </View>
        </View>
      </View>
    </Modal>
  );
};

DateTimePicker.propTypes = {
  selectedDate: PropTypes.any,
  mode: PropTypes.string,
  setSelectedDate: PropTypes.func,
  isShowDatePicker: PropTypes.bool,
  cancelClick: PropTypes.func,
  doneClick: PropTypes.func
};

export default DateTimePicker;
